# Changelog

<!-- markdownlint-disable-file MD024 -->

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.4.1] - 18-04-2023

### Fixed

- Fix the missing importation of table planet2star to make N-N link in db.

## [1.4.0] - 17-04-2023

### Added

- Add created and modified to star2publication and planet2publicationwith: `datetime.datetime.now()`.

## [1.3.4] - 17-04-2023

### Fixed

- Fix created and modified import in publication with a default: `datetime.datetime.now()`.

## [1.3.3] - 17-04-2023

### Removed

- Remove `updated` field from mapping. It is no longer in our base.

### Fixed

- Fix name of FK column `import_id` into `dbimport_id`.
- Fix name of column in table planet: `main_star` become `main_star_id`.

## [1.3.2] - 17-04-2023

### Added

- Add **temporary** now for created in table core_import.
**Need to remove when we change db and django on dates**.

## [1.3.1] - 17-04-2023

### Added

- Add possibility to give created and modified date to override default date in db.
- Add a try, except for eval if we find a SyntaxError in mapping.
- Add a new exception: `MappingError`.
- Add a new exception: `PythonSyntaxError`.
- Add a new exception: `ObjectAlphaNameError`.

### Changed

- Change regex for name validation. Now we accept in object name's:
  - `(`
  - `)`
  - `'`
  - `:`
  - `/`
  - `[`
  - `]`
  - `` ` ``
  - `*`

### Fixed

- Fix url checker, check url with `is_url` only if url are register.

## [1.2.0] - 04-04-2023

### Added

- Add exoimport version in db schema and we fill it with the actual version of the prog.
- Add tables name in config file.

### Changed

- Rename table in the config for typer tests.
- Change all table name in code for table name in config.

## [1.1.0] - 09-02-2023

### Added

- Add a cli command to get a sample of the mapping file: `sample-mapping`.

## [1.0.0] - 08-02-2023

### Added

- Add post validation of object name before the import.
- Add a name to an import.
- Add cli command to generate a config file for exoimport.
- Add the possibility to give the config in command line.
- Add the management of alternate names in the two summary (basic in terminal and json)
- Add an option to import in cli to give a json file for a detailed json summary.
- Add cli documentation (help) for import and config.
- Add import in typer.
- Add log in the project.
- Add logging management.
- Add a check to know if a star or planet is already in db when we import an object.
- Add methods to detect if a planet or star are already in db.
- Add management of a configuration file.
- Add the automatic addition of the `import_id` in the import of stars.
- Add way to link stars and publications in db.
- Add way to link planets and publications in db.
- Add function to insert publication in db.
- Add management of Mass detection for planets with exochoices.
- Add management of Radius detection for planets with exochoices.
- Add management of Unit for planets with exochoices.
- Add management of Web status for planets and stars with exochoices.
- Add management of Publication status for planets with exochoices.
- Add functions to insert alternate names and systems (star and his planets) in a db.
- Add function to insert planets in a database.
- Add function to insert stars in a database.
- ExoImport now support python3.11.
- Add functions to format data for the database.
- Add function to insert an import in a database.
- Add function to group planets by their star.
- Add function to split parameters to have planets parameters and star parameters.
- Add formatting of csv exoplanet for the insertion.
- Add the validation of headers for the import.
- Add progress bar and improve displaying of typer.
- Add a typer command for `help` in the user terminal interface.
- Add typer to get a user terminal interface for the translation.
- Add a module to write the translation in csv file.
- Add a module to filter the csv file.
- Add the module of translation.
- Add validation of csv headers and yaml mapping.
- Add a function to read a yaml.
- Add a function to read a csv.
