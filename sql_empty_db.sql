-- SQL commands to create a empty database for exoplanets.eu

-- Create tables

-- Create table core_import
CREATE TABLE core_import (
    id serial NOT NULL PRIMARY KEY,
    name VARCHAR(100) UNIQUE NOT NULL,
    created timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    external_csv BYTEA,
    mapping_yaml BYTEA,
    internal_csv BYTEA,
    exoimport_version varchar(80) NOT NULL
);


-- Create table core_star
CREATE TABLE core_star(
    id serial NOT NULL PRIMARY KEY,
    name VARCHAR(60) UNIQUE NOT NULL,
    distance double precision,
    distance_error double precision[],
    spec_type VARCHAR(15),
    magnitude_v double precision,
    magnitude_i double precision,
    magnitude_h double precision,
    magnitude_j double precision,
    magnitude_k double precision,
    mass double precision,
    mass_error double precision[],
    age double precision,
    age_error double precision[],
    teff double precision,
    teff_error double precision[],
    radius double precision,
    radius_error double precision[],
    metallicity double precision,
    metallicity_error double precision[],
    ra double precision NOT NULL,
    dec double precision NOT NULL,
    remarks text,
    other_web text,
    url_simbad VARCHAR(255),
    update date,
    radvel_proj double precision,
    created timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    -- Star web status
    status smallint NOT NULL,
    magnetic_field boolean,
    detected_disc smallint,
    dbimport_id integer references core_import(id)
);

-- Create table core_alternatestarname
CREATE TABLE core_alternatestarname(
    id serial NOT NULL PRIMARY KEY,
    star_id integer references core_star(id),
    name VARCHAR(60) UNIQUE NOT NULL
);

-- Create table core_planet
CREATE TABLE core_planet (
    id serial NOT NULL PRIMARY KEY,
    name VARCHAR(100),
    -- Planet Detection Type
    detection_type integer[] NOT NULL,
    -- Announced status: W, C, S, ...
    publication_status smallint NOT NULL,
    axis double precision,
    axis_error double precision[],
    period double precision,
    period_error double precision[],
    eccentricity double precision,
    eccentricity_error double precision[],
    omega double precision,
    omega_error double precision[],
    tzero_tr double precision,
    tzero_tr_error double precision[],
    tzero_vr double precision,
    tzero_vr_error double precision[],
    tperi double precision,
    tperi_error double precision[],
    tconj double precision,
    tconj_error double precision[],
    inclination double precision,
    inclination_error double precision[],
    discovered smallint,
    updated date,
    remarks text,
    other_web text,
    detect_mode VARCHAR(255),
    angular_distance double precision,
    temp_calculated double precision,
    temp_measured double precision,
    hot_point_lon double precision,
    log_g double precision,
    created timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    -- Planet Web Status
    status smallint NOT NULL,
    tzero_tr_sec double precision,
    tzero_tr_sec_error double precision[],
    lambda_angle double precision,
    lambda_angle_error double precision[],
    albedo double precision,
    albedo_error double precision[],
    mass_detection_type smallint,
    radius_detection_type smallint,
    impact_parameter double precision,
    impact_parameter_error  double precision[],
    K double precision,
    K_error double precision[],
    planet_status smallint,
    temp_calculated_error double precision[],
    main_star_id integer references core_star(id),
    mass_sini_unit smallint,
    mass_sini_string VARCHAR,
    mass_detected_unit smallint,
    mass_detected_string VARCHAR,
    mass_sini_error_string VARCHAR[],
    mass_detected_error_string VARCHAR[],
    radius_unit smallint,
    radius_string VARCHAR,
    radius_error_string VARCHAR[],
    planetary_system_id integer,
    dbimport_id integer references core_import(id)
);

-- Create table core_alternateplanetname
CREATE TABLE core_alternateplanetname(
    id serial NOT NULL PRIMARY KEY,
    planet_id integer references core_planet(id),
    name VARCHAR(60) UNIQUE NOT NULL
);

--Create table core_molecule
CREATE TABLE core_molecule(
    id serial NOT NULL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    created timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    status smallint NOT NULL,
    dbimport_id integer references core_import(id)
);

-- Create table core_atmospheremolecule
CREATE TABLE core_atmospheremolecule(
    id serial NOT NULL PRIMARY KEY,
    molecule_id integer references core_molecule(id),
    planet_id integer references core_planet(id),
    type smallint,
    result_value double precision,
    result_value_error double precision[],
    result_figure VARCHAR(255),
    data_source smallint,
    notes VARCHAR(255),
    publication_id integer,
    created timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    dbimport_id integer references core_import(id)
);

-- Create table core_atmospheretemperature
CREATE TABLE core_atmospheretemperature(
    id serial NOT NULL PRIMARY KEY,
    planet_id integer references core_planet(id),
    type smallint NOT NULL,
    result_value double precision,
    result_value_error double precision[],
    result_figure VARCHAR(255),
    data_source smallint,
    notes VARCHAR(255),
    publication_id integer,
    created timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    dbimport_id integer references core_import(id)
);

-- Create table core_publication
CREATE TABLE core_publication(
    id serial NOT NULL PRIMARY KEY,
    title text NOT NULL,
    author text,
    date VARCHAR(10) NOT NULL,
    -- type of publication refereed, ...
    type smallint NOT NULL,
    journal_name VARCHAR(200),
    journal_volume VARCHAR(15),
    journal_page VARCHAR(15),
    bibcode VARCHAR(19),
    keywords VARCHAR(150),
    reference text,
    url text,
    updated date,
    created timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    -- Web status
    status smallint NOT NULL,
    doi VARCHAR[],
    dbimport_id integer references core_import(id)
);

-- Create table core_planet2publication
CREATE TABLE core_planet2publication(
    id serial NOT NULL PRIMARY KEY,
    publication_id integer NOT NULL references core_publication(id),
    planet_id integer NOT NULL references core_planet(id),
    parameter smallint NOT NULL,
    created timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    -- Web status
    status smallint NOT NULL
);

-- Create Table core_star2publication
CREATE TABLE core_star2publication(
    id serial NOT NULL PRIMARY KEY,
    publication_id integer NOT NULL references core_publication(id),
    star_id integer NOT NULL references core_star(id),
    parameter smallint NOT NULL,
    created timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    -- Web status
    status smallint NOT NULL
);

-- Create table core_planetarysystem
CREATE TABLE core_planetarysystem(
    id serial NOT NULL PRIMARY KEY,
    ra double precision NOT NULL,
    dec double precision NOT NULL,
    distance double precision,
    distance_error double precision[],
    created timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    dbimport_id integer references core_import(id)
);

-- Create table core_planet2star
CREATE TABLE core_planet2star(
    id serial NOT NULL PRIMARY KEY,
    planet_id integer NOT NULL references core_planet(id),
    star_id integer NOT NULL references core_star(id),
    system_id integer references core_planetarysystem(id),
    UNIQUE (planet_id, star_id)
);

-- Create function for on update
CREATE OR REPLACE FUNCTION update_modified_column()
RETURNS TRIGGER AS $$
BEGIN
    NEW.modified := now();
    RETURN NEW;
END;
$$ language 'plpgsql';

-- Create Triggers

-- Trigger for core_star
CREATE TRIGGER update_core_star_modified_column BEFORE UPDATE ON core_star
FOR EACH ROW EXECUTE PROCEDURE update_modified_column();

-- Trigger for core_planet
CREATE TRIGGER update_core_planet_modified_column BEFORE UPDATE ON core_planet
FOR EACH ROW EXECUTE PROCEDURE update_modified_column();

-- Trigger for core_molecule
CREATE TRIGGER update_core_molecule_modified_column BEFORE UPDATE ON core_molecule
FOR EACH ROW EXECUTE PROCEDURE update_modified_column();

-- Trigger for core_atmospheremolecule
CREATE TRIGGER update_core_atmosphere_molecule_modified_column BEFORE UPDATE ON core_atmospheremolecule
FOR EACH ROW EXECUTE PROCEDURE update_modified_column();

-- Trigger for core_atmospheretemperature
CREATE TRIGGER update_core_atmosphere_temperature_modified_column BEFORE UPDATE ON core_atmospheretemperature
FOR EACH ROW EXECUTE PROCEDURE update_modified_column();

-- Trigger for core_publication
CREATE TRIGGER update_core_publication_modified_column BEFORE UPDATE ON core_publication
FOR EACH ROW EXECUTE PROCEDURE update_modified_column();

-- Trigger for core_planet2publication
CREATE TRIGGER update_core_planet2publication_modified_column BEFORE UPDATE ON core_planet2publication
FOR EACH ROW EXECUTE PROCEDURE update_modified_column();

-- Trigger for core_star2publication
CREATE TRIGGER update_core_star2publication_modified_column BEFORE UPDATE ON core_star2publication
FOR EACH ROW EXECUTE PROCEDURE update_modified_column();

-- Trigger for core_planetary_system
CREATE TRIGGER update_core_planetary_system_modified_column BEFORE UPDATE ON core_planetarysystem
FOR EACH ROW EXECUTE PROCEDURE update_modified_column();
