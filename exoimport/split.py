"""Useful function to split parameters."""

# Local imports
from .logging import get_logger
from .utils import CsvDictData, CsvDictRow

STAR_PARAMS = [
    "star_name",
    "ra",
    "dec",
    "mag_v",
    "mag_i",
    "mag_j",
    "mag_h",
    "mag_k",
    "star_distance",
    "star_distance_error_min",
    "star_distance_error_max",
    "star_metallicity",
    "star_metallicity_error_min",
    "star_metallicity_error_max",
    "star_mass",
    "star_mass_error_min",
    "star_mass_error_max",
    "star_radius",
    "star_radius_error_min",
    "star_radius_error_max",
    "star_sp_type",
    "star_age",
    "star_age_error_min",
    "star_age_error_max",
    "star_teff",
    "star_teff_error_min",
    "star_teff_error_max",
    "star_detected_disc",
    "star_magnetic_field",
    "star_status",
    "star_created",
    "star_modified",
]

# These parameters are the parameters relating to other tables than stars and planets.
OTHER_PARAMS = [
    "alternate_names",
    "star_alternate_names",
    "molecules",
    "publication_type",
    "publication_title",
    "publication_date",
    "publication_url",
    "publication_created",
    "publication_modified",
]


def split_star_planet_and_other_params(
    csv_exo_content: CsvDictData,
) -> tuple[CsvDictData, CsvDictData, CsvDictData]:
    """
    Split a csv content into 2 CsvDictData, one for planets, one for starts.

    Args:
        csv_exo_content: Csv content to split.

    Returns:
        Csv content split to separate data of planet, data of stars and data
            of other tables.
    """
    planet_result = []
    star_result = []
    other_result = []

    for row in csv_exo_content:
        res_star = {}
        res_planet = {}
        res_other = {}

        for params in row.keys():
            if params in STAR_PARAMS:
                res_star[params] = row.get(params)
            elif params in OTHER_PARAMS:
                res_other[params] = row.get(params)
            else:
                res_planet[params] = row.get(params)

        star_result.append(res_star)
        planet_result.append(res_planet)
        other_result.append(res_other)

    get_logger().info("parameters has been split")
    return (star_result, planet_result, other_result)


def _get_planets_for_one_star(
    stars: CsvDictData,
    planets: CsvDictData,
    star: CsvDictRow,
) -> list[CsvDictRow]:
    """
    Get planets link to a star.

    An element in planets list is link to the stars who have the same index in the list
    of stars. The list of stars can contains duplication.

    Args:
        stars: All stars.
        planets: All planets.
        star: Star whose related planets we want.

    Returns:
        All planets related to star.

    Examples:
        # For the first star.
        >>> stars = [{"a": 12}, {"a": 13}, {"a": 12}, {"a": 14}]
        >>> planets = [{"b": 1}, {"b": 2}, {"b": 3}, {"b": 4}]
        >>> _get_planets_for_one_star(stars, planets, stars[0])
        [{'b': 1}, {'b': 3}]

        # For the second star.
        >>> stars = [{"a": 12}, {"a": 13}, {"a": 12}, {"a": 14}]
        >>> planets = [{"b": 1}, {"b": 2}, {"b": 3}, {"b": 4}]
        >>> _get_planets_for_one_star(stars, planets, stars[1])
        [{'b': 2}]

        # For the first star.
        >>> stars = [{"a": 12}, {"a": 13}, {"a": 12}, {"a": 14}]
        >>> planets = [{"b": 1}, {"b": 2}, {"b": 3}, {"b": 4}]
        >>> _get_planets_for_one_star(stars, planets, stars[2])
        [{'b': 1}, {'b': 3}]

        # For the fourth star.
        >>> stars = [{"a": 12}, {"a": 13}, {"a": 12}, {"a": 14}]
        >>> planets = [{"b": 1}, {"b": 2}, {"b": 3}, {"b": 4}]
        >>> _get_planets_for_one_star(stars, planets, stars[3])
        [{'b': 4}]
    """
    return [planets[idx] for idx, val in enumerate(stars) if val == star]


def group_planets_by_star_index(
    stars: CsvDictData,
    planets: CsvDictData,
) -> dict[int, list[CsvDictRow]]:
    """
    Group planets by their linked star.

    An element in planets list is link to the stars who have the same index in the list
    of stars. The list of stars can contains duplication.

    Args:
        stars: All stars.
        planets: All planets.

    Returns:
        All the planets linked by the index of their star.


    Examples:
        >>> import logging as log
        >>> log.disable(log.CRITICAL)
        >>> stars = [{"a": 12}, {"a": 13}, {"a": 12}, {"a": 14}]
        >>> planets = [{"b": 1}, {"b": 2}, {"b": 3}, {"b": 4}]
        >>> group_planets_by_star_index(stars, planets)
        {0: [{'b': 1}, {'b': 3}], 1: [{'b': 2}], 3: [{'b': 4}]}
        >>> log.disable(log.NOTSET)

    """
    get_logger().info("Group planets by star index")
    return {
        index: _get_planets_for_one_star(stars, planets, star)
        for index, star in enumerate(stars)
        if star not in stars[:index]
    }
