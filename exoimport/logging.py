"""Functions to manage the logging of the project."""

# Standard imports
import logging as log
from dataclasses import dataclass, field
from functools import lru_cache
from pathlib import Path
from typing import TypedDict

# Third party imports
from arrow import now as arr_now

# Local imports
from .exceptions import LoggerAlreadyConfiguredError, LoggerAlreadyUsedError


class _LoggerConfigDict(TypedDict):
    """
    Typed Dictionary of logger configuration.

    Attributes:
        advanced: True for a advanced logging system.
        cwd: Current work directory for log files.
        already_configured: True if the configuration is already configured.
        get_logger_called: True if `get_logger()` has already been called.

    """

    advanced: bool
    cwd: Path | None
    already_configured: bool
    get_logger_called: bool


_LOGGER_CONFIG = _LoggerConfigDict(
    advanced=False,
    cwd=None,
    already_configured=False,
    get_logger_called=False,
)


class _LogLevelCapture(log.Logger):
    """
    _LogLevelCapture object use to create different loggers for each logging level.

    With this object we have 4 different loggers writing to 4 different log files.

    Logging levels:
        - INFO
        - WARNING
        - ERROR
        - CRITICAL

    Attributes:
        cwd: Current work directory. Useful for testing.

    """

    cwd: Path

    def __init__(self, cwd: Path) -> None:
        """
        Initialize the cwd attribute and the formatter for logging.

        Args:
            cwd: Current work directory. Useful for testing.
        """
        self.formatter = log.Formatter(
            '[%(asctime)s]%(levelname)s:"%(message)s"',
            datefmt="%d/%m/%Y %I:%M:%S %p",
        )
        self.cwd = cwd

    def _extendable_logger(
        self,
        log_name: str,
        log_file_name: str,
        level: int,
    ) -> log.Logger:
        """
        Create a logger for a specific logging level.

        Args:
            log_name: Name of the logger.
            log_file_name: Name of the log file for this logger.
            level: Logging level of this logger.

        Returns:
            A logger with a specific level.
        """
        folder_log_path = self.cwd / "logs"
        folder_log_path.mkdir(exist_ok=True)

        handler = log.FileHandler(folder_log_path / log_file_name)
        handler.setFormatter(self.formatter)

        specified_logger = log.getLogger(log_name)
        specified_logger.setLevel(level)
        specified_logger.addHandler(handler)

        with Path(folder_log_path / log_file_name).open(
            mode="a",
            encoding="utf-8",
        ) as log_file:
            log_file.write(
                f"""[{arr_now().format('DD/MM/YYYY HH:mm:ss')}]"""
                'INFO:"New import"\n',
            )

        return specified_logger

    def info_logger(self) -> log.Logger:
        """
        Get a logger for the level info.

        Returns:
            A logger who manages and writes the info logs.
        """
        return self._extendable_logger("info_log", "info.exoimport.log", log.INFO)

    def warning_logger(self) -> log.Logger:
        """
        Get a logger for the level warning.

        Returns:
            A logger who manages and writes the warning logs.
        """
        return self._extendable_logger(
            "warning_log", "warning.exoimport.log", log.WARNING
        )

    def error_logger(self) -> log.Logger:
        """
        Get a logger for the level error.

        Returns:
            A logger who manages and writes the error logs.
        """
        return self._extendable_logger("error_log", "error.exoimport.log", log.ERROR)

    def critical_logger(self) -> log.Logger:
        """
        Get a logger for the level critical.

        Returns:
            A logger who manages and writes the critical logs.
        """
        return self._extendable_logger(
            "critical_log",
            "critical.exoimport.log",
            log.CRITICAL,
        )


@dataclass
class _ExoImportLogger:
    """
    _ExoImportLogger object to manage advanced logging of the project.

    Attributes:
        __info: Logger for info logging level.
        __warning: Logger for warning logging level.
        __error: Logger for error logging level.
        __critical: Logger for critical logging level.

    """

    __info: log.Logger = field(init=False)
    __warning: log.Logger = field(init=False)
    __error: log.Logger = field(init=False)
    __critical: log.Logger = field(init=False)

    def __init__(self, cwd: Path) -> None:
        """
        Initialize all logger.

        Args:
            cwd: Current work directory. Useful for testing.
        """
        logger_gen = _LogLevelCapture(cwd)
        self.__info = logger_gen.info_logger()
        self.__warning = logger_gen.warning_logger()
        self.__error = logger_gen.error_logger()
        self.__critical = logger_gen.critical_logger()

    def info(self, msg: str) -> None:
        """
        Write an info log message in the info logger.

        Args:
            msg: Message to write.
        """
        self.__info.info(msg)

    def warning(self, msg: str) -> None:
        """
        Write an warning log message in the warning logger.

        Args:
            msg: Message to write.
        """
        self.__warning.warning(msg)

    def error(self, msg: str) -> None:
        """
        Write an error log message in the error logger.

        Args:
            msg: Message to write.
        """
        self.__error.error(msg)

    def critical(self, msg: str) -> None:
        """
        Write an critical log message in the critical logger.

        Args:
            msg: Message to write.
        """
        self.__critical.critical(msg)


def config_logger(
    *,
    advanced: bool,
    cwd: Path | None,
) -> None:
    """
    Configure the logger.

    We can configure the logger only one time AND before calling `get_logger()`.

    Args:
        advanced: True for an advanced logger, False for a classic logger.
        cwd: Current work directory.

    Raises:
        LoggerAlreadyConfiguredError: If we try to reconfigure the logger.
        LoggerAlreadyUsedError: If we try to configure the logger after using it.
    """
    if _LOGGER_CONFIG["already_configured"]:
        raise LoggerAlreadyConfiguredError()

    if _LOGGER_CONFIG["get_logger_called"]:
        raise LoggerAlreadyUsedError()

    _LOGGER_CONFIG["advanced"] = advanced
    _LOGGER_CONFIG["cwd"] = cwd
    _LOGGER_CONFIG["already_configured"] = True


def reset_config() -> None:
    r"""
    Reset configuration.

    /!\ THIS FUNCTION SHOULD NOT BE USED. It is used for tests.
    """
    _LOGGER_CONFIG["already_configured"] = False
    _LOGGER_CONFIG["get_logger_called"] = False
    _LOGGER_CONFIG["advanced"] = False
    _LOGGER_CONFIG["cwd"] = None


@lru_cache(maxsize=1)
def get_logger() -> _ExoImportLogger | log.Logger:
    """
    Get a logger.

    This function is cached. If we call it with the same arguments we don't recreate
        the logger.

    If advanced is True in logger config we get a precise logger who write each logging
        level messages in different files.

    Returns:
        A classic logger or a advanced logger.
    """
    _LOGGER_CONFIG["get_logger_called"] = True
    cwd = Path.cwd() if _LOGGER_CONFIG["cwd"] is None else _LOGGER_CONFIG["cwd"]
    Path(cwd / "logs").mkdir(exist_ok=True)

    if _LOGGER_CONFIG["advanced"]:
        return _ExoImportLogger(cwd)

    handler = log.FileHandler(
        filename=cwd / "logs" / "exoimport.log",
    )
    handler.setFormatter(
        log.Formatter(
            '[%(asctime)s]%(levelname)s:"%(message)s"',
            datefmt="%d/%m/%Y %I:%M:%S %p",
        ),
    )

    exolog = log.getLogger("exolog")
    exolog.setLevel(log.INFO)
    for hdlr in exolog.handlers:
        exolog.removeHandler(hdlr)
    exolog.addHandler(handler)

    exolog.info("New import")
    return exolog
