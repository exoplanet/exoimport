"""Functions for the similarity of objects."""

# Standard imports
from decimal import Decimal
from typing import NamedTuple, cast

# Third party imports
from mpmath import cos as mpm_cos
from mpmath import libmp, mp, mpf
from mpmath import nstr as mpf_to_str
from mpmath import radians as mpm_radians
from mpmath import sin as mpm_sin
from psycopg import Connection
from py_linq import Enumerable
from py_linq_sql import SQLEnumerable
from py_linq_sql import acosd as pylinqsql_acosd
from py_linq_sql import cosd as pylinqsql_cosd
from py_linq_sql import lower as pylinqsql_lower
from py_linq_sql import sind as pylinqsql_sind

# Local imports
from .config import Tables
from .logging import get_logger

mp.dps = 20


class StarRaDec(NamedTuple):
    """
    NamedTuple of a star position.

    Attributes:
        ra: Right ascension.
        dec: Declination.

    See:
        Right ascension: https://en.wikipedia.org/wiki/Right_ascension
        Declination: https://en.wikipedia.org/wiki/Declination
        Equatoirial coordinate system:
            https://en.wikipedia.org/wiki/Equatorial_coordinate_system
    """

    ra: Decimal
    dec: Decimal


def _degree_to_radian_decimal(number: Decimal) -> Decimal:
    """
    Get an angle in radians and decimal of a degrees value.

    We choose 20 significant digits (precision). If the number is less than 1 and
    greater than -1 it gives 20 deicmal digits.
    Otherwise it gives X digits before the decimal and 20 - X decimal digits.

    For example:
    A = 1.0723692508781812416
    B = 0.12452756813273719415

    A and B both have 20 significant digits.

    Args:
        number: A number in degrees which we want the radians.

    Returns:
        Cosinu in rafians and decimal of a degrees value.

    Examples:
        >>> _degree_to_radian_decimal(Decimal(12))
        Decimal('0.20943951023931954923')
    """
    return Decimal(
        mpf_to_str(
            mpm_radians(mpf(libmp.from_Decimal(number))),
            20,
            strip_zeros=False,
        ),
    )


def _sind_decimal(number: Decimal) -> Decimal:
    """
    Get the sin in decimal of a degrees value.

    We choose 20 significant digits (precision). If the number is less than 1 and
    greater than -1 it gives 20 deicmal digits.
    Otherwise it gives X digits before the decimal and 20 - X decimal digits.

    For example:
    A = 1.0723692508781812416
    B = 0.12452756813273719415

    A and B both have 20 significant digits.

    Args:
        number: A number in degrees which we want the sinus.

    Returns:
        Sinus in radians and decimal of a degrees value.

    Examples:
        >>> _sind_decimal(Decimal(12))
        Decimal('0.20791169081775933710')
    """
    number_in_radians = _degree_to_radian_decimal(number)

    return Decimal(
        mpf_to_str(
            mpm_sin(mpf(libmp.from_Decimal(number_in_radians))),
            20,
            strip_zeros=False,
        ),
    )


def _cosd_decimal(number: Decimal) -> Decimal:
    """
    Get the cos in decimal of a degrees value.

    We choose 20 significant digits (precision). If the number is less than 1 and
    greater than -1 it gives 20 deicmal digits.
    Otherwise it gives X digits before the decimal and 20 - X decimal digits.

    For example:
    A = 1.0723692508781812416
    B = 0.12452756813273719415

    A and B both have 20 significant digits.

    Args:
        number: A number in degrees which we want the cosine.

    Returns:
        Cosinu in rafians and decimal of a degrees value.

    Examples:
        >>> _cosd_decimal(Decimal(12))
        Decimal('0.97814760073380563793')
    """
    number_in_radians = _degree_to_radian_decimal(number)

    return Decimal(
        mpf_to_str(
            mpm_cos(mpf(libmp.from_Decimal(number_in_radians))),
            20,
            strip_zeros=False,
        ),
    )


def _is_at_the_same_position(
    conn: Connection,
    table: str,
    star_to_check: StarRaDec,
    epsilon: Decimal,
) -> bool:
    """
    Check if a star exist at the given position in the database.

    For more details on the formula of Angular distance,
    to known if two objects are at the same position, see the documentation.

    For sin and cos we choose 20 significant digits (precision).

    Args:
        conn: Connection to the db.
        table: Table of object to check.
        star_to_import: Data of the the star to check
        epsilon: Tolerance on the position.

    Returns:
        True if a object is already at the position, False otherwise.

    Warnings:
        Postgresql make all trigonometric function with float in input and output.
        If the difference between the two position are very close of the epsilon
        you can have false positive. It's very specific case that can happen when the
        gap is approximately smaller or equal to 1,500983157e-6 raidans
        or 8.6e-5 degrees.

    """
    exactly_same_ra_dec = (
        SQLEnumerable(conn, table)
        .select(lambda x: x.id)
        .where(lambda x: (x.dec == star_to_check.dec) & (x.ra == star_to_check.ra))
        .execute()
    )

    if cast(Enumerable, exactly_same_ra_dec).to_list():
        return True

    epsilon_in_degrees = epsilon / Decimal("3600.0")

    sam_position_near_epsilon = (
        SQLEnumerable(conn, table)
        .select(lambda x: x.id)
        .where(
            lambda x: (x.dec >= star_to_check.dec - epsilon_in_degrees)
            & (x.dec <= star_to_check.dec + epsilon_in_degrees),
        )
        .where(
            lambda x: (
                pylinqsql_acosd(
                    pylinqsql_sind(x.dec) * _sind_decimal(star_to_check.dec)
                    + pylinqsql_cosd(x.dec)
                    * _cosd_decimal(star_to_check.dec)
                    * pylinqsql_cosd(x.ra - star_to_check.ra),
                )
                < epsilon_in_degrees
            ),
        )
        .execute()
    )

    if cast(Enumerable, sam_position_near_epsilon).to_list():
        get_logger().warning("Star at the same position has been detected.")
        return True

    return False


def _has_the_same_names(
    conn: Connection,
    obj_table: str,
    alter_name_table: str,
    name_to_check: str,
) -> bool:
    """
    Check if a given name is already in database in "main" name or in alternate names.

    Args:
        conn: Connection to the db.
        obj_table: Table of objects (stars, panets, ...)
        altern_name_table: Table of alternate names.
        name_to_check: Name to check.

    Returns:
        True if the name is already used in obj_table or alter_name_table,
        False otherwise.
    """
    for table in [obj_table, alter_name_table]:
        check_same_name = (
            SQLEnumerable(conn, table)
            .select(lambda x: x.id)
            .where(lambda x: pylinqsql_lower(x.name) == name_to_check.lower())
            .execute()
        )

        if cast(Enumerable, check_same_name).to_list():
            get_logger().warning("Object with the same name has been detected.")
            return True

    return False


def is_star_already_in_db(
    conn: Connection,
    star: tuple[str, StarRaDec],
    epsilon_ra_dec: Decimal,
    config_tables: Tables,
) -> bool:
    """
    Check if a star is already in the database.

    We verify if a star with the same name or alternate name exist in db.
    Then we verify if a star is at the same position.

    For more details on the formula of Angular distance,
    to known if an object are at the same position, see the documentation.

    Args:
        conn: Connection to the db.
        star: Star name and star ra and dec.
        epsilon: Tolerance on the position.
        config_tables: Name of all tables from config file.

    Returns:
        True if a star already exist in the db, False otherwise.
    """
    name = star[0]
    ra_dec = star[1]
    get_logger().info(
        f"Checking for the existence of a star with: name: {name}, position: {ra_dec}",
    )
    return any(
        [
            _has_the_same_names(
                conn,
                config_tables.star_table,
                config_tables.alternate_star_name_table,
                name,
            ),
            _is_at_the_same_position(
                conn, config_tables.star_table, ra_dec, epsilon_ra_dec
            ),
        ],
    )


def is_planet_already_in_db(
    conn: Connection,
    planet_name: str,
    config_tables: Tables,
) -> bool:
    """
    Check if a planet is already in the database.

    We verify if a planet with the same name or alternate name exist in db.

    Args:
        conn: Connection to the db.
        planet_name: Name to check.
        config_tables: Name of all tables from config file.

    Returns:
        True if the name is already in the db, False otherwise.
    """
    get_logger().info(
        f"Checking for the existence of a planet with the name: {planet_name}",
    )
    return _has_the_same_names(
        conn,
        config_tables.planet_table,
        config_tables.alternate_planet_name_table,
        planet_name,
    )
