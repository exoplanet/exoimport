"""Functions to format detection types."""

# Standard imports
from typing import cast

# Third party imports
from exochoices import MassDetection, PlanetDetection, RadiusDetection

# Local imports
from ..utils import CsvDictData, CsvDictData_Error, need_to_format


def format_detection_type(data: CsvDictData) -> CsvDictData_Error:
    """
    Format detection_type to correspond to the database.

    Args:
        data: Data to format.

    Examples:
        >>> from decimal import Decimal
        >>> data = [
        ...     {"detection_type": "microlensing", "mass_detection_type": "timing"},
        ...     {"detection_type": "timing", "radius_detection_type": "flux"}
        ... ]
        >>> format_detection_type(data)
        [
        ...     {
        ...         'detection_type': [Decimal('4')],
        ...         'mass_detection_type': Decimal('2')
        ...     },
        ...     {
        ...         'detection_type': [Decimal('2')],
        ...         'radius_detection_type': Decimal('12')
        ...     }
        ... ]

    Returns:
        A copy of data with detection_type fields formatted.
    """
    result = cast(CsvDictData_Error, data.copy())

    for obj_res, obj_data in zip(result, data, strict=True):
        obj_res["detection_type"] = [
            PlanetDetection.from_str(cast(str, obj_data["detection_type"])).as_decimal,
        ]

        if need_to_format("mass_detection_type", obj_data):
            obj_res["mass_detection_type"] = MassDetection.from_str(
                cast(str, obj_data["mass_detection_type"]),
            ).as_decimal

        if need_to_format("radius_detection_type", obj_data):
            obj_res["radius_detection_type"] = RadiusDetection.from_str(
                cast(str, obj_data["radius_detection_type"]),
            ).as_decimal

    return result
