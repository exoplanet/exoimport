"""Functions to format units."""

# Standard imports
from typing import cast

# Third party imports
from exochoices import MassUnit, RadiusUnit

# Local imports
from ..utils import CsvDictData, need_to_format


def format_unit(data: CsvDictData) -> None:
    """
    Format unit for planets.

    Args:
        data: Data to format.

    Examples:
        >>> from decimal import Decimal
        >>> data = [
        ...     {
        ...         "radius_unit": "RJUP",
        ...         "mass_unit": "MJUP"
        ...     },
        ...     {
        ...         "radius_unit": "REARTH",
        ...         "mass_unit": "MEARTH"
        ...     }
        ... ]
        >>> format_unit(data)
        >>> print(data)
        [
        ...     {
        ...         'radius_unit': Decimal('1'),
        ...         'mass_unit': Decimal('1')
        ...     },
        ...     {
        ...         'radius_unit': Decimal('2'),
        ...         'mass_unit': Decimal('2')
        ...     }
        ... ]
    """
    for obj in data:
        if need_to_format("radius_unit", obj):
            obj["radius_unit"] = RadiusUnit.from_str(
                cast(str, obj["radius_unit"]),
            ).as_decimal

        if need_to_format("mass_unit", obj):
            obj["mass_unit"] = MassUnit.from_str(cast(str, obj["mass_unit"])).as_decimal
