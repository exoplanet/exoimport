"""Functions to format a csv for import."""

# Standard imports
from typing import cast

# Third party imports
from exochoices import PublicationType, WebStatus

# Local imports
from ..config.config import Configuration
from ..exceptions import InvalidUrlError
from ..logging import get_logger
from ..utils import CsvDictData, CsvDictData_Error, DbColumn, DbData, is_url
from .format_detection_types import format_detection_type
from .format_import import (
    format_csvdictdata_2_csvdata,
    format_url,
    get_errors_in_good_format,
    get_good_fields_names,
    get_headers_and_data_in_good_format,
)
from .format_statuses import format_planet_statuses
from .format_units import format_unit


def format_to_import_publications(data: CsvDictData) -> tuple[DbColumn, DbData]:
    """
    Format publication data to import it in the database.

    Args:
        data: Data to format.

    Returns:
        Headers and Data on the good format for the insertion with py-linq-sql.
    """
    data_copy = data.copy()

    for data_row, copy_row in zip(data, data_copy, strict=True):
        if data_row["publication_url"] and not is_url(
            cast(str, data_row["publication_url"])
        ):
            raise InvalidUrlError(cast(str, data_row["publication_url"]))
        copy_row["publication_url"] = format_url(cast(str, data_row["publication_url"]))

    data_in_csvdata = format_csvdictdata_2_csvdata(cast(CsvDictData_Error, data_copy))
    formated_data = get_headers_and_data_in_good_format(data_in_csvdata)

    headers = get_good_fields_names(formated_data[0])
    data_to_import = formated_data[1]

    get_logger().info("publication data to import has been formatted")
    return headers, data_to_import


def format_to_import(data: CsvDictData) -> tuple[DbColumn, DbData]:
    """
    Format data to import it in the database.

    Args:
        data: Data to format.

    Returns:
        Headers and Data on the good format for the insertion with py-linq-sql.
    """
    data_w_good_error_format = get_errors_in_good_format(data)
    data_in_csvdata = format_csvdictdata_2_csvdata(data_w_good_error_format)
    formated_data = get_headers_and_data_in_good_format(data_in_csvdata)

    headers = get_good_fields_names(formated_data[0])
    data_to_import = formated_data[1]

    get_logger().info("data to import has been formatted")
    return headers, data_to_import


def format_stars(data: CsvDictData, config: Configuration) -> None:
    """
    Format (web) status of a star.

    Args:
        data: Data to format.

    Examples:
        >>> import logging as log
        >>> log.disable(log.CRITICAL)
        >>> from decimal import Decimal
        >>> data = [
        ...     {
        ...         "ra": Decimal('3.1364'),
        ...     },
        ...     {
        ...         "ra": Decimal('1.34537'),
        ...     }
        ... ]
        >>> config = Configuration()
        >>> format_stars(data, config)
        >>> print(data)
        [
        ...     {
        ...         'ra': Decimal('3.1364'),
        ...         'star_status': Decimal('1')
        ...     },
        ...     {
        ...         'ra': Decimal('1.34537'),
        ...         'star_status': Decimal('1')
        ...     }
        ... ]
        >>> log.disable(log.NOTSET)
    """
    for obj in data:
        obj["star_status"] = config.web_statuses.as_decimal

    get_logger().info("stars has been formatted")


def format_planets(data: CsvDictData, config: Configuration) -> CsvDictData_Error:
    """
    Format planets to correspond to the schema of the database.

    Args:
        data: Data to format.

    Examples:
        >>> import logging as log
        >>> log.disable(log.CRITICAL)
        >>> from decimal import Decimal
        >>> data = [
        ...     {
        ...         "detection_type": "microlensing",
        ...         "mass_detection_type": "timing",
        ...         "planet_status": "Candidate",
        ...         "announced_status": "R",
        ...         "mass_unit": "MJUP",
        ...         "radius_unit": "RJUP"
        ...     },
        ...     {
        ...         "detection_type": "timing",
        ...         "radius_detection_type": "flux",
        ...         "planet_status": "Confirmed",
        ...         "announced_status": "s",
        ...         "mass_unit": "MEARTH",
        ...         "radius_unit": "REARTH"
        ...     }
        ... ]
        >>> config = Configuration()
        >>> format_planets(data, config)
        [
        ...     {
        ...         'detection_type': [Decimal('4')],
        ...         'mass_detection_type': Decimal('2'),
        ...         'planet_status': Decimal('2'),
        ...         'announced_status': Decimal('1'),
        ...         'mass_unit': Decimal('1'),
        ...         'radius_unit': Decimal('1'),
        ...         'status': Decimal('1')
        ...     },
        ...     {
        ...         'detection_type': [Decimal('2')],
        ...         'radius_detection_type': Decimal('12'),
        ...         'planet_status': Decimal('1'),
        ...         'announced_status': Decimal('2'),
        ...         'mass_unit': Decimal('2'),
        ...         'radius_unit': Decimal('2'),
        ...         'status': Decimal('1')
        ...     }
        ... ]
        >>> log.disable(log.NOTSET)

    Returns:
        A copy of data with status and detection_type fields formatted.
    """
    format_planet_statuses(data, config.web_statuses)
    format_unit(data)
    res = format_detection_type(data)

    get_logger().info("planets has been formatted")
    return res


def format_publications(data: CsvDictData, status: WebStatus) -> None:
    """
    Format type of a publication.

    Args:
        data: Data to format.

    Examples:
        >>> import logging as log
        >>> log.disable(log.CRITICAL)
        >>> from decimal import Decimal
        >>> data = [{"publication_type": "book"}]
        >>> format_publications(data, WebStatus.ACTIVE)
        >>> print(data)
        [{'publication_type': Decimal('1'), 'status': Decimal('1')}]
        >>> log.disable(log.NOTSET)
    """
    for obj in data:
        obj["publication_type"] = PublicationType.from_str(
            cast(str, obj["publication_type"]),
        ).as_decimal
        obj["status"] = status.as_decimal

    get_logger().info("publications has been formatted")
