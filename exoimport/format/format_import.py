"""Functions to format data for import."""

# Standard imports
from typing import cast

# Local imports
from ..utils import (
    CsvDictData,
    CsvDictData_Error,
    CsvDictRow,
    CsvDictRow_Error,
    CsvTypedData_Error,
    CsvTypedRow_Error,
    DbColumn,
    DbData,
    DbRow,
)

CATALOG_HEADERS = {
    "star_name": "name",
    "ra": "ra",
    "dec": "dec",
    "mag_v": "magnitude_v",
    "mag_i": "magnitude_i",
    "mag_j": "magnitude_j",
    "mag_h": "magnitude_h",
    "mag_k": "magnitude_k",
    "star_distance": "distance",
    "star_metallicity": "metallicity",
    "star_mass": "mass",
    "star_radius": "radius",
    "star_sp_type": "spec_type",
    "star_age": "age",
    "star_teff": "teff",
    "star_detected_disc": "detected_disc",
    "star_magnetic_field": "magnetic_field",
    "star_distance_error": "distance_error",
    "star_metallicity_error": "metallicity_error",
    "star_mass_error": "mass_error",
    "star_radius_error": "radius_error",
    "star_age_error": "age_error",
    "star_teff_error": "teff_error",
    "star_status": "status",
    "planet_status": "planet_status",
    "mass": "mass_detected_string",
    "mass_sini": "mass_sini_string",
    "radius": "radius_string",
    "orbital_period": "period",
    "semi_major_axis": "axis",
    "k": "K",
    "geometric_albedo": "albedo",
    "semi_major_axis_error": "axis_error",
    "orbital_period_error": "period_error",
    "geometric_albedo_error": "albedo_error",
    "k_error": "K_error",
    "mass_sini_error": "mass_sini_error_string",
    "radius_error": "radius_error_string",
    "mass_error": "mass_detected_error_string",
    "announced_status": "publication_status",
    "publication_type": "type",
    "publication_title": "title",
    "publication_date": "date",
    "publication_url": "url",
    # new
    "planet_created": "created",
    "planet_modified": "modified",
    "star_created": "created",
    "star_modified": "modified",
    "publication_created": "created",
    "publication_modified": "modified",
}

ERROR_FIELDS = [
    "star_distance",
    "star_metallicity",
    "star_mass",
    "star_radius",
    "star_age",
    "star_teff",
    "semi_major_axis",
    "orbital_period",
    "geometric_albedo",
    "k",
    "mass_sini",
    "radius",
    "mass",
    "eccentricity",
    "inclination",
    "omega",
    "tperi",
    "tconj",
    "tzero_tr",
    "tzero_tr_sec",
    "tzero_vr",
    "lambda_angle",
    "impact_parameter",
    "temp_calculated",
]


def _format_data(csv_row: CsvTypedRow_Error) -> DbRow:
    """
    Format a csv data row into the good format for the insertion.

    Args:
        csv_row: A csv row.

    Return:
        The same row in the good format for the insertion with py-linq-sql.

    Examples:
        >>> csv_row = ["a", "b", "c", "4"]
        >>> _format_data(csv_row)
        ('a', 'b', 'c', '4')

        >>> csv_row = ["a", "b", "c", "4", "", "6", "", "h"]
        >>> _format_data(csv_row)
        ('a', 'b', 'c', '4', None, '6', None, 'h')
    """
    return tuple(None if element == "" else element for element in csv_row)


def get_headers_and_data_in_good_format(
    csv_content: CsvTypedData_Error,
) -> tuple[DbColumn, DbData]:
    """
    Get the couple of headers and data in the good format for the insertion.

    Args:
        csv_content: Content of a csv file.

    Returns:
        Headers and Data on the good format for the insertion with py-linq-sql.

    Examples:
        >>> csv_content = [["a", "b", "c"], ["aa", "bb", "cc"], ["aaa", "bbb", "ccc"]]
        >>> get_headers_and_data_in_good_format(csv_content)
        (['a', 'b', 'c'], [('aa', 'bb', 'cc'), ('aaa', 'bbb', 'ccc')])

        >>> csv_content = [["a", "b", "c"], ["aa", "bb", "cc"], ["aaa", "", "ccc"]]
        >>> get_headers_and_data_in_good_format(csv_content)
        (['a', 'b', 'c'], [('aa', 'bb', 'cc'), ('aaa', None, 'ccc')])
    """
    # Ignore type because headers (csv_content[0]) are ALWAYS DbColumn
    return (
        csv_content[0],  # type: ignore[return-value]
        [_format_data(row) for row in csv_content[1:]],
    )


def format_csvdictdata_2_csvdata(content: CsvDictData_Error) -> CsvTypedData_Error:
    """
    Format a CsvDictData in CsvData.

    Args:
        content: data to format.

    Returns:
        Content formatted in CsvData.

    Examples:
        >>> content = [{"a": "aa", "b": "bb"}, {"a": "aaa", "b": "bbb"}]
        >>> format_csvdictdata_2_csvdata(content)
        [['a', 'b'], ['aa', 'bb'], ['aaa', 'bbb']]
    """
    # Insert headers at the begin of the list
    result: CsvTypedData_Error = [list(content[0].keys())]
    result.extend([list(row.values()) for row in content])

    return result


def _get_errors_for_one_obj(obj: CsvDictRow) -> CsvDictRow_Error:
    """
    Get an object with all errors fields in the good format for the database.

    Args:
        obj: Object from which data is taken.

    Returns:
        A copy of obj with the good errors fields.

    Examples:
        With two value
        >>> obj = {
        ...     "star_distance": 12,
        ...     "star_distance_error_min": 0.12,
        ...     "star_distance_error_max": 0.12,
        ... }
        >>> _get_errors_for_one_obj(obj)
        {'star_distance': 12, 'star_distance_error': [0.12, 0.12]}

        With only min
        >>> obj = {
        ...     "star_distance": 12,
        ...     "star_distance_error_min": 0.12,
        ... }
        >>> _get_errors_for_one_obj(obj)
        {'star_distance': 12, 'star_distance_error': [0.12]}

        With only max
        >>> obj = {
        ...     "star_distance": 12,
        ...     "star_distance_error_max": 0.12,
        ... }
        >>> _get_errors_for_one_obj(obj)
        {'star_distance': 12, 'star_distance_error': [0.12]}

        With no value
        >>> obj = {
        ...     "star_distance": 12,
        ...     "star_distance_error_min": None,
        ...     "star_distance_error_max": None,
        ... }
        >>> _get_errors_for_one_obj(obj)
        {'star_distance': 12, 'star_distance_error': None}

    """
    result = cast(CsvDictRow_Error, obj.copy())

    for field in ERROR_FIELDS:
        min_field = f"{field}_error_min"
        max_field = f"{field}_error_max"
        min_value = None
        max_value = None

        if min_field in result.keys() or max_field in result.keys():
            if min_field in result.keys():
                min_value = obj[min_field]
                result.pop(min_field)
            if max_field in result.keys():
                max_value = obj[max_field]
                result.pop(max_field)

            match (min_value, max_value):
                case (None, None):
                    result[f"{field}_error"] = None
                case (_, None):
                    result[f"{field}_error"] = [min_value]
                case (None, _):
                    result[f"{field}_error"] = [max_value]
                case _:
                    result[f"{field}_error"] = [min_value, max_value]

    return result


def get_errors_in_good_format(data: CsvDictData) -> CsvDictData_Error:
    """
    Get objects with all errors fields in the good format for the database.

    Args:
        data: objects form which data is taken.

    Returns:
        A copy of data with the good errors fields.

    Examples:
        >>> data = [
        ...     {
        ...         "star_distance": 12,
        ...         "star_distance_error_min": 0.12,
        ...         "star_distance_error_max": 0.12,
        ...     },
        ...     {
        ...         "star_distance": 24,
        ...         "star_distance_error_min": 0.24,
        ...         "star_distance_error_max": 0.24,
        ...     },
        ... ]
        >>> get_errors_in_good_format(data)
        [
        ...     {'star_distance': 12, 'star_distance_error': [0.12, 0.12]},
        ...     {'star_distance': 24, 'star_distance_error': [0.24, 0.24]}
        ... ]
    """
    return [_get_errors_for_one_obj(obj) for obj in data]


def get_good_fields_names(headers: DbColumn) -> DbColumn:
    """
    Get good headers for the database from a list of headers.

    Args:
        headers: Headers to transform.

    Returns:
        A copy of headers with good names.

    Examples:
        >>> headers = ["star_name", "star_distance", "star_mass"]
        >>> get_good_fields_names(headers)
        ['name', 'distance', 'mass']
    """
    result = []

    for header in headers:
        if header in CATALOG_HEADERS:
            result.append(CATALOG_HEADERS[header])
        else:
            result.append(header)

    return list(dict.fromkeys(result))


# HACK need to embed url in a <a> html link since the django template doesn't
# handle raw link
def format_url(url: str) -> str:
    """
    Format an url for corresponding to the format of url in database.

    In database th url column of publication sotre a <a/> html tag o_O.

    Args:
        url: Url to format.

    Returns:
        Url correctly formatted.

    Examples:
        >>> format_url("http://google.com")
        '<a target= "_blank"  href="http://google.com">website</a>'
    """
    return f'<a target= "_blank"  href="{url}">website</a>'
