"""Functions to format statuses."""

# Standard imports
from typing import cast

# Third party imports
from exochoices import PlanetStatus, PublicationStatuses, WebStatus

# Local imports
from ..utils import CsvDictData, need_to_format


def format_planet_statuses(data: CsvDictData, status: WebStatus) -> None:
    """
    Format status of an entier planets data.

    Args:
        data: Data to format.

    Examples:
        >>> from decimal import Decimal
        >>> data = [
        ...     {
        ...         "planet_status": "Candidate",
        ...         "announced_status": "R"
        ...     },
        ...     {
        ...         "planet_status": "Confirmed",
        ...         "announced_status": "s"
        ...     },
        ...     {
        ...         "announced_status": "c"
        ...     }
        ... ]
        >>> format_planet_statuses(data, WebStatus.ACTIVE)
        >>> print(data)
        [
        ...     {
        ...         'planet_status': Decimal('2'),
        ...         'announced_status': Decimal('1'),
        ...         'status': Decimal('1')
        ...     },
        ...     {
        ...         'planet_status': Decimal('1'),
        ...         'announced_status': Decimal('2'),
        ...         'status': Decimal('1')
        ...     },
        ...     {
        ...         'announced_status': Decimal('3'),
        ...         'status': Decimal('1')
        ...     }
        ... ]
    """
    for obj in data:
        if need_to_format("planet_status", obj):
            obj["planet_status"] = PlanetStatus.from_str(
                cast(str, obj["planet_status"]),
            ).as_decimal

        obj["status"] = status.as_decimal

        obj["announced_status"] = PublicationStatuses.from_str(
            cast(str, obj["announced_status"]),
        ).as_decimal
