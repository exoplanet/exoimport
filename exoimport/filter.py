"""Functions to filter a csv dict from a csv file."""

# Standard imports
import csv
from collections.abc import Callable
from pathlib import Path

# Local imports
from .exceptions import PythonSyntaxError
from .logging import get_logger
from .utils import CsvDictData, useless_track


def input_filter(
    filter_str: str | None,
    content: CsvDictData,
    warning_messages: list[str],
    csv_of_filtered: bool,
    output_csv_file: Path | None = None,
    *,
    track: Callable = useless_track,
) -> CsvDictData:
    """
    Filter the input csv with the filter in the mapping.

    Args:
        filter_str: The filter.
        content: The content to filter.
        warning_messages: All warning messages will be display to user later.
        csv_of_filtered: Boolean to know if we write in a csv all filtered rows.
        track: Function useful if we want display a progress bar in typer.

    Returns:
        The content without the filtered rows.
    """
    exolog = get_logger()
    if not filter_str:
        warning_messages.append(
            "  [bold orange3 on black]No filter, "
            "kept all rows.[/bold orange3 on black]",
        )
        exolog.info("No filter, kept all rows")
        exolog.info("input has been filtered")
        return content

    result = []

    if csv_of_filtered:
        headers = list(content[0].keys())
        csv_file_of_filtered = open(
            output_csv_file,  # type: ignore[arg-type]
            "w",
            encoding="utf-8",
            newline="",
        )
        writer = csv.DictWriter(csv_file_of_filtered, fieldnames=headers)
        writer.writeheader()

    for row in track(content, description="Filtering..."):
        try:
            if eval(  # noqa: PGH001
                filter_str,
                {"builtins": {}},
                row,
            ):
                result.append(row)
            elif csv_of_filtered:
                writer.writerow(row)
        except SyntaxError as err:
            raise PythonSyntaxError(filter_str) from err

    kept_rows = len(result)
    total_rows = len(content)
    filtered_rows = total_rows - kept_rows

    warning_messages.append(
        f"  [bold green on black]{kept_rows} rows[/bold green on black] kept out of "
        f"[white on black]{total_rows}[/white on black], [bold orange3 on black]"
        f"{filtered_rows} rows[/bold orange3 on black] filtered.",
    )
    exolog.info(
        f"{kept_rows} rows kept out of {total_rows}, {filtered_rows} rows filtered",
    )
    exolog.info("input has been filtered")

    return result
