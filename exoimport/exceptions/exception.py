"""Base exception for the project."""


class ExoImportError(Exception):
    """Base exception for the project."""
