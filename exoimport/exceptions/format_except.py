"""Exception related to formatting data."""

# Local imports
from .exception import ExoImportError


class FormatError(ExoImportError):
    """Base exception for the data formatting module."""


class InvalidUrlError(FormatError):
    """Exception raised when an url is invalid."""

    def __init__(self, receive_url: str):
        """Initialize an InvalidUrlError."""
        super().__init__(f"{receive_url} is invalid.")
