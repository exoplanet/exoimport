"""Exception related to validation."""

# Local imports
from .exception import ExoImportError


class ValidateError(ExoImportError):
    """Base exception for the read system."""


class ObjectCharNameError(ValidateError):
    """Exception raised when an object name's contain characters not supported."""

    def __init__(self, name: str) -> None:
        """Initialize an ObjectCharNameError exception."""
        super().__init__(
            f"Invalid name: {name}. Object name's must contain only digits, utf-8 "
            "letters, space, underscore, hyphen, parentheses, simple quote, back quote,"
            " slash, star, colon and square bracket, must start with a one of those "
            "character except the space.",
        )


class ObjectAlphaNameError(ValidateError):
    """Exception raised when an object's name does not contain a alpha character."""

    def __init__(self, name: str) -> None:
        """Initialize an ObjectAlphaNameError exception."""
        super().__init__(
            f"Invalid name: {name}. Object name's must contain at least "
            "one utf-8 letter.",
        )


class ObjectSizeNameError(ValidateError):
    """Exception raised when an object name's is too long.."""

    def __init__(self, name: str, max_size: int) -> None:
        """Initialize an ObjectSizeNameError exception."""
        super().__init__(
            f"Invalid name: {name}. Len is too long. For this object type "
            f"the max size id: {max_size}",
        )
