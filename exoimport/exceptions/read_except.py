"""Exception related to reading a file."""

# Local imports
from .exception import ExoImportError


class ReadError(ExoImportError):
    """Base exception for the read system."""


class NotCsvFileError(ReadError):
    """Exception raised when we try to read a not .csv with `read_csv()`."""

    def __init__(self, file_name: str):
        """Initialize a NotCsvFileError exception."""
        super().__init__(f"The file: {file_name} isn't a csv file.")


class NotYamlFileError(ReadError):
    """Exception raised when we try to read a not .yaml with `read_yaml()`."""

    def __init__(self, file_name: str):
        """Initialize a NotYamlFileError exception."""
        super().__init__(f"The file: {file_name} isn't a yaml file.")


class EmptyFileError(ReadError):
    """Exception raised when we read an empty file."""

    def __init__(self, file_name: str):
        """Initialize an EmptyFileError exception."""
        super().__init__(f"The file {file_name} is empty.")


class InvalidYamlError(ReadError):
    """Exception raised when we read an invalid yaml."""

    def __init__(self, file_name: str):
        """Initialize an InvalidYamlError exception."""
        super().__init__(f"The file {file_name} had a syntax error.")
