"""Exception related to log system."""

# Local imports
from .exception import ExoImportError


class LoggerError(ExoImportError):
    """Base exception for the log module."""


class LoggerAlreadyConfiguredError(LoggerError):
    """Exception raised when we try to configure the logger already configured."""

    def __init__(self) -> None:
        """Initialize a LoggerAlreadyConfiguredError."""
        super().__init__("The logger is already configured. You can't reconfigure it.")


class LoggerAlreadyUsedError(LoggerError):
    """Exception raised when we try to configure the logger after using it."""

    def __init__(self) -> None:
        """Initialize a LoggerAlreadyUsedError."""
        super().__init__("The logger has already been used. You can't configure it.")
