"""Exception related to reading a file."""

# Local imports
from .exception import ExoImportError


class MappingError(ExoImportError):
    """Base exception for the mapping eval system."""


class PythonSyntaxError(MappingError):
    """Exception raised when we have a python syntax error in mapping."""

    def __init__(self, expr_str: str):
        """Initialize a PythonSyntaxError exception."""
        super().__init__(f"Syntax Error in mapping file: {expr_str}")
