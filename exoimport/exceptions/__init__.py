"""Exceptions of the project."""

# Local imports
from .exception import ExoImportError  # noqa: F401
from .format_except import FormatError, InvalidUrlError  # noqa: F401
from .import_except import (  # noqa: F401
    DbImportError,
    ImportInDbError,
    PlanetAlreadyInDbError,
    StarAlreadyInDbError,
)
from .logging_except import (  # noqa: F401
    LoggerAlreadyConfiguredError,
    LoggerAlreadyUsedError,
    LoggerError,
)
from .mapping_except import MappingError, PythonSyntaxError  # noqa: F401
from .read_except import (  # noqa: F401
    EmptyFileError,
    InvalidYamlError,
    NotCsvFileError,
    NotYamlFileError,
    ReadError,
)
from .validate_except import (  # noqa: F401
    ObjectAlphaNameError,
    ObjectCharNameError,
    ObjectSizeNameError,
    ValidateError,
)
