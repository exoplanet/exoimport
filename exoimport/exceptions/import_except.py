"""Exception related to importing in the database."""

# Standard imports
from typing import Any

# Local imports
from .exception import ExoImportError


class ImportInDbError(ExoImportError):
    """Base exception for the import data module."""


class DbImportError(ImportInDbError):
    """Exception raised when something wrong in an import."""

    # No cover because this is a security for an Error in the database.
    def __init__(self, expected: str, receive: int):
        """Initialize an DbImportError."""
        super().__init__(  # pragma: no cover
            "Something was wrong in the import: we tried to insert "
            f"{expected} but received {receive}.",
        )


class StarAlreadyInDbError(ImportInDbError):
    """Exception raised when we try to import a star is alreaady in database."""

    # Used Any for circular import
    def __init__(self, name: str, coord: Any):  # noqa: ANN401
        """Initialize a StarAlreadyInDbError."""
        super().__init__(
            f"The star {name} with coordinates:"
            f"\tra={coord.ra}, dec={coord.dec} is already in the database.",
        )


class PlanetAlreadyInDbError(ImportInDbError):
    """Exception raised when we try to import a planet is alreaady in database."""

    def __init__(self, name: str):
        """Initialize a PlanetAlreadyInDbError."""
        super().__init__(f"The planet {name} is already in the database.")
