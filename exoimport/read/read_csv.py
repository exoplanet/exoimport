"""All function to read a csv file."""

# Standard imports
import csv
from decimal import Decimal, InvalidOperation
from pathlib import Path

# Local imports
from ..exceptions import EmptyFileError, NotCsvFileError
from ..logging import get_logger
from ..utils import CsvData, CsvDictData


def read_csv(file_name: Path) -> CsvData:
    """
    Read a csv file and produce a list row each row being a list of str.

    This is an enhanced version of std lib csv.reader() with more
    detailed error handling and that produce an actual list and not a
    generator.

    Args:
        - file_name : name of the file to be read.

    Returns:
        A list of row. Each row is a list of str values. The first row
        contains the headers (if present).

    Raises:
        NotCsvFileError: Raised if the file isn't a csv file.
        EmptyFileError: Raised if the file is empty.
        FileNotFoundError: Raised if the given file name does not exist.
    """
    result = []

    if file_name.suffix != ".csv":
        raise NotCsvFileError(str(file_name))

    try:
        with open(file_name, newline="", encoding="utf-8") as csv_file:
            reader = csv.reader(csv_file, quotechar="|")
            result = list(reader)

            if not result:
                raise EmptyFileError(str(file_name))

    except FileNotFoundError as exc:
        raise FileNotFoundError() from exc

    get_logger().info(f"{file_name} has been read")
    return result


def _to_decimal_or_str(data: str) -> Decimal | str | None:
    """
    Convert a string to a decimal if this string represent a number.

    If the string doesn't represent a number the string is just returned

    Args:
        - data: data to convert if we can.

    Returns:
        The data convert in decimal if represent a number, otherwise return data.

    Examples:
        >>> _to_decimal_or_str("toto")
        'toto'

        >>> _to_decimal_or_str("12.5978")
        Decimal('12.5978')
    """
    try:
        res = Decimal(data)
    except InvalidOperation:
        return data

    return res


def _replace_empty_str_with_none(rows: CsvDictData) -> None:
    """
    Replace empty strings by None.

    Args:
        - row: A row contains some strings.

    Examples:
        >>> row = [{"a": "aa", "b": "", "c": "cc"}]
        >>> _replace_empty_str_with_none(row)
        >>> print(row)
        [{'a': 'aa', 'b': None, 'c': 'cc'}]
    """
    for row in rows:
        for key, value in row.items():
            if not value and isinstance(value, str):
                row[key] = None


# TODO: Gerer les out of range ici (On a une virgule dans les data. PAS BIEN)
def format_csv(content: CsvData) -> CsvDictData:
    """
    Format a list of list csv representation to a list of dict representation.

    Input format:
        [headers[...], row_1[...], row_2[...], row_3[...]]

    Output format:
        [
            {header[0]: row_1[0], header[1]: row_1[1]},
            {header[0]: row_2[0], header[1]: row_2[1]},
            {header[0]: row_3[0], header[1]: row_3[1]}
        ]

    Args:
        - content: The csv content read from a CSV file.

    Returns:
        The content in the good format.

    Examples:
        >>> import logging as log
        >>> log.disable(log.CRITICAL)
        >>> format_csv(
        ...     [
        ...         ["a", "b", "c"],
        ...         ["aa", "bb", "cc"],
        ...         ["aaa", "bbb", "ccc"],
        ...     ]
        ... )
        [{'a': 'aa', 'b': 'bb', 'c': 'cc'}, {'a': 'aaa', 'b': 'bbb', 'c': 'ccc'}]
        >>> log.disable(log.NOTSET)
    """
    result = []
    headers = content[0]

    csv_rows = content[1:]

    for row in csv_rows:
        res = {}

        for identifier, data in enumerate(row):
            if data is None:
                res[headers[identifier]] = None
            else:
                res[headers[identifier]] = _to_decimal_or_str(data)

        result.append(res)

    _replace_empty_str_with_none(result)
    get_logger().info("csv content has been formatted")
    return result
