"""All function to read a yaml file."""

# Standard imports
from pathlib import Path
from typing import cast

# Third party imports
import yaml

# Local imports
from ..exceptions import EmptyFileError, InvalidYamlError, NotYamlFileError
from ..logging import get_logger


def read_yaml(file_name: Path) -> dict[str, str | None]:
    """
    Read a yaml file.

    Args:
        - file_name : name of the file to be read

    Returns:
        The contained of the yaml file.

    Raises:
        NotYamlFIleError: Raised if the given file isn't a yaml file.
        EmptyFileError: Raised if the file is empty.
        FileNotFoundError: Raised if the given file name does not exist.
    """
    if file_name.suffix not in (".yml", ".yaml"):
        raise NotYamlFileError(str(file_name))

    try:
        with open(file_name, encoding="utf-8") as file:
            loaded_file = yaml.safe_load(file)

        if not loaded_file:

            raise EmptyFileError(str(file_name))

    except FileNotFoundError as exc:
        raise FileNotFoundError() from exc

    except yaml.scanner.ScannerError as exc:
        raise InvalidYamlError(str(file_name)) from exc

    get_logger().info(f"{file_name} has been read")
    return cast(dict[str, str | None], loaded_file)
