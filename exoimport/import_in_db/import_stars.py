"""Functions to import data in star table."""

# Standard imports
from typing import cast

# Third party imports
from psycopg import Connection
from py_linq import Enumerable
from py_linq_sql import SQLEnumerable

# Local imports
from ..exceptions import DbImportError
from ..logging import get_logger
from ..utils import DbColumn, DbData, add_import_id


def _add_import_id(
    stars: tuple[DbColumn, DbData],
    import_id: int,
) -> tuple[DbColumn, DbData]:
    """
    Add foreign keys import_id to stars.

    Args:
        planets: Data of stars to import.
        import_id: Id in db of the import.

    Returns:
        Column and data to import in the database with the import id.

    Examples:
        >>> from decimal import Decimal
        >>> stars = (["cola", "colb"], [("a", "b"), ("aa", "bb")])
        >>> _add_import_id(stars, 1)
        (
        ...     ['cola', 'colb', 'dbimport_id'],
        ...     [
        ...         ('a', 'b', Decimal('1')),
        ...         ('aa', 'bb', Decimal('1'))
        ...     ]
        ... )
    """
    return add_import_id(stars, import_id)


def import_stars(
    connection: Connection,
    table_name: str,
    stars: tuple[DbColumn, DbData],
    *,
    import_id: int,
) -> list[int]:
    """
    Import star(s) in the database.

    Args:
        connection: A connection to a database.
        table_name: Name of the table to insert.
        stars: Data of stars to import.

    Returns:
        Id(s) of imported star(s).
    """
    column, data = _add_import_id(stars, import_id)

    record = SQLEnumerable(connection, table_name).insert(column, data).execute()

    number_of_stars_imported = cast(int, record)

    # No cover because this is a security for an Error in the database.
    if number_of_stars_imported <= 0:
        raise DbImportError(">0", number_of_stars_imported)  # pragma: no cover

    check = (
        SQLEnumerable(connection, table_name)
        .select(lambda x: x.id)
        .take_last(number_of_stars_imported)
        .execute()
    )

    get_logger().info("stars has been imported")
    return [element[0] for element in cast(Enumerable, check).to_list()]
