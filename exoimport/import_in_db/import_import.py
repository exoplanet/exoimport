"""Functions to import data in table import table."""

# Standard imports
import datetime
import secrets
import string
from pathlib import Path
from typing import cast

# Third party imports
from psycopg import Binary, Connection
from py_linq_sql import SQLEnumerable

# First party imports
from exoimport import __version__ as exoimport_version

# Local imports
from ..exceptions import DbImportError
from ..logging import get_logger
from ..read.read_csv import read_csv
from ..utils import CsvRow


def _is_empty_line(line: CsvRow) -> bool:
    """
    Define if a CsvRow contains only empty data.

    Args:
        line: Line to analyze.

    Returns:
        True if all element in line are empty, False otherwise.
    """
    return all(element == "" for element in line)


def _internal_is_empty(internal_csv_path: Path) -> bool:
    """
    Define if all CsvRow of a file contains only empty data.

    Args:
        internal_csv_path: File to analyze.

    Returns:
        True if all element of all line are empty, False otherwise.
    """
    content = read_csv(internal_csv_path)
    return any(_is_empty_line(line) for line in content[1:])


def _get_random_string(number: int) -> str:
    """
    Get a random string.

    See:
        https://stackoverflow.com/questions/2257441/
        random-string-generation-with-upper-case-letters-and-digits/23728630#23728630

    Args:
        number: The size of the return string.

    Returns:
        A Random string with a size equal to number.

    Examples:
        Give a random string of size 'number'
        >>> toto = _get_random_string(30)
        >>> len(toto) == 30
        True

        All random generated string are different
        >>> toto = [_get_random_string(30) for _ in range(1000)]
        >>> len(set(toto)) == len(toto)
        True
    """
    return "".join(
        secrets.choice(string.ascii_uppercase + string.digits) for _ in range(number)
    )


def _get_import_name(external_csv: Path | None) -> str:
    """
    Get the name of an import.

    If the external_csv file was not given we generate a string formatted like:
    "import_name_auto_gen" + a uniquerandomly generated 30 character string.

    Else, the name of the import is the name of the external csv file.

    Args:
        external_csv: Path of the external csv or None.

    Returns:
        Name of the import.

    """
    if external_csv:
        return external_csv.stem

    return f"import_name_autogen_{_get_random_string(30)}"


# TODO: Quand py-linq-sql aura changé et permettra d'insérer des fichiers (bytea),
# passer par py_linq_sql.SQLEnumerable.insert()
def import_table_import(
    connection: Connection,
    table_name: str,
    import_data: dict[str, Path],
) -> int | None:
    """
    Import an import in the database.

    Args:
        connection: A connection to a database.
        table_name: Name of the table to insert.
        import_data: Data to import containing:
            Path of the csv given by the user to exoimport translate.
            Path of the mapping given by the user to exoimport translate.
            Path of the csv produce by exoimport translate.

    Returns:
        Id of the import in the database.
    """
    internal_is_empty = _internal_is_empty(import_data["internal_csv"])

    external_csv_content, mapping_yaml_content = None, None

    if import_data["external_csv"]:
        with open(import_data["external_csv"], "rb") as external_csv_file:
            external_csv_content = external_csv_file.read()

    if import_data["mapping_yaml"]:
        with open(import_data["mapping_yaml"], "rb") as mapping_yaml_file:
            mapping_yaml_content = mapping_yaml_file.read()

    with open(import_data["internal_csv"], "rb") as internal_csv_file:
        internal_csv_content = internal_csv_file.read()

    import_name = _get_import_name(import_data["external_csv"])

    cursor = connection.cursor()
    # TODO: Remove now and let the db manage date when we modified the db and django.
    cursor.execute(
        f"INSERT INTO {table_name}(name, external_csv, mapping_yaml, internal_csv, "
        "exoimport_version, created) "
        "VALUES (%s, %s, %s, %s, %s, %s)",
        (
            import_name,
            Binary(external_csv_content),
            Binary(mapping_yaml_content),
            Binary(internal_csv_content),
            exoimport_version,
            datetime.datetime.now(),
        ),
    )

    import_record = cursor.rowcount

    # No cover because this is a security for an Error in the database.
    if import_record != 1:
        raise DbImportError("1", import_record)  # pragma: no cover

    if internal_is_empty:
        return None

    get_logger().info("import infos has been imported")
    return cast(
        int,
        (
            SQLEnumerable(connection, table_name)
            .select(lambda x: x.id)  # type: ignore[attr-defined, no-any-return, index]
            .take_last(1)
            .execute()
        )[0][0],
    )
