"""Functions to import data in table of the database."""

# Standard imports
import datetime
from decimal import Decimal
from pathlib import Path
from typing import cast

# Third party imports
from exochoices import WebStatus
from psycopg import Connection

# Local imports
from ..config import Configuration
from ..exceptions import StarAlreadyInDbError
from ..format.format import (
    format_publications,
    format_to_import,
    format_to_import_publications,
)
from ..logging import get_logger
from ..similarity import StarRaDec, is_star_already_in_db
from ..split import group_planets_by_star_index
from ..utils import (
    CsvDictData,
    DbColumn,
    DbData,
    ImportedLinks,
    ImportedSystems,
    ImportSystemRecord,
)
from .import_alternate_names import (
    import_planets_alternate_name,
    import_star_alternate_names,
)
from .import_import import import_table_import
from .import_link_publication import link_to_ids
from .import_planet2star import import_planet2star
from .import_planets import import_planets
from .import_publication import import_publication
from .import_stars import import_stars


def _import_one_system(
    conn: Connection,
    import_id: int,
    stars_planets_others: list[CsvDictData],
    index_of_star: int,
    config: Configuration,
) -> ImportSystemRecord:
    """
    Import a system with a star and planets.

    Import a star and his alternate names.
    Afterwards import his planets and alternate names of these planets and
    planet2star links.

    Args:
        conn: Connection to a database.
        import_id: Id in database of the import.
        stars_planets_others: Stars of this import, planets of this system, All other
            parameters for the import including the alternate names.
        index_of_star: Index of star of this system in `stars`.
        config: Configuration for the import.

    Returns:
        Record of the system import with star id, planet ids, star alternate name id and
        planet alternate name ids.
    """
    stars, planets, others = (
        stars_planets_others[0],
        stars_planets_others[1],
        stars_planets_others[2],
    )
    star_to_import = stars[index_of_star]
    formated_star = format_to_import([star_to_import])
    formated_planets = format_to_import(planets)

    star_to_check = (
        str(star_to_import["star_name"]),
        StarRaDec(
            cast(Decimal, star_to_import["ra"]),
            cast(Decimal, star_to_import["dec"]),
        ),
    )

    if is_star_already_in_db(conn, star_to_check, config.epsilon_ra_dec, config.tables):
        raise StarAlreadyInDbError(star_to_check[0], star_to_check[1])

    id_of_star = import_stars(
        conn,
        config.tables.star_table,
        formated_star,
        import_id=import_id,
    )[0]

    star_alternate_names = others[index_of_star]["star_alternate_names"]

    id_of_star_alter_name = []
    if star_alternate_names:
        id_of_star_alter_name = import_star_alternate_names(
            conn,
            id_of_star,
            cast(str, star_alternate_names),
            config.tables.alternate_star_name_table,
        )

    ids_of_planets = import_planets(
        conn,
        config.tables,
        formated_planets,
        import_id=import_id,
        main_star_id=id_of_star,
    )

    id_of_planets_alter_name = import_planets_alternate_name(
        conn,
        ids_of_planets,
        others,
        config.tables.planet_table,
        config.tables.alternate_planet_name_table,
    )

    import_planet2star(
        conn,
        config.tables.planet2star_table,
        id_of_star,
        ids_of_planets,
    )

    get_logger().info(
        f"The systems with the star: {id_of_star} "
        f"and planets: {ids_of_planets} has been imported",
    )
    return ImportSystemRecord(
        star_id=id_of_star,
        planets_id=ids_of_planets,
        star_alter_id=id_of_star_alter_name,
        planets_alter_id=id_of_planets_alter_name,
    )


def _get_other_with_planet_name(
    planets: CsvDictData,
    others: CsvDictData,
) -> CsvDictData:
    """
    Get a copy of other with the name of planet in each row.

    The first element of planets correspodn to the first element of other, etc...

    Args:
        planets: All planets of the import.
        others: All other parameters for the import.

    Returns:
        A copy of other worth the correct planet name if each row.

    Examples:
        >>> planets = [{"name": "toto", "mass": 12}, {"name": "titi", "mass": 24}]
        >>> others = [{"col_A": "a", "col_B": "b"}, {"col_A": "aa", "col_B": "bb"}]
        >>> _get_other_with_planet_name(planets, others)
        [
        ...     {'col_A': 'a', 'col_B': 'b', 'planet_name': 'toto'},
        ...     {'col_A': 'aa', 'col_B': 'bb', 'planet_name': 'titi'}
        ... ]
    """
    result = others.copy()

    for index, planet in enumerate(planets):
        result[index]["planet_name"] = planet["name"]

    return result


# TODO: Prendre en charge les planetes flottantes!
def import_rogue_planets() -> None:
    """Import rogue planets."""
    raise NotImplementedError()


# Don't forgot to format planet before with`format_planets`
# and format star with `format_stars`
def import_systems(
    conn: Connection,
    import_data: dict[str, Path],
    stars_planets_other: list[CsvDictData],
    config: Configuration,
) -> ImportedSystems:
    """
    Import systems of the import.

    We import system one by one with the star, alternate names of this stars,
    his planets and alternate names of these planets.

    Args:
        conn: Connection to a database.
        import_data: Data to insert in the import table from config.
        stars_planets_other: All stars of this import, all planets of this import, All
            other parameters for the import including the alternate names.
        config: Configuration for the import.

    Returns:
        Id of this import in the database and all Record produce by _import_one_system
            which contains id of planets group by star's id.
    """
    systems = []
    stars, planets, others = (
        stars_planets_other[0],
        stars_planets_other[1],
        stars_planets_other[2],
    )
    import_id = cast(
        int, import_table_import(conn, config.tables.import_table, import_data)
    )

    grouped_planets_by_stars = group_planets_by_star_index(stars, planets)

    others_w_planet_name = _get_other_with_planet_name(planets, others)

    for star_index, planets_of_star in grouped_planets_by_stars.items():
        systems.append(
            _import_one_system(
                conn,
                import_id,
                [stars, planets_of_star, others_w_planet_name],
                star_index,
                config,
            ),
        )

    get_logger().info("All systems has been imported")
    return import_id, systems


def _get_publication(
    others: CsvDictData,
    status: WebStatus,
) -> dict[str, str | Decimal]:
    """
    Get a publication correctly formatted form other parameters.

    Args:
        others: Others parameters.

    Returns:
        All publications information.

    Examples:
        >>> import logging as log
        >>> log.disable(log.CRITICAL)
        >>> others = [dict(
        ...     toto = "titi",
        ...     publication_title = "Tess",
        ...     publication_date = Decimal('2022'),
        ...     publication_url = "http://www.website.com",
        ...     publication_type = "book",
        ...     publication_created = "04/04/2023",
        ...     publication_modified = "04/04/2023"
        ... )]
        >>> _get_publication(others, WebStatus.ACTIVE)
        ... {
        ...     'publication_title': 'Tess',
        ...     'publication_date': Decimal('2022'),
        ...     'publication_url': 'http://www.website.com',
        ...     'publication_type': Decimal('1'),
        ...     'publication_created': "04/04/2023",
        ...     'publication_modified': "04/04/2023"
        ... }
        >>> log.disable(log.NOTSET)
    """
    first = others[0]
    # TODO: Virer le now quand la base django aura le default
    pub = [
        {
            "publication_title": first["publication_title"],
            "publication_date": first["publication_date"],
            "publication_url": first["publication_url"],
            "publication_type": first["publication_type"],
            "publication_created": first.get(
                "publication_created", str(datetime.datetime.now())
            ),
            "publication_modified": first.get(
                "publication_modified", str(datetime.datetime.now())
            ),
        },
    ]

    format_publications(pub, status)
    return cast(dict[str, str | Decimal], format_to_import_publications(pub))


def _import_publication_in_db(
    connection: Connection,
    import_id: int,
    others: CsvDictData,
    config: Configuration,
) -> int:
    """
    Import a publication in database.

    Args:
        connection: Connection to a database.
        import_id: Id of the current import.
        others: All other parameters for the import including publication parameters.

    Returns:
        Id of the imported publication.
    """
    publication = _get_publication(others, config.web_statuses)

    pub_id = import_publication(
        connection,
        config.tables.publication_table,
        cast(tuple[DbColumn, DbData], publication),
        import_id=import_id,
    )

    return pub_id[0]


def import_and_link_publication(
    connection: Connection,
    others: CsvDictData,
    imported_systems: ImportedSystems,
    config: Configuration,
) -> tuple[int, list[ImportedLinks]]:
    """
    Import a publication and link it to all parameters of stars and planets.

    Args:
        connection: Connection to a database.
        others: All other parameters for the import including publication parameters.
        imported_systems: Ids of all objects in systems and the id of the import.

    Returns:
        Id of the publication int the database and all id of links in the database.
    """
    import_id = imported_systems[0]
    systems = imported_systems[1]

    pub_id = _import_publication_in_db(connection, import_id, others, config)
    links = link_to_ids(connection, systems, pub_id, config)

    get_logger().info("publication has been imported and linked with objects")
    return (pub_id, links)
