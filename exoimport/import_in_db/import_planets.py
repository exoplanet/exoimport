"""Functions to import data in planets table."""

# Standard imports
from decimal import Decimal
from typing import cast

# Third party imports
from psycopg import Connection
from py_linq import Enumerable
from py_linq_sql import SQLEnumerable

# Local imports
from ..config import Tables
from ..exceptions import DbImportError, PlanetAlreadyInDbError
from ..logging import get_logger
from ..similarity import is_planet_already_in_db
from ..utils import DbColumn, DbData


def _add_import_and_star_id(
    planets: tuple[DbColumn, DbData],
    import_id: int,
    main_star_id: int,
) -> tuple[DbColumn, DbData]:
    """
    Add foreign keys import_id and main_star_id to planets.

    Args:
        planets: Data of planets to import.
        import_id: Id in db of the import.
        main_star_id: Id in db of main star.

    Returns:
        Column and data to import in the database with the import id and main star id.

    Examples:
        >>> planets = (["cola", "colb"], [("a", "b"), ("aa", "bb")])
        >>> _add_import_and_star_id(planets, 1, 2)
        (
        ...     ['cola', 'colb', 'dbimport_id', 'main_star_id'],
        ...     [
        ...         ('a', 'b', Decimal('1'), Decimal('2')),
        ...         ('aa', 'bb', Decimal('1'), Decimal('2'))
        ...     ]
        ... )
    """
    columns = planets[0]
    data = planets[1]

    columns.extend(["dbimport_id", "main_star_id"])

    new_data = []

    for row in data:
        res = (*row, Decimal(import_id), Decimal(main_star_id))
        new_data.append(res)

    return (columns, new_data)


def _assert_not_all_planet_already_in_db(
    connection: Connection,
    planets: tuple[DbColumn, DbData],
    config_tables: Tables,
) -> None:
    """
    Check if a planet is already in db.

    Args:
        connection: A connection to a database.
        planets: Data of planets to check.
        config_tables: Name of all tables from config file.

    Raises:
        PlanetAlreadyInDbError: If a planet is already in the db.
    """
    name_index = planets[0].index("name")
    names = [data[name_index] for data in planets[1]]

    for name in names:
        if is_planet_already_in_db(connection, cast(str, name), config_tables):
            raise PlanetAlreadyInDbError(cast(str, name))


def import_planets(
    connection: Connection,
    config_tables: Tables,
    # table_name: str,
    planets: tuple[DbColumn, DbData],
    *,
    import_id: int,
    main_star_id: int,
) -> list[int]:
    """
    Import planet(s) in the database.

    Args:
        connection: A connection to a database.
        table_name: Name of the table to insert.
        planets: Data of planets to import.
        import_id: Id in db of the import.
        main_star_id: Id in db of main star.

    Returns:
        Id(s) of imported planet(s).
    """
    column, data = _add_import_and_star_id(planets, import_id, main_star_id)

    _assert_not_all_planet_already_in_db(connection, planets, config_tables)

    record = (
        SQLEnumerable(connection, config_tables.planet_table)
        .insert(column, data)
        .execute()
    )

    number_of_imported_planets = cast(int, record)

    # No cover because this is a security for an Error in the database.
    if number_of_imported_planets <= 0:
        raise DbImportError(">0", number_of_imported_planets)  # pragma: no cover

    check = (
        SQLEnumerable(connection, config_tables.planet_table)
        .select(lambda x: x.id)
        .take_last(number_of_imported_planets)
        .execute()
    )

    get_logger().info("planets has been imported")
    return [element[0] for element in cast(Enumerable, check).to_list()]
