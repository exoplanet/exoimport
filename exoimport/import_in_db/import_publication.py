"""Functions to import data in publication table."""

# Standard imports
from typing import cast

# Third party imports
from psycopg import Connection
from py_linq import Enumerable
from py_linq_sql import SQLEnumerable

# Local imports
from ..exceptions import DbImportError
from ..logging import get_logger
from ..utils import DbColumn, DbData, add_import_id


def _add_import_id(
    publications: tuple[DbColumn, DbData],
    import_id: int,
) -> tuple[DbColumn, DbData]:
    """
    Add foreign keys import_id to a publication.

    Args:
        publications: Data of publications to import.
        import_id: Id in db of the import.

    Returns:
        Column and data to import in the database with the import id .

    Examples:
        >>> from decimal import Decimal
        >>> pubs = (["cola", "colb"], [("a", "b"), ("aa", "bb")])
        >>> _add_import_id(pubs, 1)
        (
        ...     ['cola', 'colb', 'dbimport_id'],
        ...     [
        ...         ('a', 'b', Decimal('1')),
        ...         ('aa', 'bb', Decimal('1'))
        ...     ]
        ... )
    """
    return add_import_id(publications, import_id)


def import_publication(
    connection: Connection,
    table_name: str,
    publications: tuple[DbColumn, DbData],
    *,
    import_id: int,
) -> list[int]:
    """
    Import publication(s) in the database.

    Args:
        connection: A connection to a database.
        table_name: Name of the table to insert.
        publications: Data of publications to import.

    Returns:
        Id(s) of imported publication(s).
    """
    column, data = _add_import_id(publications, import_id)

    record = SQLEnumerable(connection, table_name).insert(column, data).execute()

    number_of_imported_publication = cast(int, record)

    # No cover because this is a security for an Error in the database.
    if number_of_imported_publication <= 0:
        raise DbImportError(">0", number_of_imported_publication)  # pragma: no cover

    check = (
        SQLEnumerable(connection, table_name)
        .select(lambda x: x.id)
        .take_last(number_of_imported_publication)
        .execute()
    )

    get_logger().info("publication has been imported")
    return [element[0] for element in cast(Enumerable, check).to_list()]
