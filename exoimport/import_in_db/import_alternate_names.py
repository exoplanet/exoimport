"""Functions to import data in alternate names tables."""
# Standard imports
from typing import cast

# Third party imports
from psycopg import Connection
from py_linq import Enumerable
from py_linq_sql import PyLinqSqlInsertType, SQLEnumerable

# Local imports
from ..exceptions import DbImportError
from ..logging import get_logger
from ..utils import CsvDictData


def import_star_alternate_names(
    conn: Connection,
    star_id: int,
    star_alter_names: str,
    table_name: str,
) -> list[int]:
    """
    Import all alternate names for a star.

    Args:
        conn: Connection to the database.
        star_id: Id of the star corresponding to the alternate names.
        star_alter_names: Alternate names separate by ';'.
        table_name: Table name of the star alternate names.

    Returns:
        All id of imported alternate names.
    """
    data = [(star_id, name) for name in star_alter_names.split(";")]

    record = (
        SQLEnumerable(conn, table_name)
        .insert(
            ["star_id", "name"],
            cast(PyLinqSqlInsertType, data),
        )
        .execute()
    )

    number_of_imported_alter_names = cast(int, record)

    # No cover because this is a security for an Error in the database.
    if number_of_imported_alter_names <= 0:
        raise DbImportError(">0", number_of_imported_alter_names)  # pragma: no cover

    check = (
        SQLEnumerable(conn, table_name)
        .select(lambda x: x.id)
        .take_last(number_of_imported_alter_names)
        .execute()
    )

    get_logger().info("star alternate names has been imported")
    return [element[0] for element in cast(Enumerable, check).to_list()]


def _get_planet_alternate_name(planet_name: str, others: CsvDictData) -> str | None:
    """
    Get altenate names of a planet.

    Args:
        planet_name: Name of the planet of which we want the altenate names.
        others: All other parameters for the import including the alternate names.

    Returns:
        Alternate names of a planet if exist, None otherwise.

    Examples:
        >>> others = [
        ...     {"A": "a", "alternate_names": "toto01;Toto01", "planet_name": "toto"},
        ...     {"A": "aa", "alternate_names": "titi01;Titi01", "planet_name": "titi"},
        ...     {"A": "aa", "alternate_names": "tutu01;Tutu01", "planet_name": "tutu"},
        ... ]

        >>> _get_planet_alternate_name(planet_name="titi", others=others)
        'titi01;Titi01'

        >>> _get_planet_alternate_name(planet_name="tata", others=others)
        None
    """
    for other in others:
        if other["planet_name"] == planet_name:
            return cast(str, other["alternate_names"])

    return None


def _import_one_planet_alternate_names(
    conn: Connection,
    planet_id: int,
    planet_alter_names: str,
    table_name: str,
) -> list[int]:
    """
    Import all alternate names for a planet.

    Args:
        conn: Connection to the database.
        planet_id: Id of the planet link to alternate names.
        planet_alter_names: Alternate names separate by ';'.
        table_name: Name of the planet alternate name table.

    Returns:
        All id of imported alternate names.
    """
    data = [(planet_id, name) for name in planet_alter_names.split(";")]

    record = (
        SQLEnumerable(conn, table_name)
        .insert(["planet_id", "name"], cast(PyLinqSqlInsertType, data))
        .execute()
    )

    number_of_imported_alter_names = cast(int, record)

    # No cover because this is a security for an Error in the database.
    if number_of_imported_alter_names <= 0:
        raise DbImportError(">0", number_of_imported_alter_names)  # pragma: no cover

    check = (
        SQLEnumerable(conn, table_name)
        .select(lambda x: x.id)
        .take_last(number_of_imported_alter_names)
        .execute()
    )

    return [element[0] for element in cast(Enumerable, check).to_list()]


def import_planets_alternate_name(
    conn: Connection,
    planets_id: list[int],
    others: CsvDictData,
    planet_table_name: str,
    alter_planet_table_name: str,
) -> list[list[int]]:
    """
    Import all alternate names for one or many planets.

    Args:
        conn: Connection to the database.
        planets_id: Ids of the planets link to alternate names.
        others: All other parameters for the import including the alternate names.
        planet_table_name: Name of the planet table.
        alter_planet_table_name: Name of the planet alternate name table.

    Returns:
        All id of imported alternate names.

    """
    result = []

    for planet_id in planets_id:
        record_pl_name = (
            SQLEnumerable(conn, planet_table_name)
            .select(lambda x: x.name)
            .where(lambda x, pl_id=planet_id: x.id == pl_id)  # type: ignore[misc]
            .execute()
        )

        # Ignore type because the typing of py_linq is ...
        planet_name = cast(str, record_pl_name.first().name)  # type: ignore[union-attr]

        planet_alternate_name = _get_planet_alternate_name(planet_name, others)

        if planet_alternate_name:
            result.append(
                _import_one_planet_alternate_names(
                    conn,
                    planet_id,
                    planet_alternate_name,
                    alter_planet_table_name,
                ),
            )

    get_logger().info("planet alternate names has been imported")
    return result
