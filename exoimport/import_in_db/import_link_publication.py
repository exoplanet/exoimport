"""Functions to import a link between a object and a publication."""

# Standard imports
import datetime
from typing import cast

# Third party imports
from exochoices import Plan2PubParam, Star2PubParam
from psycopg import Connection
from py_linq import Enumerable
from py_linq_sql import PyLinqSqlInsertType, SQLEnumerable

# Local imports
from ..config import Configuration, Tables
from ..exceptions import DbImportError
from ..logging import get_logger
from ..utils import ImportedLinks, ImportSystemRecord


def _link_to_object(  # noqa: PLR0913
    connection: Connection,
    obj_id: int,
    pub_id: int,
    table_name: str,
    parameter: Star2PubParam,
    config_tables: Tables,
) -> list[int]:
    """
    Import a link between an object (planet or star) to a publication.

    Args:
        connection: A connection to a database.
        obj_id: Id of the object in the database.
        pub_id: Id of the publication in the database.
        table_name: Name of the table to insert.
        parameter: Parameter of the object the post is dict()alking about.
        config_tables: Name of all tables from config file.

    Returns:
        Id of imported link.
    """
    if table_name == config_tables.star2publication_table:
        col_name_obj_id = "star_id"
    else:
        col_name_obj_id = "planet_id"

    column = [
        "publication_id",
        col_name_obj_id,
        "parameter",
        "status",
        "created",
        "modified",
    ]
    data = (
        pub_id,
        obj_id,
        parameter.as_int,
        1,
        str(datetime.datetime.now()),
        str(datetime.datetime.now()),
    )  # The web status is "active" so '1'

    record = (
        SQLEnumerable(connection, table_name)
        .insert(column, cast(PyLinqSqlInsertType, data))
        .execute()
    )

    number_of_imported_link = cast(int, record)

    # No cover because this is a security for an Error in the database.
    if number_of_imported_link <= 0:
        raise DbImportError(">=0", number_of_imported_link)  # pragma: no cover

    check = (
        SQLEnumerable(connection, table_name)
        .select(lambda x: x.id)
        .take_last(1)
        .execute()
    )

    return [element[0] for element in cast(Enumerable, check).to_list()]


def _link_to_planet(
    connection: Connection,
    planet_id: int,
    pub_id: int,
    config_tables: Tables,
) -> dict[str, int]:
    """
    Import a link between a publication and all parameters of a planet.

    Args:
        connection: A connection to a database.
        planet_id: Id of the planet in the database.
        pub_id: Id of the publication in the database.
        config_tables: Name of all tables from config file.

    Returns:
        Ids of imported links.
    """
    result = {}

    for pub_param in list(Plan2PubParam):
        result[pub_param.as_str] = _link_to_object(
            connection,
            planet_id,
            pub_id,
            config_tables.planet2publication_table,
            pub_param,
            config_tables,
        )[0]

    return result


def _link_to_star(
    connection: Connection,
    star_id: int,
    pub_id: int,
    config_tables: Tables,
) -> dict[str, int]:
    """
    Import a link between a publication and all parameters of a star.

    Args:
        connection: A connection to a database.
        star_id: Id of the star in the database.
        pub_id: Id of the publication in the database.
        config_tables: Name of all tables from config file.

    Returns:
        Ids of imported links.
    """
    result = {}

    for pub_param in list(Star2PubParam):
        result[pub_param.as_str] = _link_to_object(
            connection,
            star_id,
            pub_id,
            config_tables.star2publication_table,
            pub_param,
            config_tables,
        )[0]

    return result


def _link_to_system(
    connection: Connection,
    system: ImportSystemRecord,
    pub_id: int,
    config: Configuration,
) -> ImportedLinks:
    """
    Import a link between a publi and all parameters of a planet & star in a system.

    Args:
        connection: A connection to a database.
        system: Ids of star and planets in the database.
        pub_id: Id of the publication in the database.
        config: Configuration from config file.

    Returns:
        Ids of imported links.
    """
    star_id = system[0]
    planet_ids = system[1]

    star2pub_id = _link_to_star(
        connection,
        star_id,
        pub_id,
        config.tables,
    )
    planet_2pub_ids = []

    for pl_id in planet_ids:
        planet_2pub_ids.append(
            _link_to_planet(
                connection,
                pl_id,
                pub_id,
                config.tables,
            )
        )

    return (star2pub_id, planet_2pub_ids)


def link_to_ids(
    connection: Connection,
    systems: list[ImportSystemRecord],
    pub_id: int,
    config: Configuration,
) -> list[ImportedLinks]:
    """
    Import a link between a publi and all parameters of a planet & star in all system.

    Args:
        connection: A connection to a database.
        systems: Ids of stars and planets group by systems in the database.
        pub_id: Id of the publication in the database.
        config: Configuration from config file.

    Returns:
        Ids of imported links.
    """
    result = []

    for system in systems:
        result.append(_link_to_system(connection, system, pub_id, config))

    get_logger().info("publication and objects has been linked")
    return result
