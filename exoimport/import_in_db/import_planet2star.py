"""Functions to import data in planet2star table."""

# Standard imports
from typing import cast

# Third party imports
from psycopg import Connection
from py_linq import Enumerable
from py_linq_sql import PyLinqSqlInsertType, SQLEnumerable

# Local imports
from ..exceptions import DbImportError
from ..logging import get_logger


def import_planet2star(
    conn: Connection,
    table: str,
    star_id: int,
    ids_of_planets: list[int],
) -> list[int]:
    """
    Import star2star in the database.

    Args:
        connection: A connection to a database.
        table: Name of the table to insert.
        star_id: Id of the star th make the link in the db.
        ids_of_planets: Ids of planets to make all links.

    Returns:
        Id(s) of imported link(s).
    """
    data = [(pl_id, star_id) for pl_id in ids_of_planets]
    record = (
        SQLEnumerable(conn, table)
        .insert(["planet_id, star_id"], cast(PyLinqSqlInsertType, data))
        .execute()
    )

    number_of_imported_p2s_links = cast(int, record)

    # No cover because this is a security for an Error in the database.
    if number_of_imported_p2s_links != len(ids_of_planets):
        raise DbImportError(  # pragma: no cover
            f"={len(ids_of_planets)}", number_of_imported_p2s_links
        )

    check = (
        SQLEnumerable(conn, table)
        .select(lambda x: x.id)
        .take_last(number_of_imported_p2s_links)
        .execute()
    )

    get_logger().info("planets has been imported")
    return [element[0] for element in cast(Enumerable, check).to_list()]
