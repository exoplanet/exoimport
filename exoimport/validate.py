"""Functions to validate csv file and yaml mapping file."""

# Standard imports
import re
from collections.abc import Callable
from math import *  # noqa: F403
from pathlib import Path
from typing import cast

# Third party imports
import regex

# Local imports
from .exceptions import (
    InvalidUrlError,
    ObjectAlphaNameError,
    ObjectCharNameError,
    ObjectSizeNameError,
    PythonSyntaxError,
)
from .logging import get_logger
from .read.read_yaml import read_yaml
from .utils import CsvData, CsvDictData, is_url, useless_track

# Regex pattern for header of csv file
_HEADER_NAME_PATTERN = re.compile(r"^[_a-zA-Z][0-9a-zA-Z_]*$")

# Regex for name pattern in db

# Pattern check if all character is valid and if we don't begin with a space
_OBJECT_NAME_PATTERN = (
    r"^[0-9\p{Alphabetic}-_.()':/`\[\]*]+[0-9\p{Alphabetic}-+_.()':/`\[\]* ]*$"
)

# Pattern check if we have at lease one utf-8 lettre in the name
_OBJECT_LETTER_PATTERN = r"^.*[\p{Alphabetic}]+.*$"


def it_is_empty_str_err(err: Exception) -> bool:
    """
    Check if the given exception is a "bad operand exception on NoneType".

    Args:
        - err: an exception to analyze.

    Returns:
        True if begin with 'bad operand type' and finish by 'NoneType',
            False otherwise.
    """
    return str(err).startswith("bad operand type") and str(err).endswith("'NoneType'")


def validate_csv_headers(
    content: CsvData,
    warning_messages: list[str],
    *,
    track: Callable = useless_track,
) -> bool:
    """
    Validate csv headers syntax.

    Args:
        content: Csv content.
        warning_messages: All warning messages will be display to user later.
        track: Function useful if we want display a progress bar in typer.

    Returns:
        True if all headers have been validated, False otherwise.
    """
    exolog = get_logger()
    headers = content[0]

    warning = []

    for element in track(headers, description="Validating headers..."):
        if not _HEADER_NAME_PATTERN.match(element):
            msg = f'\t-The header "{element}" is not a valid header.'
            warning.append(msg)
            exolog.warning(msg)

    if len(warning) > 0:
        warning_messages.extend(
            [
                "  [bold orange3 on black] CSV errors: [/bold orange3 on black]",
                f"  {len(warning)} header(s) are invalid headers.",
            ],
        )
        warning.append("\n")
        warning_messages.extend(warning)
        exolog.warning(f"{len(warning)} header(s) are invalid")
        return False

    exolog.info("csv headers has been validated")
    return True


def val(value: str) -> str:
    """
    Return the same value.

    This function is useful to accept absolute string values in mapping.

    Args:
        - value: a value string.

    Returns:
        The value in args.

    Examples:
        >>> val("toto")
        'toto'
    """
    return value


def _is_empty_mapping(
    mapping: dict[str, str | None],
    warning_messages: list[str],
) -> bool:
    """
    Define if all field in mapping are empty or not.

    When we read a yaml file, empty fields are None in the dictionary.

    Args:
        mapping: Mapping to analyze.
        warning_messages: All warning messages will be display to user later.

    Returns:
        True if all fields are empty, False otherwise.

    Examples:
        >>> import logging as log
        >>> log.disable(log.CRITICAL)
        >>> mapping = {"a": "aa", "b": "bb"}
        >>> _is_empty_mapping(mapping, [])
        False

        >>> mapping = {"a": "aa", "b": None}
        >>> _is_empty_mapping(mapping, [])
        False

        >>> mapping = {"a": None, "b": None}
        >>> _is_empty_mapping(mapping, [])
        True
        >>> log.disable(log.NOTSET)
    """
    copy = mapping.copy()

    if "filter" in copy:
        copy.pop("filter")

    if not any(copy.values()):
        warning_messages.extend(
            [
                "  [bold orange3 on black] Mapping Yaml errors: "
                "[/bold orange3 on black]",
                "\tThis mapping file is [bold red on black]empty[/bold red on black], "
                "all field are equal to None.\n",
            ],
        )
        get_logger().warning("The mapping file is empty, all filed are equal to None")
        return True

    return False


def validate_mapping_yaml(
    mapping: dict[str, str | None],
    csv_rows: CsvDictData,
    warning_messages: list[str],
    *,
    track: Callable = useless_track,
) -> bool:
    """
    Validate yaml mapping.

    Args:
        mapping: The mapping read from a yaml mapping file.
        csv_rows: The csv rows formatted in list of dictionaries,
            used to validate the mapping.
        warning_messages: All warning messages will be display to user later.
        track: Function useful if we want display a progress bar in typer.

    Returns:
        False if all formula have not been validated
            or if all fields of the mapping are None, True otherwise.
    """
    exolog = get_logger()
    first_row = csv_rows[0]
    warning: list[str] = []

    if _is_empty_mapping(mapping, warning_messages):
        return False

    for key, value in track(mapping.items(), description="Validating mapping..."):
        if value:
            try:
                eval(  # noqa: PGH001
                    value,
                    {"builtins": {}, "val": val},
                    first_row,
                )
            except TypeError as err:
                if not it_is_empty_str_err(err):
                    raise err
            except NameError:
                msg = f'\t-"{key}: {value}" is not a valid formula.'
                warning.append(msg)
                exolog.warning(msg)
            except SyntaxError as err:
                raise PythonSyntaxError(value) from err

    if len(warning) > 0:
        warning_messages.extend(
            [
                "  [bold orange3 on black] Mapping Yaml errors: "
                "[/bold orange3 on black]",
                f"  {len(warning)} formula(s) are invalid.",
            ],
        )
        warning.append("\n")
        warning_messages.extend(warning)
        exolog.warning(f"{len(warning)} formula(s) are invalid")
        return False

    exolog.info("mapping yaml has been validated")
    return True


def get_fields(
    path: Path | None = None,
) -> list[str]:
    """
    Get fields of the exoplanet database.

    By default fields are keys of the basic mapping file.

    Args:
        path: Path of the mapping file from where we want to extract fields.
            By default: tests/input_files/mapping_files/base_mapping.yml.

    Returns:
        Fields from the mapping file given in argument.
    """
    path = Path("default/mapping.yaml") if path is None else path
    mapping = read_yaml(path)
    return list(mapping.keys())


def validate_headers_for_import(
    csv_data: CsvData,
    fields: list[str],
    warning_messages: list[str],
) -> bool:
    """
    Validate headers for the import.

    We check if all headers in the csv file given are fields of the exoplanet database.

    Args:
        csv_data: Data read from a csv file.
        fields: Fields of the exoplanet database.
        warning_messages: All warning messages will be display to user later.

    Returns:
        True if all headers have been validated, False otherwise.
    """
    exolog = get_logger()
    headers = csv_data[0]
    warning = []

    for header in headers:
        if header not in fields:
            msg = f'\t-"{header}" is not a fields in exoplanet.eu database.'
            warning.append(msg)
            exolog.warning(msg)

    if len(warning) > 0:
        warning_messages.extend(
            [
                "  [bold orange3 on black] Csv headers errors: "
                "[/bold orange3 on black]",
                f"  {len(warning)} header(s) are not fields of database.",
            ],
        )
        warning.append("\n")
        warning_messages.extend(warning)
        exolog.warning(f"{len(warning)} header(s) are not fields of database")
        return False

    exolog.info("Headers has been validated for the import")
    return True


def post_validate(csv_content: CsvDictData) -> None:
    """
    Validate Star name and planet name.

    Accepted characters:
        - utf-8 letters, upper or lower
        - digits
        - underscore ('_')
        - hyphen ('-')
        - dot ('.')
        - space (' ')
        - parentheses ('(' and ')')
        - simple quote (''')
        - back quote ('`')
        - slash ('/')
        - star ('*')
        - colon (':')
        - square bracket ('[' and ']')

    Name of an object must begin with an accepted charactere except space and must
    contain at least one letter.

    Args:
        csv_content: Csv content which contain names and star names.

    Examples:
        All data are valid
        >>> data = [
        ...     {
        ...         "name": "toto.a",
        ...         "star_name": "toto",
        ...         "publication_url": "http://toto.fr"
        ...     }
        ... ]
        >>> post_validate(data)

        >>> data = [
        ...     {
        ...         "name": "toto-a",
        ...         "star_name": "toto",
        ...         "publication_url": "http://toto.fr"
        ...     }
        ... ]
        >>> post_validate(data)

        >>> data = [
        ...     {
        ...         "name": "toto_a",
        ...         "star_name": "toto",
        ...         "publication_url": "http://toto.fr"
        ...     }
        ... ]
        >>> post_validate(data)

        >>> data = [
        ...     {
        ...         "name": "toto a",
        ...         "star_name": "toto",
        ...         "publication_url": "http://toto.fr"
        ...     }
        ... ]
        >>> post_validate(data)

        >>> data = [
        ...     {
        ...         "name": "toto.1",
        ...         "star_name": "toto",
        ...         "publication_url": "http://toto.fr"
        ...     }
        ... ]
        >>> post_validate(data)

        Invalid name
        >>> data = [
        ...     {
        ...         "name": " toto_a",
        ...         "star_name": "toto",
        ...         "publication_url": "http://toto.fr"
        ...     }
        ... ]
        >>> try:
        ...     post_validate(data)
        ... except (ObjectCharNameError):
        ...     print(False)
        False

        >>> data = [
        ...     {
        ...         "name": "000",
        ...         "star_name": "toto",
        ...         "publication_url": "http://toto.fr"
        ...     }
        ... ]
        >>> try:
        ...     post_validate(data)
        ... except (ObjectAlphaNameError):
        ...     print(False)
        False

        Invalid url
        >>> data = [
        ...     {
        ...         "name": "toto_a",
        ...         "star_name": "toto",
        ...         "publication_url": "toto"
        ...     }
        ... ]
        >>> try:
        ...     post_validate(data)
        ... except (InvalidUrlError):
        ...     print(False)
        False
    """
    for element in csv_content:
        # Validate star and planet names
        name = cast(str, element["name"])
        star_name = cast(str, element["star_name"])

        if regex.match(_OBJECT_NAME_PATTERN, name) is None:
            raise ObjectCharNameError(name)

        if regex.match(_OBJECT_LETTER_PATTERN, name) is None:
            raise ObjectAlphaNameError(name)

        if regex.match(_OBJECT_NAME_PATTERN, star_name) is None:
            raise ObjectCharNameError(star_name)

        if regex.match(_OBJECT_LETTER_PATTERN, star_name) is None:
            raise ObjectAlphaNameError(name)

        if len(name.encode("utf-8")) >= 100:
            raise ObjectSizeNameError(name, 99)

        if len(star_name.encode("utf-8")) >= 60:
            raise ObjectSizeNameError(star_name, 59)

        if element["publication_url"] and not is_url(
            cast(str, element["publication_url"])
        ):
            raise InvalidUrlError(cast(str, element["publication_url"]))
