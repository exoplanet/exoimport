"""Utils function and classes for the project."""

# Standard imports
from collections import namedtuple
from collections.abc import Iterable
from decimal import Decimal
from typing import TypeAlias

# Third party imports
from validators import ValidationFailure
from validators import url as url_validator

ImportSystemRecord = namedtuple(
    "ImportSystemRecord",
    [
        "star_id",
        "planets_id",
        "star_alter_id",
        "planets_alter_id",
    ],
)

Data: TypeAlias = str | Decimal | None
CsvRow: TypeAlias = list[str]
CsvData: TypeAlias = list[CsvRow]
CsvTypedRow: TypeAlias = list[Data]
CsvTypedRow_Error: TypeAlias = list[Data | list[Data]]
CsvTypedData: TypeAlias = list[CsvTypedRow]
CsvTypedData_Error: TypeAlias = list[CsvTypedRow_Error]
CsvDictRow: TypeAlias = dict[str, Data]
CsvDictRow_Error: TypeAlias = dict[str, Data | list[Data]]
CsvDictData: TypeAlias = list[CsvDictRow]
CsvDictData_Error: TypeAlias = list[CsvDictRow_Error]
DbColumn: TypeAlias = list[str]
DbRow: TypeAlias = tuple[Data | list[Data], ...]
DbData: TypeAlias = list[DbRow]
ImportedSystems: TypeAlias = tuple[int, list[ImportSystemRecord]]
ImportedLinks: TypeAlias = tuple[dict[str, int], list[dict[str, int]]]


def useless_track(
    iter2return: Iterable,
    description: str,
) -> Iterable:
    """
    Return iter.

    This function it only exists to be useless.
    It's call when we make validation or filtration without typer and we won't display
    progress bar.

    Args:
        iter2return: An iterable to return.
        description: A useless description to match the signature of
            `typer.progress.track`

    Returns:
        iter2return.

    Examples:
        >>> useless_track([1, 2, 3], "toto")
        [1, 2, 3]
    """
    return iter2return


def need_to_format(field: str, obj: CsvDictRow) -> bool:
    """
    Tell if a field needs to be formatted.

    Args:
        field: Name of the field that we want to know if it must be formatted.
        obj: Object contains the field.

    Returns:
        True if the field needs to be formatted, False otherwise.

    Examples:
        >>> obj = {"titi": "a", "toto": None}

        # Exist and is not None -> needs to be formatted.
        >>> need_to_format("titi", obj)
        True

        # Doesn't exist.
        >>> need_to_format("tata", obj)
        False

        # Exist but is None.
        >>> need_to_format("toto", obj)
        False
    """
    return bool(field in obj.keys() and obj[field])


def is_url(str_url: str) -> bool:
    """
    Tell if a string representing an url or not.

    Args:
        str_url: String which is supposed to represent an url.

    Returns:
        True if `str_url` represents an url, False otherwise.

    Examples:
        # Valid url
        >>> is_url("https://www.google.com/")
        True

        # Invalid url
        >>> is_url("not_an_url.com")
        False
    """
    return not isinstance(url_validator(str_url), ValidationFailure)


def add_import_id(
    objects: tuple[DbColumn, DbData],
    import_id: int,
) -> tuple[DbColumn, DbData]:
    """
    Add foreign keys import_id to objects.

    Args:
        objects: Data of objects to import.
        import_id: Id in db of the import.

    Returns:
        Column and data to import in the database with the import id .

    Examples:
        >>> pubs = (["cola", "colb"], [("a", "b"), ("aa", "bb")])
        >>> add_import_id(pubs, 1)
        (
        ...     ['cola', 'colb', 'dbimport_id'],
        ...     [
        ...         ('a', 'b', Decimal('1')),
        ...         ('aa', 'bb', Decimal('1'))
        ...     ]
        ... )
    """
    columns = objects[0]
    data = objects[1]

    columns.extend(["dbimport_id"])

    new_data = []

    for row in data:
        res = (*row, Decimal(import_id))
        new_data.append(res)

    return (columns, new_data)
