"""Configuration class to configure an import."""

# Standard imports
from dataclasses import dataclass
from decimal import Decimal
from numbers import Number
from pathlib import Path
from typing import cast

# Third party imports
from exochoices import WebStatus

# Local imports
from ..logging import get_logger
from .config_utils import ConfigDict, Tables, load_config

CONFIG_FILES = [
    Path("merge_rules.yaml"),
    Path("merge_rules.yml"),
    Path("pyproject.toml"),
]


@dataclass
class Configuration:
    """
    Configuration object for an import.

    Attributes:
        epsilon_ra_dec: Angle (in arcsec) to determine if two stars are at the same
            position.
        web_statuses: Web status of planets, stars and publications.
        tables: Name of tables in database.

    """

    epsilon_ra_dec: Decimal
    web_statuses: WebStatus
    tables: Tables

    def __init__(
        self,
        *,
        file_path: Path | None = None,
        epsilon: int | None = None,
        web_statuses: str | None = None,
        tables: dict[str, str] | None = None,
        cwd: Path | None = None,
    ) -> None:
        """
        Initialize a Configuration with all data contains in a config file.

        We can give a path to get the configuration. If the path isn't given we try to
        load:
            - merge_rules.yaml
            - merge_rules.yml
            - pyproject.toml

        Args:
            file_path: Optional given path for the config path.
            epsilon: Epsilon value if we want fix it with the cmd.
            web_statuses: Web statuses value if we want fix it with the cmd.
            tables: Name of tables in database.
            cwd: Current work directory. Useful for testing.
        """
        cwd = Path.cwd() if cwd is None else cwd

        if epsilon is not None and not isinstance(epsilon, Number):
            raise TypeError("epsilon_ra_dec must be a number.")

        config = self._read_config(file_path, cwd)

        self.epsilon_ra_dec = (
            self._fix_epsilon_ra_dec(config) if epsilon is None else Decimal(epsilon)
        )
        status = (
            config.get("web_statuses", None) if web_statuses is None else web_statuses
        )
        self.web_statuses = WebStatus.from_str(status)

        self.tables = self._fix_tables(config) if tables is None else Tables(tables)

        get_logger().info("Configuration has been loaded")

    def _read_config(
        self,
        file_path: Path | None,
        cwd: Path,
    ) -> ConfigDict:
        """
        Read the correct config file to produce a config object.

        Args:
            file_path: Optional given path for the config path.
            cwd: Current work directory. Useful for testing.

        Returns:
            A configuration object.
        """
        config = None
        config_files = CONFIG_FILES.copy()

        if file_path:
            if not file_path.exists():
                raise FileNotFoundError(
                    f"{file_path} does not exist. "
                    f"Create it or use {' or '.join(str(CONFIG_FILES))}",
                )

            if file_path.suffix not in [".toml", ".yaml", ".yml"]:
                raise TypeError("You must give a toml or yaml file for config.")

            config_files.insert(0, file_path)

        for path_file in config_files:
            path = path_file if path_file.is_absolute() else cwd / path_file

            if path.exists():
                config = load_config(path)
                break

        if config is None:
            raise FileNotFoundError(
                f"No config file was found. Use {' or '.join(str(CONFIG_FILES))}",
            )

        return config

    def _fix_epsilon_ra_dec(self, config: ConfigDict) -> Decimal:
        """
        Fix the epsilon value of configuration.

        Args:
            config: Configuration values.

        Returns:
            epsilon values in decimal.
        """
        epsilon_ra_dec = config.get("epsilon_ra_dec", None)

        if epsilon_ra_dec is None or not isinstance(epsilon_ra_dec, Number):
            raise TypeError("epsilon_ra_dec must be a number.")

        return Decimal(epsilon_ra_dec)

    def _fix_tables(self, config: ConfigDict) -> Tables:
        """
        Fix the table names of configuration.

        Args;
            config: Configuration values.

        Returns:
            Table names in a Tables object.

        Raised:
            TypeError: Indirect raised by `Tables.__init__`.
        """
        tables_from_config = config.get("tables", None)

        if tables_from_config is None or not isinstance(tables_from_config, dict):
            raise TypeError("Tables must be a dictionary.")

        tables_attrs = Tables.get_attrs()
        init_tables = {
            name: tables_from_config.get(name[0 : -len("_table")], None)
            for name in cast(list, tables_attrs)
        }

        return Tables(init_tables)
