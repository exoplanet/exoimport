"""Useful functions for the configuration of an import."""

# Standard imports
import sys
from dataclasses import dataclass, fields
from pathlib import Path

# In python 3.11, tomllib was added,
# https://docs.python.org/3/whatsnew/3.11.html#new-modules, so that exoimport is
# compatible with versions 3.10 and 3.11 without having 2 different packages, we use the
# toml, https://pypi.org/project/toml/, for versions lower
# than python 3.11 and the tomlib of python 3.11 for versions greater than python 3.10
if sys.version_info < (3, 11):
    # Third party imports
    from toml import load as toml_load  # pragma: no cover
else:
    # Standard imports
    from tomllib import load as toml_load  # pragma: no cover


# Standard imports
from typing import TypedDict

# Third party imports
from yaml import safe_load as yaml_load


class ConfigDict(TypedDict):
    """
    Typed Dictionary of configuration.

    Attributes:
        epsilon_ra_dec: Angle (in arcsec) to determine if two stars are at the same
            position.

    """

    epsilon_ra_dec: str


@dataclass
class Tables:
    """
    Tables classe to store all table names.

    Attributes:
        import_table: Name of the table for imports.
        star_table: Name of the table for stars.
        alternate_star_name_table: Name of the table for alternate stars names.
        planet_table: Name of the table for planets.
        alternate_planet_name_table: Name of the table for alternate planets names.
        molecule_table: Name of the table for molecule.
        atmoshpere_molecule_table: Name of the table for atmoshpere molecule.
        publication_table: Name of the table for publications.
        planet2publication_table: Name of the table for planet2publication.
        star2publication_table: Name of the table for star2publication.
        planetary_system_table: Name of the table for planetary systems.
        planet2star_table: Name of the table for planet2star.

    """

    import_table: str
    star_table: str
    alternate_star_name_table: str
    planet_table: str
    alternate_planet_name_table: str
    molecule_table: str
    atmoshpere_molecule_table: str
    publication_table: str
    planet2publication_table: str
    star2publication_table: str
    planetary_system_table: str
    planet2star_table: str

    # TODO: Voir si faisable avec une boucle
    def __init__(self, dict_init: dict[str, str]) -> None:
        """
        Initialize a Table object.

        We check if all value in `dict_init` are not None and
        we initialize each attribute with the corresponding value in `dict_init`.

        Args:
            dict_init: Dictionnarie for initialisation.

        Raised:
            TypeError: Indirect raised by `self._check_tables`.
        """
        self._check_tables(dict_init)
        self.import_table = dict_init["import_table"]
        self.star_table = dict_init["star_table"]
        self.alternate_star_name_table = dict_init["alternate_star_name_table"]
        self.planet_table = dict_init["planet_table"]
        self.alternate_planet_name_table = dict_init["alternate_planet_name_table"]
        self.molecule_table = dict_init["molecule_table"]
        self.atmoshpere_molecule_table = dict_init["atmoshpere_molecule_table"]
        self.publication_table = dict_init["publication_table"]
        self.planet2publication_table = dict_init["planet2publication_table"]
        self.star2publication_table = dict_init["star2publication_table"]
        self.planetary_system_table = dict_init["planetary_system_table"]
        self.planet2star_table = dict_init["planet2star_table"]

    def _check_tables(self, dict_init: dict[str, str]) -> None:
        """
        Check if all value in `dict_init` are not None.

        If a value is None we create a message for the raise and we continue.

        Args:
            dict_init: Dictionnarie for initialisation.

        Raised:
            TypeError: If a value of `dict_init` is None.
        """
        excp_msg: list[str] = []
        for key, value in dict_init.items():
            if value is None:
                excp_msg.append(f"{key}: None. All table need to be register.")

        if excp_msg:
            raise TypeError(" ".join(excp_msg))

    @classmethod
    def get_attrs(cls) -> list[str]:  # noqa: ANN102
        """
        Get all attrs.

        Returns:
            List of all attrs of cls.
        """
        return [attr.name for attr in fields(cls)]


def load_config(path: Path) -> ConfigDict | None:
    """
    Load config from a config file.

    Args:
        path: Path of the config file.

    Returns:
        All config for an import.
    """
    match path.suffix:
        case ".toml":
            with open(path, "rb") as toml_file:
                loaded = toml_load(toml_file)["tool"]
                return loaded.get("exoimport") if loaded is not None else None
        case ".yml" | ".yaml":
            with open(path) as yaml_file:
                loaded = yaml_load(yaml_file)
                return loaded.get("exoimport") if loaded is not None else None
        case _:
            return None
