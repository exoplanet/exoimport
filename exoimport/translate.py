"""All function to translate a csv file in exoplanet csv."""

# Standard imports
from math import *  # noqa: F403

# Local imports
from .exceptions import PythonSyntaxError
from .logging import get_logger
from .utils import CsvDictData, CsvRow, CsvTypedRow
from .validate import it_is_empty_str_err, val


def _get_header(mapping: dict[str, str | None]) -> list[str]:
    """
    Return all headers specified as key in the mapping parameter.

    The mapping file can have a key named "filter" that is not a header name
    that why we exclude it from the headers returned.

    Args:
        - mapping: the mapping with all new headers.

    Returns:
        List of all new headers.

    Examples:
        >>> import logging as log
        >>> log.disable(log.CRITICAL)
        >>> _get_header({"a": "aa", "b": "bb", "c": "cc"})
        ['a', 'b', 'c']
        >>> _get_header({"filter": "toto", "a": "aa", "b": "bb", "c": "cc"})
        ['a', 'b', 'c']
        >>> log.disable(log.NOTSET)
    """
    result = list(mapping.keys())
    if "filter" in mapping:
        result.remove("filter")

    get_logger().info("headers has been collected")
    return result


def _dict_csv_to_exoplanet_dict_csv(
    mapping: dict[str, str | None],
    csv_rows: CsvDictData,
) -> CsvDictData:
    """
    Transform a dict from a csv to a dict formatted to generate a csv for exoplanet.eu.

    Args:
        - mapping: Mapping of the headers.
        - csv_rows: The dict csv for the csv file.

    Returns:
        A dict representing the csv with the correct headers and data in rows.

    Examples:
        >>> from decimal import Decimal
        >>> mapping = {"a": "toto", "b": "titi", "c": "-toto+2"}
        >>> csv_rows = [{"toto": Decimal('12'), "titi": Decimal('24')}]
        >>> _dict_csv_to_exoplanet_dict_csv(mapping, csv_rows)
        [{'a': Decimal('12'), 'b': Decimal('24'), 'c': Decimal('-10')}]
    """
    result = []

    for row in csv_rows:
        res = {}
        for key, value in mapping.items():
            if value and not key == "filter":
                try:
                    res[key] = eval(  # noqa: PGH001
                        value,
                        {"builtins": {}, "val": val},
                        row,
                    )
                except TypeError as err:
                    if it_is_empty_str_err(err):
                        res[key] = None
                    else:
                        raise err
                except SyntaxError as err:
                    raise PythonSyntaxError(value) from err

        result.append(res)

    return result


def _get_exoplanet_csv(
    headers: list[str],
    content: CsvDictData,
) -> list[CsvRow | CsvTypedRow]:
    """
    Get a csv formatted for exoplanet.eu from a formatted dict.

    Args:
        - headers: All headers for the csv.
        - content: All rows for the new csv.

    Returns:
        A List of headers and row for the new csv.

    Examples:
        >>> import logging as log
        >>> log.disable(log.CRITICAL)
        >>> headers = ["a", "b", "c", "d"]
        >>> content = [{"a": 12, "b": 24, "c": None, "d": 48},
        ... {"a": 12, "b": 24, "d": 48}]
        >>> _get_exoplanet_csv(headers, content)
        [['a', 'b', 'c', 'd'], [12, 24, None, 48], [12, 24, None, 48]]
        >>> log.disable(log.NOTSET)
    """
    result: list[CsvRow | CsvTypedRow] = [headers]

    for raw in content:
        res = []
        for header in headers:
            res.append(raw.get(header))
        result.append(res)

    get_logger().info("data has been converted to exoplanet csv")
    return result


def translate(
    mapping: dict[str, str | None],
    csv_rows: CsvDictData,
) -> list[CsvRow | CsvTypedRow]:
    """
    Translate a dict csv from file into a formatted csv for exoplanet.eu.

    Args:
        - mapping: Mapping of the headers.
        - csv_rows: The dict csv for the csv file.

    Returns:
        A list of headers and rows formatted for exoplanet.eu
            ready to be written to a csv file.
    """
    headers = _get_header(mapping)
    raw_content = _dict_csv_to_exoplanet_dict_csv(mapping, csv_rows)
    res = _get_exoplanet_csv(headers, raw_content)

    get_logger().info("Data has been translated")
    return res
