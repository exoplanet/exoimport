"""Functions to write translate content in output csv file."""

# Standard imports
import csv
from pathlib import Path

# Local imports
from .logging import get_logger
from .utils import CsvData, CsvRow, CsvTypedRow


def write_csv(
    content: list[CsvRow | CsvTypedRow] | CsvData,
    dir_name: Path,
    output_filename: Path,
) -> None:
    """
    Write a CSV data for exoplanet in a csv file.

    Args:
        content: CSV data to write.
        dir_name: Path to the directory where the data will be written.
        output_filename: The name of the output file.
    """
    dir_name.mkdir(exist_ok=True, parents=True)

    with open(
        dir_name / output_filename,
        "w",
        encoding="utf-8",
        newline="",
    ) as csv_file:
        writer = csv.writer(
            csv_file,
            delimiter=",",
        )
        for row in content:
            writer.writerow(row)

    get_logger().info(f"{dir_name / output_filename} has been written")
