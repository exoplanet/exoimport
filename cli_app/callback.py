"""Callback function for typer."""

# Standard imports
from os import path as os_path
from pathlib import Path

# Third party imports
import typer
from rich.console import Console

# First party imports
import exoimport

CONSOLE = Console()


def version_callback(value: bool) -> None:
    """
    Get the version, display the version if `--version` was used.

    Args:
        value: Boolean to known if we display the version or not.
    """
    if value:
        CONSOLE.print(f"{exoimport.__app_name__} v.{exoimport.__version__}")
        raise typer.Exit()


def debug_callback(value: bool) -> bool:
    """
    Get the value to activate the debug option.

    If debug is activated, we display an message in the terminal.

    Args:
        value: Boolean to known if we activate debug option or not.
    """
    if value:
        CONSOLE.print("\n[bold yellow]Debug option is active[/bold yellow]")
    return value


def adv_log_callback(value: bool) -> bool:
    """
    Get the value to activate the advanced logging option.

    Args:
        value: Boolean to known if we activate advanced logging option or not.
    """
    return value


def log_path_callback(value: Path | None) -> Path | None:
    """
    Get the value to of cwd log.

    Args:
        value: Cwd for logging or None for the cwd './'.
    """
    return value


def csv_input_file_callback(path: Path) -> Path:
    """
    Validate the csv input file.

    Args:
        path: Path of the file to validate.

    Returns:
        The path if is validated.

    Raises:
        BadParameter: If the csv input file isn't a `.csv` file
            or if the file does not exist.
    """
    if not path.suffix == ".csv":
        raise typer.BadParameter("Enter a `.csv` file!")
    if not os_path.exists(path):
        raise typer.BadParameter(f"The file {str(path)} does not exist!")
    return path


def csv_output_file_callback(path: Path) -> Path:
    """
    Validate the csv output file.

    Args:
        path: Path of the file to validate.

    Returns:
        The path id if validated.

    Raises:
        BadParameter: If the csv output file isn't a `.csv` file.
    """
    if not path.suffix == ".csv":
        raise typer.BadParameter("Enter a `.csv` file!")
    return path


def yaml_file_callback(path: Path) -> Path:
    """
    Validate the yaml input file.

    Args:
        path: Path of the file to validate.

    Returns:
        The path if is validated.

    Raises:
        BadParameter: If the yaml input file isn't a `.yaml` or `.yml` file
            or if the file does not exist.
    """
    if path.suffix not in (".yaml", ".yml"):
        raise typer.BadParameter("Enter a `.yaml` or `.yml` file!")
    if not os_path.exists(path):
        raise typer.BadParameter(f"The file {str(path)} does not exist!")
    return path
