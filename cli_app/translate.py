"""Useful function for translate typer."""

# Standard imports
from os import EX_DATAERR
from pathlib import Path

# Third party imports
import typer
from art import text2art
from rich.console import Console
from rich.progress import track

# First party imports
from exoimport import (
    CsvData,
    CsvDictData,
    format_csv,
    get_logger,
    input_filter,
    validate_csv_headers,
    validate_mapping_yaml,
    write_csv,
)

# Local imports
from .utils import get_content_in_csv_format

# Rich console
CONSOLE = Console()


def do_validation(
    csv_content: CsvData,
    mapping_content: dict[str, str | None],
    warning_messages: list[str],
) -> None:
    """
    Do the validation of the csv and yaml files.

    Args:
        csv_content: Content of the csv file to validate.
        mapping_content: Content of the yaml file to validate.
        warning_messages: Warning messages us to inform the user about the validation.
    """
    exolog = get_logger()
    print(text2art("Validation"))

    valid_csv = validate_csv_headers(csv_content, warning_messages, track=track)

    formatted_csv_content = format_csv(csv_content)

    valid_mapping = validate_mapping_yaml(
        mapping_content,
        formatted_csv_content,
        warning_messages,
        track=track,
    )

    if not valid_csv or not valid_mapping:
        CONSOLE.print("\n")
        CONSOLE.print(*warning_messages, sep="\n")
        CONSOLE.print(f"  Valid csv? {valid_csv}. Valid mapping? {valid_mapping}.")
        exolog.error(
            f"Csv or mapping are invalid. csv: {valid_csv}, mapping: {valid_mapping}",
        )
        raise typer.Exit(EX_DATAERR)

    exolog.info("Csv and mapping are valid")
    CONSOLE.print(f"\n  Valid csv? {valid_csv}. Valid mapping? {valid_mapping}.\n\n")


def do_filtration(
    csv_content: CsvDictData,
    mapping_content: dict[str, str | None],
    warning_messages: list[str],
    filtered_path: Path,
    kept_path: Path,
) -> CsvDictData:
    """
    Do the filtration of the csv file with the filter in the mapping.

    Args:
        csv_content: Content of the csv file to validate.
        mapping_content: Content of the yaml file to validate.
        warning_messages: Warning messages us to inform the user about the filtration.
        filtered_path: Path of the file to get filtered rows in an output file.
        kept_path: Path of the file to get kept rows in an output file.

    Returns:
        The filtered csv content.
    """
    exolog = get_logger()
    print(text2art("Filtration"))
    output_filtered = None
    output_kept = None

    output_filtered = typer.confirm(
        f"Do you want to write filtered row(s) in the output file: {filtered_path}?",
        default=False,
    )

    if output_filtered:
        exolog.info(f"filtered rows will be written to the file: {filtered_path}")
        CONSOLE.print(
            "\t:wastebasket:  Output file for the filtered rows: "
            f"[orange3 on black]{filtered_path}[/orange3 on black]",
        )

    output_kept = typer.confirm(
        f"Do you want to write filtered row(s) in the output file: {kept_path}?",
        default=False,
    )

    if output_kept:
        exolog.info(f"kept rows will be written to the file: {kept_path}")
        CONSOLE.print(
            "\t:card_file_box:  Output file for the filtered rows: "
            f"[green on black]{kept_path}[green on black]",
        )

    filtered_content = input_filter(
        mapping_content.get("filter"),
        csv_content,
        warning_messages,
        output_filtered,
        filtered_path,
        track=track,
    )

    if output_kept:
        write_csv(
            get_content_in_csv_format(filtered_content),
            Path.cwd(),
            kept_path,
        )

    CONSOLE.print("\n")
    CONSOLE.print(*warning_messages, sep="\n")

    return filtered_content
