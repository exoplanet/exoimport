"""Utils function for typer."""

# Standard imports
import sys
from collections.abc import Iterable
from typing import cast

# Third party imports
import toml
import yaml
from rich.console import Console
from rich.panel import Panel

# First party imports
from exoimport import CsvData, CsvDictData

# In python 3.11, the enumerables have been changed,
# https://docs.python.org/3/whatsnew/3.11.html#enum, so that py-linq-sql is compatible
#  with versions 3.10 and 3.11 without having 2 different packages, we use the
# LowercaseStrEnums of strenum, https://pypi.org/project/StrEnum/, for versions lower
# than python 3.11 and the StrEnums of python 3.11 for versions greater than python 3.10
if sys.version_info < (3, 11):
    # Third party imports
    from strenum import LowercaseStrEnum as StrEnum  # pragma: no cover
else:
    # Standard imports
    from enum import StrEnum  # pragma: no cover

# Rich console
CONSOLE = Console()


class ConfigExt(StrEnum):
    """Enum of possible config file extension."""

    TOML = "toml"
    YAML = "yaml"


def generate_config(ext: str, epsilon: int, web_statuses: str) -> str:
    """
    Generate a config file with the given values.

    The generate file is .toml or .yaml depending on what the user requested.

    Args:
        ext: Extension of the config file. toml or yaml.
        epsilon: Value of `epsilon` in the config file.
        web_statuses: Value of `web_statuses` in the config file.

    Returns:
        Name of the generate config file.
    """
    config_data = {
        "exoimport": {
            "epsilon_ra_dec": epsilon,
            "web_statuses": web_statuses,
        }
    }
    match ext:
        case "toml":
            with open("merge_rules.toml", "w") as toml_file:
                toml.dump({"tool": config_data}, toml_file)
            return "merge_rules.toml"
        case _:
            with open("merge_rules.yaml", "w") as yaml_file:
                yaml.dump(config_data, yaml_file)
            return "merge_rules.yaml"


def display_error_no_debug(error: Exception) -> None:
    """
    Display an error fromated with a rich Panel when the debug option isn't use.

    Args:
        error: An exception to display.
    """
    CONSOLE.print(
        Panel(
            f"[bold red]{type(error).__name__}:[/bold red] {str(error)}",
            border_style="red",
            title="Error",
            title_align="left",
        ),
    )


def get_headers(csv_content: CsvDictData) -> list[str]:
    """
    Get headers from a CsvDictData for a CsvData.

    Args:
        csv_content: Content of the CsvDictData.

    Returns:
        All headers from the CsvDictData.

    Examples:
        >>> get_headers([{"a": "aa", "b": "bb"}, {"a": "toto", "b": "titi"}])
        ['a', 'b']
    """
    return list(csv_content[0].keys())


def get_content_in_csv_format(csv_content: CsvDictData) -> CsvData:
    """
    Get the content of the CsvDictData in the format CsvData.

    Args:
        csv_content: content of the CsvDictData to format in CsvData.

    Returns:
        The content in the csv format.

    Examples:
        >>> get_content_in_csv_format(
        ...     [{"a": "aa", "b": "bb"}, {"a": "toto", "b": "titi"}]
        ... )
        [['a', 'b'], ['aa', 'bb'], ['toto', 'titi']]

    """
    result = [get_headers(csv_content)]

    for row in csv_content:
        result.append(list(cast(Iterable, row.values())))

    return result
