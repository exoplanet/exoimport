"""App for typer to interact with a user in command lines."""
# Standard imports
from os import getenv
from pathlib import Path
from time import sleep
from typing import Optional, cast

# Third party imports
import typer
from art import text2art
from dotenv import load_dotenv
from exochoices import WebStatus
from rich.console import Console
from tmp_connection_psql import connect

# First party imports
from docs import display_md_file_content, get_resume, get_sample_mapping
from exoimport import (
    Configuration,
    CsvDictData,
    DbImportError,
    ImportInDbError,
    LoggerError,
    PythonSyntaxError,
    ReadError,
    ValidateError,
    config_logger,
    format_csv,
    format_planets,
    format_stars,
    get_logger,
    import_and_link_publication,
    import_systems,
    post_validate,
    read_csv,
    read_yaml,
    split_star_planet_and_other_params,
)
from exoimport import translate as translate_csv
from exoimport import write_csv

# Local imports
from .callback import (
    adv_log_callback,
    csv_input_file_callback,
    csv_output_file_callback,
    debug_callback,
    log_path_callback,
    version_callback,
    yaml_file_callback,
)
from .importdb import get_json_resume, log_and_display_summary
from .translate import do_filtration, do_validation
from .utils import ConfigExt, display_error_no_debug, generate_config

# App manager
APP = typer.Typer(
    help="[bold blue on black]Exo Import.[/bold blue on black] :inbox_tray:",
    rich_markup_mode="rich",
)

# Rich console
CONSOLE = Console()
_DEBUG = {"value": False}


class TopicDoesNotExist(Warning):
    """Warning raise when we try to get help on a topic ho doesn't exist."""

    pass


@APP.command(
    name="help",
    help="[bold white on black]Display help for ExoImport. "
    ":question_mark:[/bold white on black]",
    rich_help_panel="Documentation",
)
def display_help(
    topic: str = typer.Argument(
        None,
        help="Topic on which you want more information.",
        show_default=False,
    ),
) -> None:
    """
    Command use to display all help for Exo Import.

    Args:
        topic: Topic on which we want documentation. None by default which means
            that we display the documentation of the help command.
    """
    try:
        debug = _DEBUG["value"]
        exolog = get_logger()

        match topic:
            case None:
                CONSOLE.print(get_resume(), soft_wrap=True)
            case _:
                topic_status, msg = display_md_file_content(topic)
                CONSOLE.print(msg, soft_wrap=True)
                if not topic_status:
                    raise TopicDoesNotExist(f"{topic} topic does not exist")

    except TopicDoesNotExist as err:
        exolog.error(f"{type(err).__name__}: {str(err)}")
        if not debug:
            raise typer.Exit(1)  # noqa: B904
        raise err
    # No cover because this is a security for an Error in the database.
    except Exception as err:  # pragma: no cover
        exolog.error(f"{type(err).__name__}: {str(err)}")
        if not debug:
            display_error_no_debug(err)
            raise typer.Exit(1)  # noqa: B904
        raise err


@APP.command(
    name="sample-mapping",
    help="[bold white on black]Get a sample of mapping file. [/bold white on black]",
    rich_help_panel="Documentation",
)
def sample_mapping(
    output_file: Optional[Path] = typer.Option(
        None,
        "--output-file",
        "-o",
        help="yaml output file.",
        show_default=False,
    ),
) -> None:
    """
    Give a sample of the mapping file.

    Args:
        output_file: Option to give an output yaml file.
    """
    try:
        debug = _DEBUG["value"]
        exolog = get_logger()

        mapping = get_sample_mapping()

        match output_file:
            case None:
                CONSOLE.print(mapping, soft_wrap=True)
            case _:
                if output_file.suffix not in (".yaml", ".yml"):
                    raise typer.BadParameter("Enter a `.yaml` or `.yml` file!")

                output_file.touch(exist_ok=True)

                with open(output_file, "w", encoding="utf-8") as out:
                    out.write(mapping)

                    exolog.info(f"File {output_file} was succesly written.")
                CONSOLE.print(f"File {output_file} was succesly written.")

    except Exception as err:
        exolog.error(f"{type(err).__name__}: {str(err)}")
        if not debug:
            display_error_no_debug(err)
            raise typer.Exit(1)  # noqa: B904
        raise err


# TODO: Integrer la generation des noms de table
@APP.command(
    name="generate-config",
    help="[bold white on black]Generate a config file for ExoImport. :gear:"
    "[/bold white on black]",
    rich_help_panel="Configuration",
)
def generate_config_cli(
    extension: ConfigExt = typer.Option(
        ConfigExt.TOML,
        help="Choose your config file extension.",
        case_sensitive=False,
        prompt="Which type of file for your config file?",
    ),
) -> None:
    """
    Generate a config file for ExoImport.

    Ask to the user which type of file (.yaml or .toml) and the value of epsilon and
    web statuses.
    The user can ask some help with enter 'help' in the prompt.

    Args:
        extension: Type of the file, toml or yaml. By default: toml.
    """
    exolog = get_logger()
    epsilon, web_statuses = None, None

    while epsilon is None:
        input_eps = typer.prompt("Epsilon?")

        if input_eps.isnumeric():
            epsilon = int(input_eps)
            break

        if input_eps == "help":
            display_help("epsilon")
            CONSOLE.print()

        CONSOLE.print(
            "[black on orange3] :warning: Espilon must be a numeric values."
            "[/black on orange3]",
        )

    while web_statuses is None:
        input_ws = typer.prompt("Web statuses?")

        if input_ws.title() in WebStatus.as_str_list():
            web_statuses = input_ws
            break

        if input_ws == "help":
            display_help("web_statuses")
            CONSOLE.print()

        CONSOLE.print(
            "[black on orange3] :warning: Web statuses must be in "
            f"{WebStatus.as_str_list()}.[/black on orange3]",
        )

    generate_config_file = generate_config(extension, epsilon, web_statuses)
    exolog.info(f"The config file {generate_config_file} has been generated")
    CONSOLE.print(f"The config file {generate_config_file} has been generated.")


@APP.command(
    name="translate",
    help="[bold white on black]Translate a csv file to normalized "
    "csv for exoplanet.eu. :telescope:[/bold white on black]",
    rich_help_panel="Translation",
)
def translate_csv_cli(  # noqa: PLR0913, PLR0912
    csv_file: Path = typer.Argument(
        ...,
        help="Csv file to translate.",
        show_default=False,
        rich_help_panel=":page_facing_up: Files arguments",
        callback=csv_input_file_callback,
    ),
    mapping_file: Path = typer.Argument(
        ...,
        help="Mapping file use to translate the csv file.",
        show_default=False,
        rich_help_panel=":page_facing_up: Files arguments",
        callback=yaml_file_callback,
    ),
    filtered_path: Path = typer.Option(
        Path("out.csv"),
        help="Csv output file for filtered row(s) in filter.",
        rich_help_panel=":memo: Output Files",
        callback=csv_output_file_callback,
    ),
    kept_path: Path = typer.Option(
        Path("keep.csv"),
        help="Csv output file for kept row(s) in filter.",
        rich_help_panel=":memo: Output Files",
        callback=csv_output_file_callback,
    ),
    translate_path: Path = typer.Option(
        Path("translated.csv"),
        help="Csv output file for all translate lines in translate.",
        rich_help_panel=":memo: Output Files",
        callback=csv_output_file_callback,
    ),
    do_validate: bool = typer.Option(
        True,
        "--validate/--no-validate",
        help="True by default to make the validation, False for no validation.",
        rich_help_panel="Translate options",
    ),
    do_filter: bool = typer.Option(
        True,
        "--filter/--no-filter",
        help="True by default to make the filtration, False for no filtration.",
        rich_help_panel="Translate options",
    ),
    do_translate: bool = typer.Option(
        True,
        "--translate/--no-translate",
        help="True by default to make the translation, False for no translation",
        rich_help_panel="Translate options",
    ),
) -> None:
    """
    Command used to translate a csv file with a mapping yaml file.

    Args:
        csv_file: Path of the csv file to translate.
        mapping_file: Path of the mapping yaml file for the translation.
        filtered_path: Path of the file to get filtered rows in an output file.
            Default: 'out.csv'.
        kept_path: Path of the file to get kept rows in an output file.
            Default: 'keep.csv'.
        translate_path: Path of the file which will contain the translated csv.
            Default: 'translated.csv'.
        do_validate: Option to validate files. Default: True.
        do_filter: Option to filter the csv file. Default: True.
        do_translate: Option to translate the csv with the mapping. Default: True.
    """
    try:
        debug = _DEBUG["value"]
        exolog = get_logger()
        try:
            csv_content = read_csv(csv_file)
        except (ReadError, FileNotFoundError) as err:
            raise err

        try:
            mapping_content = read_yaml(mapping_file)
        except (ReadError, FileNotFoundError) as err:
            raise err

        warning_messages: list[str] = []

        if do_translate and not do_validate:
            CONSOLE.print(
                "  [bold white on red] :warning::warning::warning: WARNING: you try to "
                "make a translation without validation, We do not ensure the smooth "
                "running of the translation!!! [/bold white on red]",
            )
            exolog.warning(
                "you try to make a translation without validation. "
                "We do not ensure the smooth running of the translation!!!",
            )

        if do_validate:
            try:
                do_validation(csv_content, mapping_content, warning_messages)
            except (TypeError, PythonSyntaxError) as err:
                raise err

        formatted_csv_content = format_csv(csv_content)

        if do_filter:
            formatted_csv_content = do_filtration(
                formatted_csv_content,
                mapping_content,
                warning_messages,
                filtered_path,
                kept_path,
            )

        if do_translate:
            with CONSOLE.status("[bold green]Translating csv..."):
                print(text2art("Translation"))
                try:
                    translated_csv = translate_csv(
                        mapping_content,
                        formatted_csv_content,
                    )
                except (TypeError, PythonSyntaxError) as err:
                    raise err
                exolog.info("translating csv complete")

                sleep(1)
                write_csv(translated_csv, Path.cwd(), translate_path)
                exolog.info("writing csv complete")
                CONSOLE.print(
                    f"The file {str(Path.cwd() / translate_path)} was created.\n",
                )
        exolog.info("The translate end without error")
        CONSOLE.print("\n🥳 [bold]The translate end without error.[/bold] ✅")
    except Exception as err:
        exolog.error(f"{type(err).__name__}: {str(err)}")
        if not debug:
            display_error_no_debug(err)
            raise typer.Exit(1)  # noqa: B904
        raise err


@APP.command(
    name="import",
    help="[bold white on black]Import a normalized csv exoplanet file for "
    "exoplanet.eu. :telescope:[/bold white on black]",
    rich_help_panel="Translation",
)
def import_cli(  # noqa: PLR0913, PLR0912
    external_csv_file: Path = typer.Argument(
        ...,
        help="External csv before the translation.",
        show_default=False,
        rich_help_panel=":page_facing_up: Files arguments",
        callback=csv_input_file_callback,
    ),
    mapping_file: Path = typer.Argument(
        ...,
        help="Mapping file use in the translation of the csv file.",
        show_default=False,
        rich_help_panel=":page_facing_up: Files arguments",
        callback=yaml_file_callback,
    ),
    exoplanet_csv_file: Path = typer.Argument(
        ...,
        help="External csv before the translation.",
        show_default=False,
        rich_help_panel=":page_facing_up: Files arguments",
        callback=csv_input_file_callback,
    ),
    config_file: Optional[Path] = typer.Option(
        None,
        "--config-file",
        help="Configuration file.",
        rich_help_panel=":gear: Configuration",
    ),
    epsilon_conf: Optional[int] = typer.Option(
        None,
        "--epsilon",
        help="Configuration for epsilon",
        rich_help_panel=":gear: Configuration",
    ),
    web_statuses_conf: Optional[str] = typer.Option(
        None,
        "--web-statuses",
        help="Configuration for web statuses",
        rich_help_panel=":gear: Configuration",
    ),
    show_summary: bool = typer.Option(
        True,
        "--show-summary/--no-show-summary",
        "-s",
        help="Show summary (all imported ids sorte by table).",
        rich_help_panel="Output options",
    ),
    json_path: Optional[Path] = typer.Option(
        None,
        "--json-output-path",
        help="Get a very detailed summary in a json path.",
        rich_help_panel="Output options",
    ),
) -> None:
    """
    Command used to import an exoplanet csv file.

    Args:
        external_csv_file: Path of the translated csv file use to create the exoplanet
            csv file.
        mapping_file: Path of the mapping yaml file use to create the exoplanet csv
            file.
        exoplanet_csv_file: Path of the csv file to import.
        config_file: Optional path of the config file.
        epsilon_conf: Optional configuration for epsilon.
        web_statuses_conf: Optional configuration for web_statuses.
        show_summary: Boolean to show a summary of the import. True by default.
        json_path:  Optional path of the json output file for a detailed summary.
    """
    try:
        debug = _DEBUG["value"]
        exolog = get_logger()
        print(text2art("Import"))

        CONSOLE.print(
            "\n[bold white on yellow] :warning: Input files :warning: "
            "[/bold white on yellow]\n"
            f" External csv: {external_csv_file} \n Mapping file: {mapping_file}\n "
            f"Exoplanet.eu csv: {exoplanet_csv_file}",
        )
        typer.confirm(
            "\nIs that right?",
            default=False,
            abort=True,
        )

        exolog.info(
            f"External file: {external_csv_file}\n"
            f"Mapping file: {mapping_file}\n"
            f"Internal file: {exoplanet_csv_file}\n",
        )

        try:
            csv_content = read_csv(exoplanet_csv_file)
        except (ReadError, FileNotFoundError) as err:
            raise err

        formated_content = format_csv(csv_content)

        try:
            post_validate(formated_content)
        except ValidateError as err:
            raise err

        stars, planets, other = split_star_planet_and_other_params(formated_content)

        try:
            conf = Configuration(
                file_path=config_file,
                epsilon=epsilon_conf,
                web_statuses=web_statuses_conf,
            )
        except (FileNotFoundError, TypeError) as err:
            raise err

        form_planets = format_planets(planets, conf)
        format_stars(stars, conf)

        import_data = {
            "external_csv": Path().cwd() / external_csv_file,
            "mapping_yaml": Path().cwd() / mapping_file,
            "internal_csv": Path().cwd() / exoplanet_csv_file,
        }

        load_dotenv()
        conn = connect(
            user=getenv("EXOIMPORT_DBUSER"),
            password=getenv("EXOIMPORT_DBPASSWORD"),
            host=getenv("EXOIMPORT_DBHOST"),
            port=getenv("EXOIMPORT_DBPORT"),
            database=getenv("EXOIMPORT_DBNAME"),
        )

        try:
            systems_res = import_systems(
                conn,
                import_data,
                [stars, cast(CsvDictData, form_planets), other],
                conf,
            )
        except (ImportInDbError, FileNotFoundError, ReadError) as err:
            raise err

        try:
            publications_res = import_and_link_publication(
                conn, other, systems_res, conf
            )
        # No cover because this is a security for an Error in the database.
        except (DbImportError) as err:  # pragma: no cover
            raise err

        log_and_display_summary(systems_res, publications_res, show_summary)
        conn.commit()

        if json_path is not None:
            get_json_resume(conn, systems_res, publications_res, json_path, conf)
            exolog.info(f"The file {json_path} with the summary has been created")
            CONSOLE.print(
                f"\n[bold]The file {json_path} with the summary has been created[bold]",
            )

        exolog.info("The import has completed without error")
        CONSOLE.print("\n🥳 [bold]The import has completed without error.[/bold] ✅")
    except Exception as err:
        exolog.error(f"{type(err).__name__}: {str(err)}")
        if not debug:
            display_error_no_debug(err)
            raise typer.Exit(1)  # noqa: B904
        raise err


@APP.callback()
def main(
    _version: bool = typer.Option(
        False,
        "--version",
        "-v",
        help="Show the application's version and exit.",
        callback=version_callback,
        is_eager=True,
    ),
    debug: bool = typer.Option(
        False,
        "--debug",
        help="Use this option to display the traceback of errors.",
        callback=debug_callback,
        is_eager=True,
    ),
    adv_log: bool = typer.Option(
        False,
        "--advanced-log",
        help="use this option to activate the advanced logging.",
        callback=adv_log_callback,
        is_eager=True,
    ),
    log_path: Optional[Path] = typer.Option(
        None,
        "--logging-cwd-path",
        help="Use this option to give the current work directory of log.",
        callback=log_path_callback,
        is_eager=True,
    ),
) -> None:
    """
    Display the version if we use the option '--version' or '-v'.

    Enable debug mode if we use the option '--debug'.
    Enable advanced log if we use the option '--advanced-log'.
    Give the current work directory of log with '--logging-cwd-path'.

    Args:
        version: Boolean option to know if we want display the version or not.
            Default: False
        debug: Boolean option to enable debug output. Default: False.
        adv_log: Boolean option to enable advanced logging. Default: False.
        log_path: Option to give the current work directory of log. Default: None (./).
    """
    _DEBUG["value"] = debug
    try:
        config_logger(advanced=adv_log, cwd=log_path)
    except (LoggerError) as err:  # pragma: no cover
        if not debug:
            display_error_no_debug(err)
            raise typer.Exit(1)  # noqa: B904
        raise err

    return
