"""
Exo-Import is a program to help in importation in the exoplanets.eu catalog.

Its can be used for all exo-objects (exoplanets, exomoons, discs, comets...).

It is a TYPER program that can launch the different verbs (actions) with their
respective options.
"""

# First party imports
import cli_app
import exoimport

__version__ = exoimport.__version__
__status__ = "development"
__author__ = "Ulysse CHOSSON for LESIA"
__email__ = "ulysse.chosson@obspm.fr"
__copyright__ = "LESIA"
__license__ = "EUPL v1.2"
__contributors__ = ["Pierre-Yves MARTIN (LESIA)"]


def main() -> None:
    """Launch cli application for Exo Import."""
    cli_app.APP()


if __name__ == "__main__":
    main()
