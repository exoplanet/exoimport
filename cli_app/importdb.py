"""Useful function for import typer."""

# Standard imports
import datetime
from json import JSONEncoder
from json import dumps as json_dumps
from pathlib import Path
from typing import Any, NamedTuple, cast

# Third party imports
from psycopg import Connection
from py_linq_sql import SQLEnumerable
from rich.console import Console

# First party imports
from exoimport import Configuration, ImportedLinks, ImportedSystems, get_logger

# Rich console
CONSOLE = Console()

IMPORT_ID = 0
STAR_IDS = 1
PLANET_IDS = 2
STAR_ALTER_NAME_IDS = 3
PLANET_ALTER_NAME_IDS = 4
PUB_ID = 0
STAR2PUB_IDS = 1
PLANET2PUB_IDS = 2
FIRST = 0
LAST = -1


class _CustomJSONEncoder(JSONEncoder):
    """
    Custom JSON encoder to encode a non-serializable type or encoding in other way.

    Non-serializable type :
        - datetime.date -> format in iso string (YYY-MM-dd'T'HH:mm:ss.ms'+'timezone).
        - datetime.dateime -> format in iso string (YYY-MM-dd'T'HH:mm:ss.ms'+'timezone).

    Encoding in some other way:
        - bytes -> Give the size of the file.
    """

    def default(self, obj: datetime.date | datetime.date | bytes) -> str | None:
        """
        Return a serializable object for none-serializable or encoding in other way.

        Args:
            obj: object to serialize.

        Returns:
            Serialized object.
        """
        match obj:
            case datetime.date() | datetime.datetime():
                return obj.isoformat()
            case bytes():
                return f"<file of size {len(obj)} bytes>"
            case _:  # pragma: no cover
                return None


def _get_json(data: dict[str, list[dict[str, Any]]]) -> str:
    """
    Get a json from data.

    Args:
        data: data we want to have in json.

    Returns:
        A json contains data.

    Raises:
        ValueError: Indirect raise by `json.dumps`.
    """
    return json_dumps(data, cls=_CustomJSONEncoder)


def _rows_to_dict(rows: list[NamedTuple]) -> list[dict[str, Any]]:
    """
    Convert Pyscopg Rows in a dictionary.

    Args:
        rows: All rows to convert.

    Returns:
        All rows converted in dictionaries.
    """
    result = []
    for row in rows:
        res = {}
        for name in row._fields:
            res[name] = getattr(row, name)
        result.append(res)

    return result


def get_json_resume(
    conn: Connection,
    systems: ImportedSystems,
    pub_and_links: tuple[int, list[ImportedLinks]],
    json_path: Path,
    conf: Configuration,
) -> None:
    """
    Get a json summary of an import and write it in a file.

    Args:
        conn: Connection to the database.
        systems: All systems ids.
        pub_and_links: All publication and link ids.
        json_path: Path of the json file will be written.
        conf: Configuration for the import.
    """
    res = {}
    systems_ids = _get_ids_from_systems(systems)
    pub_link_ids = _get_ids_from_pub_and_link(pub_and_links)
    tables_name = conf.tables

    res_import = (
        SQLEnumerable(conn, tables_name.import_table)
        .select()
        .where(lambda x: x.id == systems_ids[IMPORT_ID])
        .execute()
    )

    res[tables_name.import_table] = _rows_to_dict(cast(list, res_import))

    res_star = (
        SQLEnumerable(conn, tables_name.star_table)
        .select()
        .where(lambda x: x.id >= int(systems_ids[STAR_IDS][FIRST]))
        .where(lambda x: x.id <= int(systems_ids[STAR_IDS][LAST]))
        .execute()
    )

    res[tables_name.star_table] = _rows_to_dict(cast(list, res_star))

    if systems_ids[STAR_ALTER_NAME_IDS]:
        res_star_alter = (
            SQLEnumerable(conn, tables_name.alternate_star_name_table)
            .select()
            .where(lambda x: x.id >= int(systems_ids[STAR_ALTER_NAME_IDS][FIRST]))
            .where(lambda x: x.id <= int(systems_ids[STAR_ALTER_NAME_IDS][LAST]))
            .execute()
        )

        res[tables_name.alternate_star_name_table] = _rows_to_dict(
            cast(list, res_star_alter)
        )
    else:
        res[tables_name.alternate_star_name_table] = []

    res_planet = (
        SQLEnumerable(conn, tables_name.planet_table)
        .select()
        .where(lambda x: x.id >= int(systems_ids[PLANET_IDS][FIRST]))
        .where(lambda x: x.id <= int(systems_ids[PLANET_IDS][LAST]))
        .execute()
    )

    res[tables_name.planet_table] = _rows_to_dict(cast(list, res_planet))

    if systems_ids[PLANET_ALTER_NAME_IDS]:
        res_planet_alter = (
            SQLEnumerable(conn, tables_name.alternate_planet_name_table)
            .select()
            .where(lambda x: x.id >= int(systems_ids[PLANET_ALTER_NAME_IDS][FIRST]))
            .where(lambda x: x.id <= int(systems_ids[PLANET_ALTER_NAME_IDS][LAST]))
            .execute()
        )

        res[tables_name.alternate_planet_name_table] = _rows_to_dict(
            cast(list, res_planet_alter)
        )
    else:
        res[tables_name.alternate_planet_name_table] = []

    res_publi = (
        SQLEnumerable(conn, tables_name.publication_table)
        .select()
        .where(lambda x: x.id == pub_link_ids[PUB_ID])
        .execute()
    )

    res[tables_name.publication_table] = _rows_to_dict(cast(list, res_publi))

    res_star2pub = (
        SQLEnumerable(conn, tables_name.star2publication_table)
        .select()
        .where(lambda x: x.id >= pub_link_ids[STAR2PUB_IDS][FIRST])
        .where(lambda x: x.id <= pub_link_ids[STAR2PUB_IDS][LAST])
        .execute()
    )

    res[tables_name.star2publication_table] = _rows_to_dict(cast(list, res_star2pub))

    res_planet2pub = (
        SQLEnumerable(conn, tables_name.planet2publication_table)
        .select()
        .where(lambda x: x.id >= pub_link_ids[PLANET2PUB_IDS][FIRST])
        .where(lambda x: x.id <= pub_link_ids[PLANET2PUB_IDS][LAST])
        .execute()
    )

    res[tables_name.planet2publication_table] = _rows_to_dict(
        cast(list, res_planet2pub)
    )

    with open(json_path, "w") as json_outfile:
        json_outfile.write(_get_json(res))


def _get_ids_from_systems(
    systems: ImportedSystems,
) -> tuple[str, list[str], list[str], list[str], list[str]]:
    """
    Get import id, star ids and planet ids from a ImportedSystems result.

    Args:
        systems: Import systems result which contains all ids.

    Returns:
        The import id, all star ids and all planet ids.
    """
    import_id = str(systems[0])
    sys_ids = systems[1]

    star_ids: list[str] = []
    planet_ids: list[str] = []
    star_alter_ids: list[str] = []
    planet_alter_ids: list[str] = []

    for sys in sys_ids:
        star_ids.append(sys.star_id)
        for star_alter in sys.star_alter_id:
            star_alter_ids.append(star_alter)
        for planet in sys.planets_id:
            planet_ids.append(planet)
        for planet_alter in sys.planets_alter_id:
            for alter in planet_alter:
                planet_alter_ids.append(alter)

    return (import_id, star_ids, planet_ids, star_alter_ids, planet_alter_ids)


def _get_ids_from_pub_and_link(
    pub_and_links: tuple[int, list[ImportedLinks]],
) -> tuple[str, list[int], list[int]]:
    """
    Get publication id and all link id on star and planet.

    Args:
        pub_and_links: Publication id and all links ids to planet and star.

    Results:
        The publication id, all star2publication link ids and all planet2publication
        link ids
    """
    pub_id = str(pub_and_links[0])
    link_ids = pub_and_links[1]

    star2pub_ids = []
    planet2pub_ids = []

    for link in link_ids:
        star2pub_ids.extend(list(link[0].values()))
        for planet_link in link[1]:
            planet2pub_ids.extend(list(planet_link.values()))

    return (pub_id, star2pub_ids, planet2pub_ids)


def log_and_display_summary(
    systems_res: ImportedSystems,
    publications_res: tuple[int, list[ImportedLinks]],
    display: bool,
) -> None:
    """
    Log (and possibly display) a summary of all imported ids and their number.

    Args:
        systems_res: Import systems result which contains all ids.
        publications_res: Publication id and all links ids to planet and star.
        display: True to display summary, False otherwise.
    """
    exolog = get_logger()

    sys_ids_to_display = _get_ids_from_systems(systems_res)
    pub_ids_to_display = _get_ids_from_pub_and_link(publications_res)

    str_sys_star_ids = ",".join(str(st_id) for st_id in sys_ids_to_display[1])
    str_sys_planet_ids = ",".join(str(pl_id) for pl_id in sys_ids_to_display[2])
    str_sys_star_alter_ids = ",".join(str(st_alt) for st_alt in sys_ids_to_display[3])
    str_sys_planet_alter_ids = ",".join(str(pl_alt) for pl_alt in sys_ids_to_display[4])
    str_pub_star2pub_ids = ",".join(str(st_link) for st_link in pub_ids_to_display[1])
    str_pub_planet2pub_ids = ",".join(str(pl_link) for pl_link in pub_ids_to_display[2])

    imported_rows = (
        2
        + len(sys_ids_to_display[1])
        + len(sys_ids_to_display[2])
        + len(sys_ids_to_display[3])
        + len(sys_ids_to_display[4])
        + len(pub_ids_to_display[2])
        + len(pub_ids_to_display[1])
    )

    exolog.info(f"import id: {str(sys_ids_to_display[0])}")
    exolog.info(f"star id(s): [{str_sys_star_ids}]")
    exolog.info(f"planet id(s): [{str_sys_planet_ids}]")
    exolog.info(f"star alternate name id(s): [{str_sys_star_alter_ids}]")
    exolog.info(f"planet alternate name id(s): [{str_sys_planet_alter_ids}]")
    exolog.info(f"publication id: {pub_ids_to_display[0]}")
    exolog.info(f"star2publication id(s): {str_pub_star2pub_ids}")
    exolog.info(f"planet2publication id(s): {str_pub_planet2pub_ids}")
    exolog.info(f"{imported_rows} imported rows")
    exolog.info("1 imported import")
    exolog.info(f"{len(sys_ids_to_display[1])} imported stars")
    exolog.info(f"{len(sys_ids_to_display[2])} imported planets")
    exolog.info(f"{len(sys_ids_to_display[3])} imported star alternate names")
    exolog.info(f"{len(sys_ids_to_display[4])} imported planet alternate names")
    exolog.info("1 imported publication")
    exolog.info(f"{len(pub_ids_to_display[1])} imported star2publication links")
    exolog.info(f"{len(pub_ids_to_display[2])} imported planet2publication links")

    if display:
        CONSOLE.print("\nImported ids:")
        CONSOLE.print(f"  Import id: {sys_ids_to_display[0]}")
        CONSOLE.print(f"  Star id(s): [{str_sys_star_ids}]")
        CONSOLE.print(f"  Planet id(s): [{str_sys_planet_ids}]")
        CONSOLE.print(f"  Star alternate name id(s): [{str_sys_star_alter_ids}]")
        CONSOLE.print(f"  Planet alternate name id(s): [{str_sys_planet_alter_ids}]")
        CONSOLE.print(f"  Publication id: {pub_ids_to_display[0]}")
        CONSOLE.print(f"  Star2Publication link: [{str_pub_star2pub_ids}]")
        CONSOLE.print(f"  Planet2Publication link: [{str_pub_planet2pub_ids}]")
        CONSOLE.print(f"\n{imported_rows} imported rows")
        CONSOLE.print("  1 imported import")
        CONSOLE.print(f"  {len(sys_ids_to_display[1])} imported star(s)")
        CONSOLE.print(f"  {len(sys_ids_to_display[2])} imported planet(s)")
        CONSOLE.print(f"  {len(sys_ids_to_display[3])} imported star alternate names")
        CONSOLE.print(f"  {len(sys_ids_to_display[4])} imported planet alternate names")
        CONSOLE.print("  1 imported publication")
        CONSOLE.print(f"  {len(pub_ids_to_display[1])} imported star2publication links")
        CONSOLE.print(
            f"  {len(pub_ids_to_display[2])} imported planet2publication links",
        )
