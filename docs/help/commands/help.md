# Get documentation on commands and some tutorials

**Name:**

&ensp;exoimport-help  - Display help information about Exo-Import

**Usage:**

&ensp;exoimport help [--help] [COMMAND|TUTORIAL]

**Description:**

&ensp;With no COMMAND or TUTORIAL given, `help` the resume of all Exo-Import command are
printed on the standard output.

&ensp;If a command, or a tutorials, is given, a manual page for that command or tutorial
is printed on the standard output.
