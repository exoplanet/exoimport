# Import a csv normalized file in exoplanet.eu

**Name:**

&ensp;exoimport-import  - Import a normalized csv file in the exoplanet.eu database.

**Usage:**

&ensp;exoimport import [--help] [--config-file] [--show-summary/-s] EXTERNAL_CSV_FILE
MAPPING_FILE EXOPLANET_CSV_FILE

**Description:**

&ensp;This command `import` data form a normalized csv file into [exoplanet.eu](http://exoplanet.eu/)
database.

&ensp;You need to configure the import with a config file given to the command line or
use one of three default config file: 'merge_rules.yml', 'merge_rules.yaml' and 'pyptoject.toml'.
You can see a tutorials on config with the command: `exoimport help config`

**Arguments:**

* EXTERNAL_CSV_FILE: Base csv file with none normalized data. The file that was used to
produce the normalized csv file with `exoimport translate`. You can see a tutorial on
"How to write csv headers!" with the command : `exoimport help headers`.

* MAPPING_FILE: Mapping file use for the translation with `exoimport translate`. You can
see a tutorial on "How to write a mapping yaml file!" with the command: `exoimport help mapping`.

* EXOPLANET_CSV_FILE: The normalized csv file produce by `exoimport translate` using
the two previous files. It is the data from this file that will be imported.

**Options:**

&ensp;--help: Display help information about the command `import`

&ensp;--epsilon: Allow you to pass the epsilon configuration in command line.

&ensp;--web-statuses: Allow you to pass the web statuses configuration in command line.

&ensp;--config-file: Allow you to pass a config file. If nothing is given the default
config file will be used. Warning the epsilon et web-statuses configuration in command
line will always take precedence over the configuration file.

&ensp;--json-output-path: Allow you to pass a json file to get all information on
imported data in json format.

&ensp;--show-summary/--no-show-summary: Activate by default. Allow you to control if
`exoimport import` display the summary, all imported ids and their number, or not.

**Examples:**

&ensp; * Make an import of a normalized csv file:

&ensp;    $ exoimport import tess_csv.csv mapping_tess.yaml exoplanet.csv

&ensp; * Make an import with epsilon config:

&ensp;    $ exoimport import --epsilon=1 tess.csv mapping_tess.yaml exoplanet.csv

&ensp; * Make an import with web-statuses config:

&ensp;    $ exoimport import --web-statuses=active tess.csv mapping_tess.yaml exoplanet.csv

&ensp; * Make an import with a config file:

&ensp;    $ exoimport import --config-file=config.yml tess.csv mapping_tess.yaml exoplanet.csv

&ensp; * Make an import without summary:

&ensp;    $ exoimport import --no-show-summary tess.csv mapping_tess.yaml exoplanet.csv

&ensp; * Make an import with a json output file:

&ensp;    $ exoimport import --no-show-summary tess.csv mapping_tess.yaml exoplanet.csv --json-output-path=exoimport_summary.json

**See also:**

&ensp;`exoimport-help generate-config` `exoimport-help config` `exoimport-help config`
