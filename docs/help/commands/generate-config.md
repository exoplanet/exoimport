# Generate a config file

/!\ For the moment the generation of config don't support generation of table names.

**Name:**

&ensp;exoimport-generate-config  - Generate a config file for an import.

**Usage:**

&ensp;exoimport import [--help] [--extension]

**Description:**

&ensp; This command allow you to generate a config file with all mandatory config values.

&ensp; The command ask you the extension of the file (if not given with --extension option),
the epsilon and the web_statuses values.

**Options:**

&ensp;--help: Display help information about the command `generate-config`

&ensp;--extension: Allow you to pass the config file extension in command line.
The config file can be a toml or yaml file.

**Examples:**

&ensp; * Generate a config with without given options:

&ensp;    $ exoimport generate-config

&ensp; * Generate a config with the toml extension given in command line:

&ensp;    $ exoimport generate-config --extension=toml

&ensp; * Generate a config with the yaml extension given in command line:

&ensp;    $ exoimport generate-config --extension=yaml

**See also:**

&ensp;`exoimport-help web_statuses` `exoimport-help epsilon` `exoimport-help config`
