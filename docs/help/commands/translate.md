# Translate a csv into a normalized csv for exoplanet.eu with a mapping

**Name:**

&ensp;exoimport-translate  - Translate a csv file into a normalized csv for exoplanet.eu

**Usage:**

&ensp;exoimport translate [--help] [--validate] [--filter] [--translate]
[--filtered-path] [--kept-path] [--translate-path] CSV_FILE MAPPING_FILE

**Description:**

&ensp;This command `translate` a csv file from a space database (Nasa, Gaia, ..) into a
normalized csv for [exoplanet.eu](http://exoplanet.eu/).

&ensp;By default `translate` write the csv for [exoplanet.eu](http://exoplanet.eu/) in a
file named: *translated.csv*. You can change this with the option `--translate-path`.

&ensp;The command `translate` allow you to make validation of csv headers and mapping file.
You can have the filtration output the filtered columns and the kept columns to files
named: *out.csv* and *keep.csv*. You can change this with options `--filtered-path.csv`
and `kept-path.csv`.

&ensp;To translate a csv we need a mapping file to match the columns of the csv to the
fields of the [exoplanet.eu](http://exoplanet.eu/) catalog database.

&ensp;The mapping file is a yaml file with `key: value` pairs so that's the program knows
how to reduce the translate the input csv.

**Arguments:**

* CSV_FILE: File to translate, you need to respect some rules for headers. You can
see a tutorial on "How to write csv headers!" with the command : `exoimport help headers`.

* MAPPING_FILE: Yaml file contains the mapping to match the csv headers with csv
headers for [exoplanet.eu](http://exoplanet.eu/).
You can see a tutorial on "How to write a mapping yaml file!" with the
command: `exoimport help mapping`.

**Options:**

&ensp;--help: Display help information about the command `translate`

&ensp;--validate/--no-validate: Activate by default. Allow you to make the translation
without validation. Use *--no-validate* to make the translation without validation.

&ensp;--filter/--no-filter: Activate by default. Allow you to make the translation
without filtration. Use *--no-filter* to make the translation without filtration.

&ensp;--translate/--no-translate: Activate by default. Allow you to make the translation
without translation :surprise-face:. In reality this option allow you to make the
validation and/or filtration without translation. Use *--no-translate* to make the
validation and/or filtration without translation.

&ensp;--filtered-path: Allow you to give a path for filtered rows (rows eliminated
by the filter). By default the path is: *out.csv*.

&ensp;--kept-path: Allow you to give a path for kept rows (rows kept
by the filter). By default the path is: *keep.csv*.

&ensp;--translate-path: Allow you to give a path for translated rows (the final csv).
By default the path is: *translated.csv*.

**Examples:**

&ensp; * Make a validation, filtration and translation of a csv file:

&ensp;    $ exoimport translate tess.csv mapping_tess.yaml

&ensp; * Make a validation, filtration and translation of a csv file with an output csv file:

&ensp;    $ exoimport translate tess.csv mapping_tess.yaml --translate-path=tess_for_exoplanet.csv

&ensp; * Make only validation and filtration of a csv file:

&ensp;    $ exoimport translate tess.csv mapping_tess.yaml --no-translate

&ensp; * Make a validation, filtration and translation of a csv file and give a path
for the filtered rows:

&ensp;    $ exoimport translate tess.csv mapping_tess.yaml --filtered-path=get_out_tess.csv

**See also:**

&ensp;`exoimport-help headers` `exoimport-help mapping`
