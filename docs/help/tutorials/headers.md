# How the headers of a csv should be written

To write headers you need to respect some rules.

Those rules are simple and are 4 in number.
It allows exoimport to interpret the headers without worries.

1. A header name must start with a letter or the underscore character.
2. A header name cannot start with a number.
3. A header name can only contain alphanumeric characters and underscores:
    (abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_) and **NO SPACE**
4. Header names are case-sensitive (age, Age and AGE are three different variable)

**Examples:**

- These headers are valid:
  - age
  - age5
  - age5age
  - age_age
  - age_567age_
  - _age
  - _age5
  - _age_age5
  - Age
  - Age5age_AGE
  - AGE_

- These headers are not valid:
  - 1age -> *start with a number, '1'*
  - 1_age -> *start with a number, '1'*
  - -age -> *start with a symbol, '-'*
  - age-2 -> *contains a symbol, '-'*
  - .age/3 -> *contains a symbol, '/'*
  - is_valid? -> *contains a symbol, '?'*
  - age=age+2 -> *contains 2 symbol, '+' and '='*
  - simplified age -> *contains a space*
  - "age" -> *start with a symbol and contains the same symbol, '"'*
