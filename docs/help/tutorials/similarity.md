# Similarity

## Similarity between 2 planets

Two planets are considered identical if they have the same name. Specifically the name
of planet #2 is the same as that of planet #1 or if it is included in the alternative
names of the planet #1.

## Similarity between 2 stars

To know if 2 stars are identical, we have 2 criteria:

- the name
- the position

**The name:**

Two stars are considered identical if they have the same name. Specifically the name
of star #2 is the same as that of star #1 or if it is included in the alternative
names of the star #1.

**The position:**

For the position we use the angular distance formula. You can read the
[wikipedia topics](https://en.wikipedia.org/wiki/Angular_distance) for more information.

$$\theta = \cos^{-1}\left[\sin\delta_A \sin\delta_B + \cos\delta_A \cos\delta_B $$
$$\cos(\alpha_A - \alpha_B)\right]$$

With $\delta$ for DEC and $\alpha$ for RA.

If $\theta$ <= `epsilon`. The two star are the same.

`Epsilon` fix the tolerance for star position. The interval in which we consider
that another star is already at this position and therefore that it is the same star.
`Epsilon` is given in arcsec.

In addition to that we check if the RA and the DEc are exactly the same. We must be
check this because postgresql make `cos()` and `sin()` in float and if ra and dec are
the same the floating error cause a false negative.

You can read more explanation here:

[link 1](https://stackoverflow.com/questions/2533386/postgresql-error-error-input-is-out-of-range)
[link 2](https://www.postgresql.org/message-id/12376732.post@talk.nabble.com) -->

For all those formula we choose 20 significant digit. These two numbers have the same
significant digit: 20

- 1.0723692508781812416
- 0.12452756813273719415

## Rogue Planetes

For rogue planets we use the same method as for the stars.

## Examples

**Planets:**

- One: (name: tess.001.01, alternate names: [t001.01, gaia.235 a])
- Two: (name: tess.002.01, alternate names: [t002.02, gaia.236 a])
- Three: (name: tess.001.01, alternate names: [Kepler-6789 a, k.6789.01])
- Four: (name: TOI-3456 a, alternate names: [tess.001.01, TOI.3456.01])

Give:

- `One` and `Two` are different.
- `One` and `Three` are same because they have the same name.
- `One` and `Four` are same because one of alternate name of `Four` are the name of `One`.

**Stars:**

- One: (name: tess.01, alternate names: [t001, gaia.235], ra: 12.451, dec: -5.782)
- Two: (name: tess.02, alternate names: [t002, gaia.236], ra: 0.648, dec: -73.639)
- Three: (name: tess.03, alternate names: [t003, gaia.237], ra: 12.451, dec: -5.782)
- Four: (name: tess.04, alternate names: [t004, gaia.238], ra: 12.452, dec: -5.781)
- Five: (name: tess.01, alternate names: [Kepler-6789, k.6789], ra: 98.784, dec: 97.003)
- Six: (name: TOI-3456, alternate names: [TOI.3456, tess.01], ra: 45.839, dec: 3.001)

Give:

- `One` and `Two` are different.
- `One` and `Three` are the same because they are at the same position.
- `One` and `Four` are the same because they are at the same position.
- `One` and `Five` are the same because they have the same name.
- `One` and `Six` are the same because one of alternate name of `Six` are the name of `One`.

**Rogue planets:**

Exactly like stars.
