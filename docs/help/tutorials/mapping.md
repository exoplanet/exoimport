# How to write a mapping file

To write a mapping file you need [the basic mapping yaml file](https://gitlab.obspm.fr/exoplanet/exoimport/-/blob/main/docs/mapping.yaml).

The field filter is optional and allow you to write a filter for the csv file.
This filter must be write with a python syntax.
You can see examples lower.

You need to fill fields with a formula based on the csv headers you want translate.
This must respect the python syntax.
You can see examples lower.

One function is allow in addition in mapping files:

```python
>>> def val(value: str) -> str:
...     return value

>>> print(val("hello world"))
hello world

```

`val()` allow you to fix a value for all rows.
You can see examples lower.

**Examples:**

&ensp;***Filter examples***

&ensp; * Write a filter with an equality.

```yaml
filter: status == "Confirmed"
```

&ensp; * Write a filter with a comparison

```yaml
filter: mass <= 60
```

&ensp;    You can also write a filter with various filter.

```yaml
filter: mass <= 60 and status == "Confirmed"
```

&ensp;***Formula examples***

&ensp; * Write a field with a simple formula, the 'basic' formula is just the name of a
header.

```yaml
temp_meas_Fah: pl_temp_deg
```

&ensp; * Write a field with a more complex formula.

```yaml
temp_meas_Cel: (pl_temp_deg - 32) * 5/9
```

```yaml
negative_temp_meas: -pl_temp_deg
```

&ensp;***Val examples***

&ensp; * Write a field with a value.

```yaml
status: val("Confirmed")
```
