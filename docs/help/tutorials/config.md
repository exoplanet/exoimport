# How the config file should be written

You can pass a path of your config file to `exoimport import`.
This file must be a yaml or toml file (.toml, .yml, .yaml).

If no config file was given in the command line, `exoimport` take one of those file:

- merge_rules.yaml
- merge_rules.yml
- pyproject.toml

In this order.

The config file need to have some mandatory keys:

- epsilon_ra_dec (arcsec)
- web_statuses

**Epsilon RA DEC:**

`epsilon_ra_dec` fix the tolerance for star position. The interval in which we consider
that another star is already at this position and therefore that it is the same star.

`epsilon_ra_dec` is given in arcsec.

For more information on the Angular distance formula you can read the documentation on
similarity.

**Web statuses:**

`web_statuses` fix the web status (if we display it on the website or not) for all
imported rows. 4 values ​​are possible:

- active
- hidden
- imported
- suggested

**Tables:**

`tables` fix all tables name example for the planet table in db.
All tables name are mandatory.
`tables` is a dictionary.

**Give configuration in command line:**

You can also give the configuration on the command line with the options '--epsilon',
'--web-statuses' --'tables'. They will always take precedence over the configuration file.

You don't have to enter all config on the command line. In this case the missing
configurations will be taken from the config file.

If you give tables in command line you need to suffix all table name by `_table`.

**Generate configuration:**

You can also ask to `exoimport` to generate for you a config file with the command:
`generate-config`.

**Examples:**

For a yaml file:

```yaml
exoimport:
  epsilon_ra_dec: 5
  web_statuses: active
  tables:
    import: "app_import"
    star: "app_star"
    alternate_star_name: "app_alternate_star_name"
    planet: "app_planet"
    alternate_planet_name: "app_alternate_planet_name"
    molecule: "app_molecule"
    atmoshpere_molecule: "app_atmoshpere_molecule"
    publication: "app_publication"
    planet2publication: "app_planet2publication"
    star2publication: "app_star2publication"
    planetary_system: "app_planetary_system"
    planet2star: "app_planet2star"
```

For a toml file:

```toml
[tool.exoimport]
epsilon_ra_dec = 1
web_statuses = "active"

[tool.exoimport.tables]
import = "app_import"
star = "app_star"
alternate_star_name = "app_alternate_star_name"
planet = "app_planet"
alternate_planet_name = "app_alternate_planet_name"
molecule = "app_molecule"
atmoshpere_molecule = "app_atmoshpere_molecule"
publication = "app_publication"
planet2publication = "app_planet2publication"
star2publication = "app_star2publication"
planetary_system = "app_planetary_system"
planet2star = "app_planet2star"
```

In command line with all config

```sh
exoimport import tess.csv mapping.yml exoplanet.csv --epsilon=1 --web-statuses=active
--tables={import_table="core_import", ...}
```

In command line without all config but with a config file

```sh
exoimport import tess.csv mapping.yml exoplanet.csv --epsilon=1 --config-file=conf.yml
```

Generate configuration file

```sh
exoimport generate-config
```
