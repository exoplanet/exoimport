# What is web statuses?

`web_statuses` fix the web status (if we display it on the website or not) for all
imported rows. 4 values ​​are possible:

- active: We display it.
- hidden: We don't display it.
- imported: ??
- suggested: ??
