# What is epsilon?

`epsilon_ra_dec` fix the tolerance for star position. The interval in which we consider
that another star is already at this position and therefore that it is the same star.

`epsilon_ra_dec` is given in arcsec.

For more information on the Angular distance formula you can read the documentation on
similarity and the [wikipedia topic](https://en.wikipedia.org/wiki/Angular_distance).

**See also:**

&ensp;`exoimport-help similarity`
