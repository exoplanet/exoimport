"""Function to get sample of mapping file."""

# Standard imports
from pathlib import Path

MAPPING_FILE = Path(__file__).parent / "mapping.yaml"


def get_sample_mapping() -> str:
    """
    Get a sample of the mapping file.

    Returns:
        The sample from the default mapping file.
    """
    with open(MAPPING_FILE, encoding="utf-8") as sample_mapping:
        return sample_mapping.read()
