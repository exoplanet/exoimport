"""Function to get help for cli_app."""

# Standard imports
from pathlib import Path

# Third party imports
from rich.markdown import Markdown

BASEDIR = Path(__file__).parent / Path("help")


def _get_sorted_topics(paths: list[Path]) -> dict[str, list[str]]:
    """
    Get all topics sorted by category.

    All category are the folder name in which is the topic.

    Args:
        paths: All topics.

    Returns:
        All topics sorted by category.

    Examples:
        >>> paths = [
        ...     Path("docs/help/commands/help.md"),
        ...     Path("docs/help/commands/translate.md"),
        ...     Path("docs/help/tutorials/headers.md"),
        ...     Path("docs/help/toto.md"),
        ... ]
        >>> _get_sorted_topics(paths)
        {'commands': ['help.md', 'translate.md'], 'tutorials': ['headers.md']}
    """
    result: dict[str, list[str]] = {}

    for path in paths:
        parent = path.parent.name
        if parent == "help":
            pass
        elif parent in result:
            result[parent].append(path.name)
        else:
            result[parent] = [path.name]

    return result


def _get_spaces(max_len: int, topic: str) -> str:
    """
    Get the good number of space in relation to the maximum length of topics.

    The good number of spaces is 3 plus the difference between the max_len and the len
    of the current topic.

    Args:
        max_len: Maximum length of topics.
        topic: Current topic.

    Returns:
        Good number of spaces.

    Examples:
        >>> _get_spaces(8, "toto")
        '       '
        >>> _get_spaces(8, "spaceman")
        '   '
    """
    nb_space = 3 + max_len - len(topic)
    return " " * nb_space


def get_resume() -> str:
    """
    Get the resume of the help. How to use the command `help`.

    The usage of the program and all the arguments can be call with help.

    This list of arguments is sorted by category,
    all category are the folder name in which is the topic.

    The description of an argument is the first line of his file.

    This message are dynamically built relative to the contents of the
    ./docs/help/ folder.

    Returns:
        The correct message for the command `help`.
    """
    result = [
        "\n[bold]Usage:[/bold] [bold white]exo-import [--version] [--debug] "
        "[--advanced-log] [----logging-cwd-path] [--help] COMMAND [<args>]"
        "[/bold white]\n\n",
    ]
    topics_paths = BASEDIR
    topics = sorted(topics_paths.glob("**/*.md"))

    max_len = max([len(topic.name) for topic in topics])
    sorted_topics = _get_sorted_topics(topics)

    for key, value in sorted_topics.items():
        result.append(f"{key.title()}\n")
        for topic in value:
            res = []
            # Get the name of file without extension `.md`
            res.append(f"\t[bold]{topic[0:-3]}[/bold]")

            # Get the good number of space
            res.append(_get_spaces(max_len, topic))

            # Get the description of the command or the tutorials
            with open(topics_paths / key / topic, encoding="utf-8") as md_file:
                res.append(md_file.readline()[2:])

            result.append("".join(res))
        result.append("\n")

    result.pop()
    return "".join(result)


def display_md_file_content(file_name: str) -> tuple[bool, str | Markdown]:
    """
    Get the content of a md file in ./docs/help folder or a warning message with resume.

    Args:
        file_name: Name of the md file that we want to display.

    Returns:
        True with he content of a help or False and a warning message and the resume.

    """
    topics_paths = BASEDIR
    md_file = list(topics_paths.glob(f"**/{file_name}.md"))

    if not md_file:
        result_msg = [
            "[black on orange3] Warning [/black on orange3]"
            "[bold] :warning: The argument isn't valid, "
            "try with those arguments [/bold]\n",
        ]
        result_msg.append(get_resume())
        return (False, "".join(result_msg))

    with open(md_file[0], encoding="utf-8") as opened_file:
        markdown = Markdown(opened_file.read())

    return (True, markdown)
