<!-- markdownlint-disable-file MD024 MD041 MD029 -->
# ExoImport

![maintenance](https://img.shields.io/maintenance/yes/2023)
![open issue](https://img.shields.io/gitlab/issues/open-raw/exoplanet/exoimport?gitlab_url=https%3A%2F%2Fgitlab.obspm.fr%2F)

[![pipeline status](https://gitlab.obspm.fr/exoplanet/exoimport/badges/main/pipeline.svg)](https://gitlab.obspm.fr/exoplanet/exoimport/-/commits/main)
[![coverage report](https://gitlab.obspm.fr/exoplanet/exoimport/badges/main/coverage.svg)](https://gitlab.obspm.fr/exoplanet/exoimport/-/commits/main)

![banner](banner.png)

A usfull tool to make import from external sources into [exoplanet.eu](http://exoplanet.eu/)
database.

## Credits

- Author: CHOSSON Ulysse
- Maintenair: CHOSSON Ulysse
- Email: ulysse.chosson@obspm.fr
- Contributors:
  - MARTIN Pierre-Yves <pierre-yves.martin@obspm.fr>
  - KRAL Quentin <quentin.kral@obspm.fr>
  - ROQUES Francoise <francoise.roques@obspm.fr>

## Installation

1. Install poetry locally.

```sh
curl -sSL https://install.python-poetry.org | python3 -
```

2. Install exoimport.

```sh
pip3 install git+https://gitlab.obspm.fr/exoplanet/exoimport.git
```

## Usage

To use `exoimport` you need to create a `.env` file with your connection information to
the database.

Example

```.env
EXOIMPORT_DBUSER = toto
EXOIMPORT_DBPASSWORD = totopassword
EXOIMPORT_DBHOST = localhost
EXOIMPORT_DBPORT = 5432
EXOIMPORT_DBNAME = exoimport
```

Note: You can also export each variable.

## Command line usage

`Exoimport` is separated into 2 parts.

### Translate

```sh
exoimport translate [OPTIONS] csv_file mapping_file
```

Translate allows you to convert a csv file from an external source into a normalized csv
file for [exoplanet.eu](http://exoplanet.eu/).
To make this you need to write a mapping file in yaml.

For more information you can see documentation with the following command lines.

```sh
$exoimport translate --help
$exoimport help
$exoimport help translate
$exoimport help headers
$exoimport help mapping
```

### Import

```sh
exoimport import [OPTIONS] external_csv_file mapping_file exoplanet_csv_file
```

Import allows you to import a normalized csv file for [exoplanet.eu](http://exoplanet.eu/)
into the database.
To make this you need a normalized csv file (obtained with `exoimport translate`) and some
configuration.

To write the config file you can read the documentation of configuration or generate it
with the command `exoimport generate-config`.

For more information you can see documentation with the following command lines.

```sh
$exoimport import --help
$exoimport help
$exoimport help import
$exoimport help config
$exoimport help epsilon
$exoimport help web_statuses
$exoimport help similarity
```

## Encountered any bugs?

If you encounter any bugs or have any suggestions for improvements, please open an issue
on the repository.

## License

This project is licensed und the [EUPL v1.2 License](LICENSE.md)
