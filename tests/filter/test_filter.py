# Pytest imports
import pytest
from pytest import param

# Standard imports
from pathlib import Path

# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from exoimport import (
    CsvDictData,
    PythonSyntaxError,
    format_csv,
    input_filter,
    read_csv,
    read_yaml,
)

TEST_PATH = "tests/input_files"


@pytest.mark.parametrize(
    "filter, content, expected_res, expected_msg",
    [
        param(
            "toto > 10",
            [
                {"toto": 11, "titi": 12, "tutu": "Candidate"},
                {"toto": 7, "titi": 8, "tutu": "Candidate"},
                {"toto": 15, "titi": 16, "tutu": "Candidate"},
            ],
            [
                {"toto": 11, "titi": 12, "tutu": "Candidate"},
                {"toto": 15, "titi": 16, "tutu": "Candidate"},
            ],
            [
                "  [bold green on black]2 rows[/bold green on black] kept out of "
                "[white on black]3[/white on black], [bold orange3 on black]1 rows"
                "[/bold orange3 on black] filtered.",
            ],
            id="Filter: greater.",
        ),
        param(
            "toto >= 10",
            [
                {"toto": 11, "titi": 12, "tutu": "Candidate"},
                {"toto": 7, "titi": 8, "tutu": "Candidate"},
                {"toto": 15, "titi": 16, "tutu": "Candidate"},
            ],
            [
                {"toto": 11, "titi": 12, "tutu": "Candidate"},
                {"toto": 15, "titi": 16, "tutu": "Candidate"},
            ],
            [
                "  [bold green on black]2 rows[/bold green on black] kept out of "
                "[white on black]3[/white on black], [bold orange3 on black]1 rows"
                "[/bold orange3 on black] filtered.",
            ],
            id="Filter: greater or equal.",
        ),
        param(
            "toto < 11",
            [
                {"toto": 11, "titi": 12, "tutu": "Candidate"},
                {"toto": 7, "titi": 8, "tutu": "Candidate"},
                {"toto": 15, "titi": 16, "tutu": "Candidate"},
            ],
            [
                {"toto": 7, "titi": 8, "tutu": "Candidate"},
            ],
            [
                "  [bold green on black]1 rows[/bold green on black] kept out of "
                "[white on black]3[/white on black], [bold orange3 on black]2 rows"
                "[/bold orange3 on black] filtered.",
            ],
            id="Filter: lesser.",
        ),
        param(
            "toto <=11",
            [
                {"toto": 11, "titi": 12, "tutu": "Candidate"},
                {"toto": 7, "titi": 8, "tutu": "Candidate"},
                {"toto": 15, "titi": 16, "tutu": "Candidate"},
            ],
            [
                {"toto": 11, "titi": 12, "tutu": "Candidate"},
                {"toto": 7, "titi": 8, "tutu": "Candidate"},
            ],
            [
                "  [bold green on black]2 rows[/bold green on black] kept out of "
                "[white on black]3[/white on black], [bold orange3 on black]1 rows"
                "[/bold orange3 on black] filtered.",
            ],
            id="Filter: lesser or equal.",
        ),
        param(
            "toto == 11",
            [
                {"toto": 11, "titi": 12, "tutu": "Candidate"},
                {"toto": 7, "titi": 8, "tutu": "Candidate"},
                {"toto": 15, "titi": 16, "tutu": "Candidate"},
            ],
            [
                {"toto": 11, "titi": 12, "tutu": "Candidate"},
            ],
            [
                "  [bold green on black]1 rows[/bold green on black] kept out of "
                "[white on black]3[/white on black], [bold orange3 on black]2 rows"
                "[/bold orange3 on black] filtered.",
            ],
            id="Filter: equal.",
        ),
        param(
            "toto != 10",
            [
                {"toto": 11, "titi": 12, "tutu": "Candidate"},
                {"toto": 7, "titi": 8, "tutu": "Candidate"},
                {"toto": 15, "titi": 16, "tutu": "Candidate"},
            ],
            [
                {"toto": 11, "titi": 12, "tutu": "Candidate"},
                {"toto": 7, "titi": 8, "tutu": "Candidate"},
                {"toto": 15, "titi": 16, "tutu": "Candidate"},
            ],
            [
                "  [bold green on black]3 rows[/bold green on black] kept out of "
                "[white on black]3[/white on black], [bold orange3 on black]0 rows"
                "[/bold orange3 on black] filtered.",
            ],
            id="Filter: not equal.",
        ),
        param(
            "toto >= 10 and titi != 10 and tutu == 'Candidate'",
            [
                {"toto": 11, "titi": 12, "tutu": "Candidate"},
                {"toto": 7, "titi": 8, "tutu": "Candidate"},
                {"toto": 15, "titi": 16, "tutu": "Candidate"},
            ],
            [
                {"toto": 11, "titi": 12, "tutu": "Candidate"},
                {"toto": 15, "titi": 16, "tutu": "Candidate"},
            ],
            [
                "  [bold green on black]2 rows[/bold green on black] kept out of "
                "[white on black]3[/white on black], [bold orange3 on black]1 rows"
                "[/bold orange3 on black] filtered.",
            ],
            id="Filter: mixed.",
        ),
        param(
            None,
            [
                {"toto": 11, "titi": 12, "tutu": "Candidate"},
                {"toto": 7, "titi": 8, "tutu": "Candidate"},
                {"toto": 15, "titi": 16, "tutu": "Candidate"},
            ],
            [
                {"toto": 11, "titi": 12, "tutu": "Candidate"},
                {"toto": 7, "titi": 8, "tutu": "Candidate"},
                {"toto": 15, "titi": 16, "tutu": "Candidate"},
            ],
            [
                "  [bold orange3 on black]No filter, kept all rows."
                "[/bold orange3 on black]"
            ],
            id="No filter.",
        ),
    ],
)
def test_filter_without_csv_out(
    filter,
    content,
    expected_res,
    expected_msg,
):
    warning_msg = []
    result = input_filter(filter, content, warning_msg, False)

    with soft_assertions():
        assert_that(result).is_equal_to(expected_res)
        assert_that(warning_msg).is_equal_to(expected_msg)


def test_filter_python_syntax_error():
    with pytest.raises(PythonSyntaxError):
        input_filter(
            'toto if "BOOK" elif toto',
            [{"toto": 11, "titi": 12, "tutu": "Candidate"}],
            [],
            False,
        )


def test_filter_real_condition(filtered_expected: CsvDictData):
    mapping = read_yaml(Path(f"{TEST_PATH}/mapping_files/mapping_tess.yml"))
    csv_content = read_csv(Path(f"{TEST_PATH}/csv_files/tess.csv"))
    formatted_csv = format_csv(csv_content)
    filter_str = mapping.get("filter")

    # filtered_expected return a list cf. conftest.py
    assert input_filter(filter_str, formatted_csv, [], False) == filtered_expected
