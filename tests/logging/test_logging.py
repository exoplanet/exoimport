# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from exoimport import (
    LoggerAlreadyConfiguredError,
    LoggerAlreadyUsedError,
    config_logger,
    get_logger,
    reset_config,
)


@pytest.mark.parametrize(
    "exist",
    [
        param(True, id="cwd/logs exist"),
        param(False, id="cwd/logs doesn't exist"),
    ],
)
@pytest.mark.disable_autouse
def test_basic_log(exist, tmp_path):
    reset_config()
    path = tmp_path / "logs_exist"
    path.mkdir()
    logs_path = path / "logs"

    if exist:
        logs_path.mkdir()

    get_logger.cache_clear()
    config_logger(advanced=False, cwd=path)

    exolog = get_logger()
    exolog.info("info check")
    exolog.warning("warning check")
    exolog.error("error check")
    exolog.critical("critical check")

    log_file = logs_path / "exoimport.log"

    content = log_file.read_text()

    line_content = content.split("\n")

    with soft_assertions():
        assert_that(line_content[0]).contains('INFO:"New import"')
        assert_that(line_content[1]).contains('INFO:"info check"')
        assert_that(line_content[2]).contains('WARNING:"warning check"')
        assert_that(line_content[3]).contains('ERROR:"error check"')
        assert_that(line_content[4]).contains('CRITICAL:"critical check"')


@pytest.mark.parametrize(
    "exist",
    [
        param(True, id="cwd/logs exist"),
        param(False, id="cwd/logs doesn't exist"),
    ],
)
@pytest.mark.disable_autouse
def test_advanced_log(exist, tmp_path):
    reset_config()
    path = tmp_path / "logs_exist"
    path.mkdir()
    logs_path = path / "logs"

    if not exist:
        logs_path.mkdir()

    get_logger.cache_clear()
    config_logger(advanced=True, cwd=path)

    exolog = get_logger()
    exolog.info("info check")
    exolog.warning("warning check")
    exolog.error("error check")
    exolog.critical("critical check")

    info_log_file = logs_path / "info.exoimport.log"
    warning_log_file = logs_path / "warning.exoimport.log"
    error_log_file = logs_path / "error.exoimport.log"
    critical_log_file = logs_path / "critical.exoimport.log"

    info_content = info_log_file.read_text().split("\n")
    warning_content = warning_log_file.read_text().split("\n")
    error_content = error_log_file.read_text().split("\n")
    critical_content = critical_log_file.read_text().split("\n")

    with soft_assertions():
        assert_that(info_content[0]).contains('INFO:"New import"')
        assert_that(info_content[1]).contains('INFO:"info check"')
        assert_that(warning_content[0]).contains('INFO:"New import"')
        assert_that(warning_content[1]).contains('WARNING:"warning check"')
        assert_that(error_content[0]).contains('INFO:"New import"')
        assert_that(error_content[1]).contains('ERROR:"error check"')
        assert_that(critical_content[0]).contains('INFO:"New import"')
        assert_that(critical_content[1]).contains('CRITICAL:"critical check"')


def test_exception_logging_config_alreay_configure():
    reset_config()
    config_logger(advanced=False, cwd=None)
    with pytest.raises(LoggerAlreadyConfiguredError):
        config_logger(advanced=False, cwd=None)


def test_exception_logging_config_before_get_logger():
    reset_config()
    get_logger.cache_clear()
    get_logger()
    with pytest.raises(LoggerAlreadyUsedError):
        config_logger(advanced=False, cwd=None)
