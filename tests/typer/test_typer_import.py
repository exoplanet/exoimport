# Pytest imports
import pytest
from pytest import param

# Standard imports
from json import load as json_load
from os import getenv

# Third party imports
from assertpy import assert_that, soft_assertions
from typer.testing import CliRunner

# First party imports
from cli_app import APP
from exoimport import get_logger, reset_config

RUNNER = CliRunner()
TEST_PATH_CSV = "tests/input_files/csv_files"
TEST_PATH_YAML = "tests/input_files/mapping_files"


@pytest.mark.parametrize(
    "input_cmd_args, input_input, exit_code_expected",
    [
        param(
            [
                "import",
                f"{TEST_PATH_CSV}/csv_exoplanet_5_obj.csv",
                f"{TEST_PATH_YAML}/mapping_tess.yml",
                f"{TEST_PATH_CSV}/csv_exoplanet_5_obj_imp.csv",
            ],
            "y",
            0,
            id="simple usage",
        ),
        param(
            [
                "import",
                f"{TEST_PATH_CSV}/csv_exoplanet_5_obj.csv",
                f"{TEST_PATH_YAML}/mapping_tess.yml",
                f"{TEST_PATH_CSV}/csv_exoplanet_5_obj_imp.csv",
            ],
            "n",
            1,
            id="simple usage with input 'N' so Abort",
        ),
        param(
            [
                "import",
                f"{TEST_PATH_CSV}/csv_exoplanet_5_obj.csv",
                f"{TEST_PATH_YAML}/mapping_tess.yml",
                f"{TEST_PATH_CSV}/csv_exoplanet_5_obj_imp.csv",
                "--no-show-summary",
            ],
            "y",
            0,
            id="with display",
        ),
        param(
            [
                "import",
                f"{TEST_PATH_CSV}/csv_exoplanet_5_obj.csv",
                f"{TEST_PATH_YAML}/mapping_tess.yml",
                f"{TEST_PATH_CSV}/csv_exoplanet_5_obj_imp.csv",
                "--config-file=pyproject.toml",
            ],
            "Y",
            0,
            id="with config file: --config-file",
        ),
        param(
            [
                "import",
                f"{TEST_PATH_CSV}/csv_exoplanet_5_obj.csv",
                f"{TEST_PATH_YAML}/mapping_tess.yml",
                f"{TEST_PATH_CSV}/csv_exoplanet_5_obj_imp.csv",
                "--epsilon=1",
            ],
            "Y",
            0,
            id="with config epsilon: --epsilon",
        ),
        param(
            [
                "import",
                f"{TEST_PATH_CSV}/csv_exoplanet_5_obj.csv",
                f"{TEST_PATH_YAML}/mapping_tess.yml",
                f"{TEST_PATH_CSV}/csv_exoplanet_5_obj_imp.csv",
                "--web-statuses=active",
            ],
            "Y",
            0,
            id="with config web statuses: --web-statuses",
        ),
        param(
            [
                "import",
                f"{TEST_PATH_CSV}/csv_exoplanet_5_obj.csv",
                f"{TEST_PATH_YAML}/mapping_tess.yml",
                f"{TEST_PATH_CSV}/csv_exoplanet_5_obj_imp.csv",
                "--epsilon=1",
                "--web-statuses=active",
                "--config-file=pyproject.toml",
            ],
            "Y",
            0,
            id="with all config: --web-statuses, --epsilon, --config-file",
        ),
    ],
)
def test_typer_import(
    input_cmd_args,
    input_input,
    exit_code_expected,
    db_for_typer,
):
    reset_config()
    get_logger.cache_clear()

    result = RUNNER.invoke(
        APP,
        input_cmd_args,
        input=input_input,
        env=dict(EXOIMPORT_DBPORT=getenv("EXOIMPORT_DBPORT")),
    )

    assert result.exit_code == exit_code_expected


def test_typer_import_json_output(db_for_typer, tmp_path):
    reset_config()
    get_logger.cache_clear()

    result = RUNNER.invoke(
        APP,
        [
            "import",
            f"{TEST_PATH_CSV}/csv_exoplanet_5_obj.csv",
            f"{TEST_PATH_YAML}/mapping_tess.yml",
            f"{TEST_PATH_CSV}/csv_exoplanet_5_obj_imp.csv",
            f"--json-output-path={tmp_path / 'exoimport.json'}",
        ],
        input="y",
        env=dict(EXOIMPORT_DBPORT=getenv("EXOIMPORT_DBPORT")),
    )

    with open(tmp_path / "exoimport.json") as json_file:
        check = json_load(json_file)

    with soft_assertions():
        assert_that(result.exit_code).is_equal_to(0)
        assert_that(check).contains_key(
            "core_import",
            "core_star",
            "core_alternatestarname",
            "core_planet",
            "core_alternateplanetname",
            "core_publication",
            "core_star2publication",
            "core_planet2publication",
        )
        assert_that(check["core_import"][0]).contains_key("id")
        assert_that(check["core_star"][0]).contains_key("id")
        assert_that(check["core_planet"][0]).contains_key("id")
        assert_that(check["core_publication"][0]).contains_key("id")
        assert_that(check["core_star2publication"][0]).contains_key("id")
        assert_that(check["core_planet2publication"][0]).contains_key("id")
        assert_that(check["core_import"][0]).contains_entry(
            {"external_csv": "<file of size 3329 bytes>"},
        )


def test_typer_import_json_output_alternate_planet_name(db_for_typer, tmp_path):
    reset_config()
    get_logger.cache_clear()

    result = RUNNER.invoke(
        APP,
        [
            "import",
            f"{TEST_PATH_CSV}/csv_exoplanet_5_obj.csv",
            f"{TEST_PATH_YAML}/mapping_tess.yml",
            f"{TEST_PATH_CSV}/csv_exoplanet_5_obj_imp_2.csv",
            f"--json-output-path={tmp_path / 'exoimport.json'}",
        ],
        input="y",
        env=dict(EXOIMPORT_DBPORT=getenv("EXOIMPORT_DBPORT")),
    )

    with open(tmp_path / "exoimport.json") as json_file:
        check = json_load(json_file)

    with soft_assertions():
        assert_that(result.exit_code).is_equal_to(0)
        assert_that(check).contains_key(
            "core_import",
            "core_star",
            "core_alternatestarname",
            "core_planet",
            "core_alternateplanetname",
            "core_publication",
            "core_star2publication",
            "core_planet2publication",
        )
        assert_that(check["core_alternateplanetname"][0]).contains_key("id")


@pytest.mark.parametrize(
    "input_cmd_args, input_stdout",
    [
        param(
            [
                "import",
                "wrong.csv",
                f"{TEST_PATH_YAML}/mapping_tess.yml",
                f"{TEST_PATH_CSV}/csv_exoplanet_5_obj_imp.csv",
            ],
            "The file wrong.csv does not exist!",
            id="csv external file: does not exist",
        ),
        param(
            [
                "import",
                "tests/input_files.toto.md",
                f"{TEST_PATH_YAML}/mapping_tess.yml",
                f"{TEST_PATH_CSV}/csv_exoplanet_5_obj_imp.csv",
            ],
            "Enter a `.csv` file!",
            id="csv external file: not a csv file",
        ),
        param(
            [
                "import",
                f"{TEST_PATH_CSV}/csv_exoplanet_5_obj.csv",
                "wrong.yml",
                f"{TEST_PATH_CSV}/csv_exoplanet_5_obj_imp.csv",
            ],
            "The file wrong.yml does not exist!",
            id="mapping file: does not exist",
        ),
        param(
            [
                "import",
                f"{TEST_PATH_CSV}/csv_exoplanet_5_obj.csv",
                "tests/input_files.toto.md",
                f"{TEST_PATH_CSV}/csv_exoplanet_5_obj_imp.csv",
            ],
            "Enter a `.yaml` or `.yml` file!",
            id="mapping file: not a yaml file",
        ),
        param(
            [
                "import",
                f"{TEST_PATH_CSV}/tess.csv",
                f"{TEST_PATH_YAML}/mapping_tess.yml",
                "wrong.csv",
            ],
            "The file wrong.csv does not exist!",
            id="csv exoplanet file: does not exist",
        ),
        param(
            [
                "import",
                f"{TEST_PATH_CSV}/tess.csv",
                f"{TEST_PATH_YAML}/mapping_tess.yml",
                "tests/input_files.toto.md",
            ],
            "Enter a `.csv` file!",
            id="csv exoplanet file: not a csv file",
        ),
    ],
)
def test_typer_import_wrong_parameter_file(input_cmd_args, input_stdout, db_for_typer):
    reset_config()
    get_logger.cache_clear()

    result = RUNNER.invoke(
        APP,
        input_cmd_args,
        env=dict(EXOIMPORT_DBPORT=getenv("EXOIMPORT_DBPORT")),
    )
    assert result.exit_code == 2
    assert input_stdout in result.stdout


@pytest.mark.parametrize(
    "input_cmd_args",
    [
        param(
            [
                "import",
                f"{TEST_PATH_CSV}/tess.csv",
                f"{TEST_PATH_YAML}/mapping_tess.yml",
                f"{TEST_PATH_CSV}/csv_exo_invalid_url.csv",
            ],
            id="invalid_url",
        ),
        param(
            [
                "--debug",
                "import",
                f"{TEST_PATH_CSV}/tess.csv",
                f"{TEST_PATH_YAML}/mapping_tess.yml",
                f"{TEST_PATH_CSV}/csv_exo_invalid_url.csv",
            ],
            id="with_debug_invalid_url",
        ),
        param(
            [
                "import",
                f"{TEST_PATH_CSV}/tess.csv",
                f"{TEST_PATH_YAML}/mapping_tess.yml",
                f"{TEST_PATH_CSV}/empty.csv",
            ],
            id="csv exoplanet file: empty",
        ),
        param(
            [
                "--debug",
                "import",
                f"{TEST_PATH_CSV}/tess.csv",
                f"{TEST_PATH_YAML}/mapping_tess.yml",
                f"{TEST_PATH_CSV}/empty.csv",
            ],
            id="with debug csv exoplanet file: empty",
        ),
        param(
            [
                "import",
                f"{TEST_PATH_CSV}/tess.csv",
                f"{TEST_PATH_YAML}/mapping_tess.yml",
                f"{TEST_PATH_CSV}/csv_exoplanet_5_obj_imp.csv",
                "--config-file=toto.toml",
            ],
            id="config file does not exist",
        ),
        param(
            [
                "--debug",
                "import",
                f"{TEST_PATH_CSV}/tess.csv",
                f"{TEST_PATH_YAML}/mapping_tess.yml",
                f"{TEST_PATH_CSV}/csv_exoplanet_5_obj_imp.csv",
                "--config-file=toto.toml",
            ],
            id="with debug config file does not exist",
        ),
        param(
            [
                "import",
                f"{TEST_PATH_CSV}/tess.csv",
                f"{TEST_PATH_YAML}/mapping_tess.yml",
                f"{TEST_PATH_CSV}/wrong_name.csv",
            ],
            id="wrong planet name",
        ),
        param(
            [
                "--debug",
                "import",
                f"{TEST_PATH_CSV}/tess.csv",
                f"{TEST_PATH_YAML}/mapping_tess.yml",
                f"{TEST_PATH_CSV}/wrong_name.csv",
            ],
            id="with debug wrong planet name",
        ),
        param(
            [
                "import",
                f"{TEST_PATH_CSV}/tess.csv",
                f"{TEST_PATH_YAML}/mapping_tess.yml",
                f"{TEST_PATH_CSV}/wrong_star_name.csv",
            ],
            id="wrong star name",
        ),
        param(
            [
                "--debug",
                "import",
                f"{TEST_PATH_CSV}/tess.csv",
                f"{TEST_PATH_YAML}/mapping_tess.yml",
                f"{TEST_PATH_CSV}/wrong_star_name.csv",
            ],
            id="with debug wrong star name",
        ),
    ],
)
def test_typer_import_exception(input_cmd_args):
    reset_config()
    get_logger.cache_clear()

    result = RUNNER.invoke(
        APP,
        input_cmd_args,
        input="y",
        env=dict(EXOIMPORT_DBPORT=getenv("EXOIMPORT_DBPORT")),
    )
    assert result.exit_code == 1


@pytest.mark.parametrize(
    "input_cmd_args",
    [
        param(
            [
                "import",
                f"{TEST_PATH_CSV}/csv_exoplanet_5_obj.csv",
                f"{TEST_PATH_YAML}/mapping_tess.yml",
                f"{TEST_PATH_CSV}/csv_exoplanet_5_obj_imp.csv",
            ],
            id="star already in db",
        ),
        param(
            [
                "--debug",
                "import",
                f"{TEST_PATH_CSV}/csv_exoplanet_5_obj.csv",
                f"{TEST_PATH_YAML}/mapping_tess.yml",
                f"{TEST_PATH_CSV}/csv_exoplanet_5_obj_imp.csv",
            ],
            id="with debug, star already in db",
        ),
    ],
)
def test_typer_import_exception_import_error(input_cmd_args, db_for_typer_with_data):
    reset_config()
    get_logger.cache_clear()

    result = RUNNER.invoke(
        APP,
        input_cmd_args,
        input="y",
        env=dict(EXOIMPORT_DBPORT=getenv("EXOIMPORT_DBPORT")),
    )
    assert result.exit_code == 1
