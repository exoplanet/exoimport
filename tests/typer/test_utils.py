# Pytest imports
import pytest
from pytest import param

# Standard imports
from pathlib import Path

# Third party imports
from assertpy import assert_that, fail
from click import BadParameter

# First party imports
from cli_app import (
    csv_input_file_callback,
    csv_output_file_callback,
    get_content_in_csv_format,
    get_headers,
    log_and_display_summary,
    yaml_file_callback,
)
from cli_app.importdb import _get_ids_from_pub_and_link, _get_ids_from_systems
from exoimport import ImportSystemRecord

TEST_PATH = "tests/input_files"


@pytest.mark.parametrize(
    "input_dict, expected",
    [
        param(
            [{"a": "aa", "b": "bb"}, {"a": 12, "b": "titi"}],
            ["a", "b"],
            id="classic",
        ),
        param([{"a": "aa", "b": "bb"}], ["a", "b"], id="one row"),
    ],
)
def test_typer_utils_get_headers(input_dict, expected):
    assert get_headers(input_dict) == expected


@pytest.mark.parametrize(
    "input_dict, expected",
    [
        param(
            [{"a": "aa", "b": "bb"}, {"a": 12, "b": "titi"}],
            [["a", "b"], ["aa", "bb"], [12, "titi"]],
            id="classic",
        ),
        param([{"a": "aa", "b": "bb"}], [["a", "b"], ["aa", "bb"]], id="one row"),
    ],
)
def test_typer_utils_get_content_in_csv_format(input_dict, expected):
    assert get_content_in_csv_format(input_dict) == expected


def test_typer_utils_csv_input_callback_succeed():
    assert csv_input_file_callback(Path(f"{TEST_PATH}/csv_files/tess.csv")) == Path(
        f"{TEST_PATH}/csv_files/tess.csv",
    )


@pytest.mark.parametrize(
    "input_file, input_err",
    [
        param(
            Path("wrong.csv"),
            "The file wrong.csv does not exist!",
            id="file does not exist",
        ),
        param(
            Path("README.md"),
            "Enter a `.csv` file!",
            id="not csv file",
        ),
    ],
)
def test_typer_utils_csv_input_callback_fails(input_file, input_err):
    try:
        csv_input_file_callback(input_file)
        fail("Should have raised BadParameter error")
    except BadParameter as err:
        assert_that(str(err)).is_equal_to(input_err)


def test_typer_utils_csv_output_callback_succeed():
    assert csv_output_file_callback(Path("wrong.csv")) == Path("wrong.csv")


def test_typer_utils_csv_output_callback_fails():
    try:
        csv_output_file_callback(Path("toto.md"))
        fail("Should have raised BadParameter error")
    except BadParameter as err:
        assert_that(str(err)).is_equal_to("Enter a `.csv` file!")


@pytest.mark.parametrize(
    "input_file",
    [
        param(
            Path(f"{TEST_PATH}/mapping_files/empty.yml"),
            id=".yml file",
        ),
        param(
            Path(
                f"{TEST_PATH}/mapping_files/mapping_tess_candidate_september_2022.yaml"
            ),
            id=".yaml file",
        ),
    ],
)
def test_typer_utils_yaml_callback_succeed(input_file):
    assert yaml_file_callback(input_file) == input_file


@pytest.mark.parametrize(
    "input_file, input_err",
    [
        param(
            Path("toto.yml"),
            "The file toto.yml does not exist!",
            id="file '.yml' does not exist",
        ),
        param(
            Path("toto.yaml"),
            "The file toto.yaml does not exist!",
            id="file '.yaml' does not exist",
        ),
        param(
            Path("README.md"),
            "Enter a `.yaml` or `.yml` file!",
            id="not yaml or yml file",
        ),
    ],
)
def test_typer_utils_yaml_callback_fails(input_file, input_err):
    try:
        yaml_file_callback(input_file)
        fail("Should have raised BadParameter error")
    except BadParameter as err:
        assert_that(str(err)).is_equal_to(input_err)


@pytest.mark.parametrize(
    "systems, expected",
    [
        param(
            (
                1,
                [
                    ImportSystemRecord(1, [1], [], [[]]),
                    ImportSystemRecord(2, [2, 3], [], [[]]),
                ],
            ),
            ("1", [1, 2], [1, 2, 3], [], []),
            id="without alternate name",
        ),
        param(
            (
                1,
                [
                    ImportSystemRecord(1, [1], [1], [[1]]),
                    ImportSystemRecord(2, [2, 3], [2], [[2], [3]]),
                ],
            ),
            ("1", [1, 2], [1, 2, 3], [1, 2], [1, 2, 3]),
            id="without alternate name",
        ),
    ],
)
def test_typer_utils_get_ids_from_systems(systems, expected):
    assert _get_ids_from_systems(systems) == expected


def test_typer_utils_get_ids_from_pub_and_link():
    pub_and_link = (
        1,
        [
            (
                {
                    "<generic_pub>": 1,
                    "distance": 2,
                },
                [
                    {
                        "<generic_pub>": 1,
                        "mass": 2,
                    },
                ],
            ),
            (
                {
                    "<generic_pub>": 3,
                    "distance": 4,
                },
                [
                    {
                        "<generic_pub>": 3,
                        "mass": 4,
                    },
                    {
                        "<generic_pub>": 5,
                        "mass": 6,
                    },
                ],
            ),
        ],
    )
    assert _get_ids_from_pub_and_link(pub_and_link) == (
        "1",
        [1, 2, 3, 4],
        [1, 2, 3, 4, 5, 6],
    )


@pytest.mark.parametrize(
    "display, expected",
    [
        (False, ""),
        (
            True,
            "\nImported ids:\n  Import id: 1\n  Star id(s): [1,2]\n  Planet id(s): "
            "[1,2,3]\n  Star alternate name id(s): []\n  Planet alternate name id(s): "
            "[]\n  Publication id: 1\n  Star2Publication link: [1,2,3,4]\n  "
            "Planet2Publication link: [1,2,3,4,5,6]\n\n17 imported rows\n  1 imported "
            "import\n  2 imported star(s)\n  3 imported planet(s)\n  0 imported star "
            "alternate names\n  0 imported planet alternate names\n  1 imported "
            "publication\n  4 imported star2publication links\n  6 imported "
            "planet2publication links\n",
        ),
    ],
)
def test_typer_utils_log_and_display(capsys, display, expected):
    systems = (
        1,
        [
            ImportSystemRecord(1, [1], [], []),
            ImportSystemRecord(2, [2, 3], [], []),
        ],
    )
    pub_and_link = (
        1,
        [
            (
                {
                    "<generic_pub>": 1,
                    "distance": 2,
                },
                [
                    {
                        "<generic_pub>": 1,
                        "mass": 2,
                    },
                ],
            ),
            (
                {
                    "<generic_pub>": 3,
                    "distance": 4,
                },
                [
                    {
                        "<generic_pub>": 3,
                        "mass": 4,
                    },
                    {
                        "<generic_pub>": 5,
                        "mass": 6,
                    },
                ],
            ),
        ],
    )
    log_and_display_summary(systems, pub_and_link, display)
    captured = capsys.readouterr()
    assert captured.out == expected
