# Pytest imports
import pytest
from pytest import param

# Standard imports
from pathlib import Path

# Third party imports
import typer
from art import text2art
from assertpy import assert_that, soft_assertions
from typer.testing import CliRunner

# First party imports
from cli_app import APP, do_validation
from exoimport import get_logger, reset_config

RUNNER = CliRunner()
CWD = Path.cwd()
TEST_PATH_CSV = "tests/input_files/csv_files"
TEST_PATH_YAML = "tests/input_files/mapping_files"


def test_typer_translate_do_validation_succeed(capfd, define_new_cwd):
    warning_msg = []

    do_validation(
        [["a", "b"], ["a", "b"], ["aa", "bb"], ["aaa", "bbb"]],
        {"toto": "a", "titi": "b"},
        warning_msg,
    )

    out, err = capfd.readouterr()
    expected_validation_msg = "\n  Valid csv? True. Valid mapping? True.\n\n\n"
    expected_bar = (
        "\nValidating headers... ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 100% 0:00:00"
        "\nValidating mapping... ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 100% "
        "0:00:00\n"
    )
    expected_out = f"{text2art('Validation')}{expected_bar}{expected_validation_msg}"

    with soft_assertions():
        assert_that(warning_msg).is_equal_to([])
        assert_that(out).is_equal_to(expected_out)


@pytest.mark.parametrize(
    "csv_content, mapping_content, exp_validation_msg, expected_warning_msg, "
    "exp_warning_msg_less_rich",
    [
        param(
            [["a-b", "b"], ["a", "b"], ["aa", "bb"], ["aaa", "bbb"]],
            {"toto": "a-b", "titi": "b"},
            "\n  Valid csv? False. Valid mapping? False.\n",
            [
                "  [bold orange3 on black] CSV errors: [/bold orange3 on black]",
                "  1 header(s) are invalid headers.",
                '\t-The header "a-b" is not a valid header.',
                "\n",
                "  [bold orange3 on black] Mapping Yaml errors: "
                "[/bold orange3 on black]",
                "  1 formula(s) are invalid.",
                '\t-"toto: a-b" is not a valid formula.',
                "\n",
            ],
            "   CSV errors: \n  1 header(s) are invalid headers.\n"
            '        -The header "a-b" is not a valid header.\n\n\n'
            "   Mapping Yaml errors: \n  1 formula(s) are invalid.\n"
            '        -"toto: a-b" is not a valid formula.\n\n',
            id="csv is invalid",
        ),
        param(
            [["a", "b"], ["a", "b"], ["aa", "bb"], ["aaa", "bbb"]],
            {"toto": "tutu", "titi": "b"},
            "\n  Valid csv? True. Valid mapping? False.\n",
            [
                "  [bold orange3 on black] Mapping Yaml errors: "
                "[/bold orange3 on black]",
                "  1 formula(s) are invalid.",
                '\t-"toto: tutu" is not a valid formula.',
                "\n",
            ],
            "   Mapping Yaml errors: \n  1 formula(s) are invalid.\n"
            '        -"toto: tutu" is not a valid formula.\n\n',
            id="yaml is invalid",
        ),
        param(
            [["a-b", "b"], ["a", "b"], ["aa", "bb"], ["aaa", "bbb"]],
            {"toto": "tutu", "titi": "b"},
            "\n  Valid csv? False. Valid mapping? False.\n",
            [
                "  [bold orange3 on black] CSV errors: [/bold orange3 on black]",
                "  1 header(s) are invalid headers.",
                '\t-The header "a-b" is not a valid header.',
                "\n",
                "  [bold orange3 on black] Mapping Yaml errors: "
                "[/bold orange3 on black]",
                "  1 formula(s) are invalid.",
                '\t-"toto: tutu" is not a valid formula.',
                "\n",
            ],
            "   CSV errors: \n  1 header(s) are invalid headers.\n"
            '        -The header "a-b" is not a valid header.\n\n\n'
            "   Mapping Yaml errors: \n  1 formula(s) are invalid.\n"
            '        -"toto: tutu" is not a valid formula.\n\n',
            id="All is invalid",
        ),
    ],
)
def test_typer_translate_do_validation_fails(
    csv_content,
    mapping_content,
    exp_validation_msg,
    expected_warning_msg,
    exp_warning_msg_less_rich,
    capfd,
    define_new_cwd,
):
    warning_msg = []

    with pytest.raises(typer.Exit):
        do_validation(csv_content, mapping_content, warning_msg)

    out, err = capfd.readouterr()
    txt2art = text2art("Validation")
    expected_bar = (
        "\nValidating headers... ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 100% 0:00:00"
        "\nValidating mapping... ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 100% "
        "0:00:00\n"
    )
    expected_out = (
        f"{txt2art}{expected_bar}\n\n{exp_warning_msg_less_rich}{exp_validation_msg}"
    )

    with soft_assertions():
        assert_that(warning_msg).is_equal_to(expected_warning_msg)
        assert_that(out).is_equal_to(expected_out)


@pytest.mark.parametrize(
    "input_cmd_args, input_path, input_input",
    [
        # ONE step
        param(
            [
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                f"{CWD / TEST_PATH_YAML}/mapping_tess.yml",
                "--no-filter",
                "--no-translate",
            ],
            {"filtered": False, "kept": False, "translate": False},
            None,
            id="just validate",
        ),
        param(
            [
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                f"{CWD / TEST_PATH_YAML}/mapping_tess.yml",
                "--no-validate",
                "--no-translate",
            ],
            {"filtered": False, "kept": False, "translate": False},
            "n\nn\n",
            id="just filter, without filtered, without kept",
        ),
        param(
            [
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                f"{CWD / TEST_PATH_YAML}/mapping_tess.yml",
                "--no-validate",
                "--no-translate",
            ],
            {"filtered": False, "kept": False, "translate": False},
            "y\nn\n",
            id="just filter, with filtered (without path), without kept",
        ),
        param(
            [
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                f"{CWD / TEST_PATH_YAML}/mapping_tess.yml",
                "--no-validate",
                "--no-translate",
            ],
            {"filtered": True, "kept": False, "translate": False},
            "y\nn\n",
            id="just filter, with filtered (with path), without kept",
        ),
        param(
            [
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                f"{CWD / TEST_PATH_YAML}/mapping_tess.yml",
                "--no-validate",
                "--no-translate",
            ],
            {"filtered": False, "kept": False, "translate": False},
            "n\ny\n",
            id="just filter, without filtered , with kept (without path)",
        ),
        param(
            [
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                f"{CWD / TEST_PATH_YAML}/mapping_tess.yml",
                "--no-validate",
                "--no-translate",
            ],
            {"filtered": False, "kept": True, "translate": False},
            "n\ny\n",
            id="just filter, without filtered , with kept (with path)",
        ),
        param(
            [
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                f"{CWD / TEST_PATH_YAML}/mapping_tess.yml",
                "--no-validate",
                "--no-translate",
            ],
            {"filtered": False, "kept": False, "translate": False},
            "y\ny\n",
            id="just filter, with filtered (without path), with kept (without path)",
        ),
        param(
            [
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                f"{CWD / TEST_PATH_YAML}/mapping_tess.yml",
                "--no-validate",
                "--no-translate",
            ],
            {"filtered": True, "kept": False, "translate": False},
            "y\ny\n",
            id="just filter, with filtered (with path), with kept (without path)",
        ),
        param(
            [
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                f"{CWD / TEST_PATH_YAML}/mapping_tess.yml",
                "--no-validate",
                "--no-translate",
            ],
            {"filtered": False, "kept": True, "translate": False},
            "y\ny\n",
            id="just filter, with filtered (without path), with kept (with path)",
        ),
        param(
            [
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                f"{CWD / TEST_PATH_YAML}/mapping_tess.yml",
                "--no-validate",
                "--no-translate",
            ],
            {"filtered": True, "kept": True, "translate": False},
            "y\ny\n",
            id="just filter, with filtered (with path), with kept (with path)",
        ),
        param(
            [
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                f"{CWD / TEST_PATH_YAML}/mapping_tess.yml",
                "--no-filter",
                "--no-validate",
            ],
            {"filtered": False, "kept": False, "translate": False},
            None,
            id="just translate, without path",
        ),
        param(
            [
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                f"{CWD / TEST_PATH_YAML}/mapping_tess.yml",
                "--no-filter",
                "--no-validate",
            ],
            {"filtered": False, "kept": False, "translate": True},
            None,
            id="just translate, with path",
        ),
        # TWO steps
        param(
            [
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                f"{CWD / TEST_PATH_YAML}/mapping_tess.yml",
                "--no-translate",
            ],
            {"filtered": False, "kept": False, "translate": False},
            "n\nn\n",
            id="validate and filter",
        ),
        param(
            [
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                f"{CWD / TEST_PATH_YAML}/mapping_tess.yml",
                "--no-filter",
            ],
            {"filtered": False, "kept": False, "translate": False},
            None,
            id="validate and translate",
        ),
        param(
            [
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                f"{CWD / TEST_PATH_YAML}/mapping_tess.yml",
                "--no-validate",
            ],
            {"filtered": False, "kept": False, "translate": False},
            "n\nn\n",
            id="filter and translate",
        ),
        # ALL steps
        param(
            [
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                f"{CWD / TEST_PATH_YAML}/mapping_tess.yml",
            ],
            {"filtered": False, "kept": False, "translate": False},
            "n\nn\n",
            id="filter and validate and translate",
        ),
    ],
)
def test_typer_translate(
    input_cmd_args,
    input_path,
    input_input,
    tmp_path,
    define_new_cwd,
):
    reset_config()
    get_logger.cache_clear()

    if any(input_path):
        dir_path = tmp_path / "test_typer_translate"
        dir_path.mkdir()

    if input_path["filtered"]:
        input_cmd_args.append(f"--filtered-path={dir_path}/filtered.csv")
    if input_path["kept"]:
        input_cmd_args.append(f"--kept-path={dir_path}/i_will_keep.csv")
    if input_path["translate"]:
        input_cmd_args.append(f"--translate-path={dir_path}/toto.csv")

    result = RUNNER.invoke(APP, input_cmd_args, input=input_input)
    assert result.exit_code == 0


@pytest.mark.parametrize(
    "input_cmd_args, input_stdout",
    [
        param(
            [
                "translate",
                "wrong.csv",
                f"{CWD / TEST_PATH_YAML}/mapping_tess.yml",
            ],
            "The file wrong.csv does not exist!",
            id="csv file: does not exist",
        ),
        param(
            [
                "translate",
                "tests/input_files.toto.md",
                f"{CWD / TEST_PATH_YAML}/mapping_tess.yml",
            ],
            "Enter a `.csv` file!",
            id="csv file: not a csv file",
        ),
        param(
            [
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                "wrong.yml",
            ],
            "The file wrong.yml does not exist!",
            id="mapping file: does not exist",
        ),
        param(
            [
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                "tests/input_files.toto.md",
            ],
            "Enter a `.yaml` or `.yml` file!",
            id="mapping file: not a yaml file",
        ),
        param(
            [
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                f"{CWD / TEST_PATH_YAML}/mapping_tess.yml",
                "--filtered-path=wrong.md",
            ],
            "Enter a `.csv` file!",
            id="filtered path: not a csv file",
        ),
        param(
            [
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                f"{CWD / TEST_PATH_YAML}/mapping_tess.yml",
                "--kept-path=wrong.md",
            ],
            "Enter a `.csv` file!",
            id="kept path: not a csv file",
        ),
        param(
            [
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                f"{CWD / TEST_PATH_YAML}/mapping_tess.yml",
                "--translate-path=wrong.md",
            ],
            "Enter a `.csv` file!",
            id="translate path: not a csv file",
        ),
    ],
)
def test_typer_translate_wrong_parameter_file(input_cmd_args, input_stdout):
    reset_config()
    get_logger.cache_clear()

    result = RUNNER.invoke(APP, input_cmd_args)
    assert result.exit_code == 2
    assert input_stdout in result.stdout


@pytest.mark.parametrize(
    "input_cmd_args, input_stdout",
    [
        param(
            [
                "translate",
                f"{CWD / TEST_PATH_CSV}/empty.csv",
                f"{CWD / TEST_PATH_YAML}/mapping_tess.yml",
            ],
            "EmptyFileError",
            id="empty csv file no debug",
        ),
        param(
            [
                "--debug",
                "translate",
                f"{CWD / TEST_PATH_CSV}/empty.csv",
                f"{CWD / TEST_PATH_YAML}/mapping_tess.yml",
            ],
            "",
            id="empty csv file with debug",
        ),
        param(
            [
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                f"{CWD / TEST_PATH_YAML}/empty.yml",
            ],
            "EmptyFileError",
            id="empty yaml file no debug",
        ),
        param(
            [
                "--debug",
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                f"{CWD / TEST_PATH_YAML}/empty.yml",
            ],
            "",
            id="empty yaml file with debug",
        ),
        param(
            [
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                f"{CWD / TEST_PATH_YAML}/type_error.yml",
            ],
            "TypeError: bad operand type for unary -: 'str'",
            id="type error in mapping yaml file in validate no debug",
        ),
        param(
            [
                "--debug",
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                f"{CWD / TEST_PATH_YAML}/type_error.yml",
            ],
            "",
            id="type error in mapping yaml file in validate with debug",
        ),
        param(
            [
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                f"{CWD / TEST_PATH_YAML}/type_error.yml",
                "--no-validate",
                "--no-filter",
            ],
            "TypeError: bad operand type for unary -: 'str'",
            id="type error in translate no debug",
        ),
        param(
            [
                "--debug",
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                f"{CWD / TEST_PATH_YAML}/type_error.yml",
                "--no-validate",
                "--no-filter",
            ],
            "",
            id="type error in translate with debug",
        ),
        param(
            [
                "translate",
                f"{CWD / TEST_PATH_CSV}/tess.csv",
                f"{CWD / TEST_PATH_YAML}/invalid_yaml.yml",
            ],
            "InvalidYamlError:",
            id="invalid yaml mapping file",
        ),
    ],
)
def test_typer_translate_empty_file(input_cmd_args, input_stdout):
    reset_config()
    get_logger.cache_clear()

    result = RUNNER.invoke(APP, input_cmd_args)
    assert result.exit_code == 1
    assert input_stdout in result.stdout
