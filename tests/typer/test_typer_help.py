# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that, soft_assertions
from typer.testing import CliRunner

# First party imports
from cli_app import APP
from exoimport import reset_config

RUNNER = CliRunner()


@pytest.mark.parametrize(
    "input_cmd_args, input_stdout, input_exit_code",
    [
        param(
            ["help"],
            "\nUsage: exo-import [--version] [--debug] [--advanced-log] "
            "[----logging-cwd-path] [--help] COMMAND [<args>]",
            0,
            id="simple help",
        ),
        param(
            ["help", "help"],
            "Get documentation on commands and some tutorials",
            0,
            id="help: help",
        ),
        param(
            ["help", "import"],
            "Import a csv normalized file in exoplanet.eu",
            0,
            id="help: import",
        ),
        param(
            ["help", "translate"],
            "Translate a csv into a normalized csv for exoplanet.eu with a mapping",
            0,
            id="help: translate",
        ),
        param(
            ["help", "headers"],
            "How the headers of a csv should be written",
            0,
            id="help: headers",
        ),
        param(
            ["help", "mapping"],
            "How to write a mapping file",
            0,
            id="help: mapping",
        ),
        param(
            ["help", "import"],
            "Import a csv normalized file in exoplanet.eu",
            0,
            id="help: import",
        ),
        param(
            ["help", "config"],
            "How the config file should be written",
            0,
            id="help: config",
        ),
        param(
            ["help", "epsilon"],
            "What is epsilon?",
            0,
            id="help: epsilon",
        ),
        param(
            ["help", "web_statuses"],
            "What is web statuses?",
            0,
            id="help: web statuses",
        ),
        param(
            ["help", "toto"],
            " Warning  ⚠ The argument isn't valid, try with those arguments",
            1,
            id="help: unknown args",
        ),
        param(
            ["--debug", "help", "toto"],
            " Warning  ⚠ The argument isn't valid, try with those arguments",
            1,
            id="help: unkmown args with debug",
        ),
    ],
)
def test_typer_help(input_cmd_args, input_stdout, input_exit_code):
    reset_config()
    result = RUNNER.invoke(APP, input_cmd_args)
    with soft_assertions():
        assert_that(result.exit_code).is_equal_to(input_exit_code)
        assert_that(result.stdout).contains(input_stdout)
