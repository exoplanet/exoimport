# Pytest imports
import pytest
from pytest import param

# Standard imports
from os import getenv

# Third party imports
from assertpy import assert_that, soft_assertions
from typer.testing import CliRunner

# First party imports
from cli_app import APP
from exoimport import get_logger, reset_config

RUNNER = CliRunner()


@pytest.mark.parametrize(
    "cmd_args, input_input",
    [
        param(["generate-config"], "toml\n1\nactive", id="first time toml"),
        param(["generate-config"], "yaml\n1\nactive", id="first time yaml"),
        param(
            ["generate-config"],
            "toto\ntoml\n1\nactive",
            id="wrong extension before toml",
        ),
        param(
            ["generate-config"],
            "toto\nyaml\n1\nactive",
            id="wrong extension before yaml",
        ),
        param(
            ["generate-config"],
            "toml\ntoto\n1\nactive",
            id="wrong epsilon type before good type",
        ),
        param(
            ["generate-config"],
            "toml\n1\ntoto\nactive",
            id="wrong web statuses value before good value",
        ),
        param(
            ["generate-config", "--extension=toml"],
            "1\nactive",
            id="extension give in command line: toml",
        ),
        param(
            ["generate-config", "--extension=yaml"],
            "1\nactive",
            id="extension give in command line: yaml",
        ),
    ],
)
def test_typer_generate_config(cmd_args, input_input, define_new_cwd):
    reset_config()
    get_logger.cache_clear()

    result = RUNNER.invoke(
        APP,
        cmd_args,
        input=input_input,
        env=dict(EXOIMPORT_DBPORT=getenv("EXOIMPORT_DBPORT")),
    )
    assert result.exit_code == 0


@pytest.mark.parametrize(
    "input_input, expected_contains",
    [
        param("help\n1\nactive", "What is epsilon?", id="help on epsilon"),
        param("1\nhelp\nactive", "What is web statuses?", id="help on web statuses"),
    ],
)
def test_type_generate_config_help(input_input, expected_contains, define_new_cwd):
    reset_config()
    get_logger.cache_clear()

    result = RUNNER.invoke(
        APP,
        ["generate-config", "--extension=toml"],
        input=input_input,
        env=dict(EXOIMPORT_DBPORT=getenv("EXOIMPORT_DBPORT")),
    )

    with soft_assertions():
        assert_that(result.exit_code).is_equal_to(0)
        assert_that(result.stdout).contains(expected_contains)
