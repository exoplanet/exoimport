# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that, soft_assertions
from typer.testing import CliRunner

# First party imports
from cli_app import APP
from exoimport import reset_config

RUNNER = CliRunner()


@pytest.mark.parametrize(
    "input_cmd_args, input_stdout, input_exit_code",
    [
        param(
            ["sample-mapping"],
            "# Default mapping file for ExoImport",
            0,
            id="simple sample",
        ),
        param(
            ["sample-mapping", "--output-file", "sample_mapping.yaml"],
            "File sample_mapping.yaml was succesly written.",
            0,
            id="sample: with output --output-file",
        ),
        param(
            ["sample-mapping", "-o", "sample_mapping.yaml"],
            "File sample_mapping.yaml was succesly written.",
            0,
            id="sample: with output -o",
        ),
        param(
            ["sample-mapping", "-o", "toto"],
            "BadParameter: Enter a `.yaml` or `.yml` file!",
            1,
            id="sample: not yaml file",
        ),
        param(
            ["--debug", "sample-mapping", "-o", "toto"],
            "Invalid value: Enter a `.yaml` or `.yml` file!",
            2,
            id="sample: not yaml file with --debug",
        ),
    ],
)
def test_typer_help(input_cmd_args, input_stdout, input_exit_code, define_new_cwd):
    reset_config()
    result = RUNNER.invoke(APP, input_cmd_args)
    with soft_assertions():
        assert_that(result.exit_code).is_equal_to(input_exit_code)
        assert_that(result.stdout).contains(input_stdout)
