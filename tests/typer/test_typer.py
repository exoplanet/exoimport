# Pytest imports
import pytest

# Third party imports
import typer
from typer.testing import CliRunner

# First party imports
from cli_app import APP, version_callback
from exoimport import __app_name__ as app_name
from exoimport import __version__ as version

RUNNER = CliRunner()


def test_typer_version_with_version(capfd):
    with pytest.raises(typer.Exit):
        version_callback(True)
    out, err = capfd.readouterr()
    assert out == f"{app_name} v.{version}\n"


def test_typer_version_without_version():
    assert version_callback(False) is None


def test_app_version():
    result = RUNNER.invoke(APP, ["--version"])
    assert result.exit_code == 0
    assert f"{app_name} v.{version}" in result.stdout
