# Mapping file for ExoImport

# The field filter is optional and allow you to write a filter for the csv file.
# This filter must be write with a python syntax.
# Examples:
#     filter: status == "Confirmed"
#     filter: mass <= 60 and status == "Confirmed"

# You need to fill fields with a formula based on the csv headers you want translate.
# This must respect the python syntax.
# Examples:
#     temp_meas_Fah: pl_temp_deg
#     negative_temp_meas: -pl_temp_deg
#     temp_meas_Cel: (pl_temp_deg - 32) * 5/9

# One function is allow in addition in mapping files: val().

# val(value: str) -> str:
#   return value

# `val()` allow you to fix a value for all rows.
#     status: val("Confirmed")

# ----------
# | Filter |
# ----------

filter:

# ---------------------
# | Planet parameters |
# ---------------------

# SQL Type: VARCHAR(100)
name:

# SQL Type: VARCHAR
# Possible values: Confirmed, Candidate, Controversial, Unconfirmed, Retracted
planet_status:

# SQL Type: VARCHAR(1)
# Possible values: W, C, S, R
announced_status:

# SQL Type: VARCHAR
# Unit: MJup
mass:

# SQL Type: VARCHAR
# Unit: MJup
mass_error_min:

# SQL Type: VARCHAR
# Unit: MJup
mass_error_max:

# SQL Type: VARCHAR
# Unit: MJup
mass_sini:

# SQL Type: VARCHAR
# Unit: MJup
mass_sini_error_min:

# SQL Type: VARCHAR
# Unit: MJup
mass_sini_error_max:

# SQL Type: VARCHAR
# Unit: RJup
radius:

# SQL Type: VARCHAR
# Unit: RJup
radius_error_min:

# SQL Type: VARCHAR
# Unit: RJup
radius_error_max:

# SQL Type: DOUBLE PRECISION
# Unit: Day
orbital_period:

# SQL Type: DOUBLE PRECISION
# Unit: Day
orbital_period_error_min:

# SQL Type: DOUBLE PRECISION
# Unit: Day
orbital_period_error_max:

# SQL Type: DOUBLE PRECISION
# Unit: AU
semi_major_axis:

# SQL Type: DOUBLE PRECISION
# Unit: AU
semi_major_axis_error_min:

# SQL Type: DOUBLE PRECISION
# Unit: AU
semi_major_axis_error_max:

# SQL Type: DOUBLE PRECISION
# Unit: Pure Number
eccentricity:

# SQL Type: DOUBLE PRECISION
# Unit: Pure Number
eccentricity_error_min:

# SQL Type: DOUBLE PRECISION
# Unit: Pure Number
eccentricity_error_max:

# SQL Type: DOUBLE PRECISION
# Unit: Deg
inclination:

# SQL Type: DOUBLE PRECISION
# Unit: Deg
inclination_error_min:

# SQL Type: DOUBLE PRECISION
# Unit: Deg
inclination_error_max:

# SQL Type: DOUBLE PRECISION
# Unit: Arcsec
angular_distance:

# SQL Type: SMALL INT
# Unit: Pure Number
discovered:

# SQL Type: DOUBLE PRECISION
# Unit: Deg
omega:

# SQL Type: DOUBLE PRECISION
# Unit: Deg
omega_error_min:

# SQL Type: DOUBLE PRECISION
# Unit: Deg
omega_error_max:

# SQL Type: DOUBLE PRECISION
# Unit: JD
tperi:

# SQL Type: DOUBLE PRECISION
# Unit: JD
tperi_error_min:

# SQL Type: DOUBLE PRECISION
# Unit: JD
tperi_error_max:

# SQL Type: DOUBLE PRECISION
# Unit: JD
tconj:

# SQL Type: DOUBLE PRECISION
# Unit: JD
tconj_error_min:

# SQL Type: DOUBLE PRECISION
# Unit: JD
tconj_error_max:

# SQL Type: DOUBLE PRECISION
# Unit: JD
tzero_tr:

# SQL Type: DOUBLE PRECISION
# Unit: JD
tzero_tr_error_min:

# SQL Type: DOUBLE PRECISION
# Unit: JD
tzero_tr_error_max:

# SQL Type: DOUBLE PRECISION
# Unit: JD
tzero_tr_sec:

# SQL Type: DOUBLE PRECISION
# Unit: JD
tzero_tr_sec_error_min:

# SQL Type: DOUBLE PRECISION
# Unit: JD
tzero_tr_sec_error_max:

# SQL Type: DOUBLE PRECISION
# Unit: Deg
lambda_angle:

# SQL Type: DOUBLE PRECISION
# Unit: Deg
lambda_angle_error_min:

# SQL Type: DOUBLE PRECISION
# Unit: Deg
lambda_angle_error_max:

# SQL Type: DOUBLE PRECISION
# Unit: % (percent)
impact_parameter:

# SQL Type: DOUBLE PRECISION
# Unit: % (percent)
impact_parameter_error_min:

# SQL Type: DOUBLE PRECISION
# Unit: % (percent)
impact_parameter_error_max:

# SQL Type: DOUBLE PRECISION
# Unit: JD
tzero_vr:

# SQL Type: DOUBLE PRECISION
# Unit: JD
tzero_vr_error_min:

# SQL Type: DOUBLE PRECISION
# Unit: JD
tzero_vr_error_max:

# SQL Type: DOUBLE PRECISION
# Unit: (m/s)
k:

# SQL Type: DOUBLE PRECISION
# Unit: (m/s)
k_error_min:

# SQL Type: DOUBLE PRECISION
# Unit: (m/s)
k_error_max:

# SQL Type: DOUBLE PRECISION
# Unit: K
temp_calculated:

# SQL Type: DOUBLE PRECISION
# Unit: K
temp_calculated_error_min:

# SQL Type: DOUBLE PRECISION
# Unit: K
temp_calculated_error_max:

# SQL Type: DOUBLE PRECISION
# Unit: K
temp_measured:

# SQL Type: DOUBLE PRECISION
# Unit: Deg
hot_point_lon:

# SQL Type: DOUBLE PRECISION
# Unit: Pure Number
geometric_albedo:

# SQL Type: DOUBLE PRECISION
# Unit: Pure Number
geometric_albedo_error_min:

# SQL Type: DOUBLE PRECISION
# Unit: Pure Number
geometric_albedo_error_max:

# SQL Type: DOUBLE PRECISION
# Unit: Pure Number
log_g:

# SQL Type: VARCHAR
# Possible values: RADIAL_VELOCITY, TIMMING, MICROLENSING, IMAGING, PRIMARY_TRANSIT, ASTROMETRY, TTV, OTHER, SECONDARY_TRANSIT
# MANDATORY
detection_type:

# SQL Type: VARCHAR
# Possible value: RADIAL_VELOCITY, TIMMING, CONTROVERSIAL, MICROLENSING, ASTROMETRY, TTV, SPECTRUM, THEORETICAL
mass_detection_type:

# SQL Type: VARCHAR
# Possible value: PRIMARY_TRANSIT, THEORETICAL, FLUX
radius_detection_type:

# SQL Type: VARCHAR(60)
# Notes: Format is: all names separate with a ';'.
# Examples: "tess-1009.01;Tess_1009.01"
alternate_names:

# Notes: Not implemented.
molecules:

# -------------------
# | Star parameters |
# -------------------

# SQL Type: VARCHAR(60)
# MANDATORY
star_name:

# SQL Type: DOUBLE PRECISION
# Unit: Radius
# MANDATORY
ra:

# SQL Type: DOUBLE PRECISION
# Unit: Radius
# MANDATORY
dec:

# SQL Type: DOUBLE PRECISION
# Unit: Pure Number
mag_v:

# SQL Type: DOUBLE PRECISION
# Unit: Pure Number
mag_i:

# SQL Type: DOUBLE PRECISION
# Unit: Pure Number
mag_j:

# SQL Type: DOUBLE PRECISION
# Unit: Pure Number
mag_h:

# SQL Type: DOUBLE PRECISION
# Unit: Pure Number
mag_k:

# SQL Type: DOUBLE PRECISION
# Unit: PC
star_distance:

# SQL Type: DOUBLE PRECISION
# Unit: PC
star_distance_error_min:

# SQL Type: DOUBLE PRECISION
# Unit: PC
star_distance_error_max:

# SQL Type: DOUBLE PRECISION
# Unit: Pure Number
star_metallicity:

# SQL Type: DOUBLE PRECISION
# Unit: Pure Number
star_metallicity_error_min:

# SQL Type: DOUBLE PRECISION
# Unit: Pure Number
star_metallicity_error_max:

# SQL Type: DOUBLE PRECISION
# Unit: MSun
star_mass:

# SQL Type: DOUBLE PRECISION
# Unit: MSun
star_mass_error_min:

# SQL Type: DOUBLE PRECISION
# Unit: MSun
star_mass_error_max:

# SQL Type: DOUBLE PRECISION
# Unit: RSun
star_radius:

# SQL Type: DOUBLE PRECISION
# Unit: RSun
star_radius_error_min:

# SQL Type: DOUBLE PRECISION
# Unit: RSun
star_radius_error_max:

# SQL Type: VARCHAR(15)
star_sp_type:

# SQL Type: DOUBLE PRECISION
# Unit: Gy
star_age:

# SQL Type: DOUBLE PRECISION
# Unit: Gy
star_age_error_min:

# SQL Type: DOUBLE PRECISION
# Unit: Gy
star_age_error_max:

# SQL Type: DOUBLE PRECISION
# Unit: K
star_teff:

# SQL Type: DOUBLE PRECISION
# Unit: K
star_teff_error_min:

# SQL Type: DOUBLE PRECISION
# Unit: K
star_teff_error_max:

# SQL Type: VARCHAR
# Possible values: IMAGING, IR_EXCESS
star_detected_disc:

# SQL Type: BOOLEAN
# Possible values: True, False
star_magnetic_field:

# SQL Type: VARCHAR(60)
# Notes: Format is all names separate with a ';'.
# Examples: "tess-1009;Tess_1009"
star_alternate_names:

# --------------------------
# | Publication parameters |
# --------------------------

# SQL Type: VARCHAR
# Possible values: BOOK, THESIS, REFEREED_ARTICLE, PROCEEDING, UNKNOWN, REPORT, ARXIV
# MANDATORY
publication_type:

# SQL Type: TEXT
# MANDATORY
publication_title:

# SQL Type: VARCHAR(10)
# Notes: "jj/mm/yyyy" format.
# MANDATORY
publication_date:

# SQL Type: TEXT
publication_url:
