# Pytest imports
import pytest
from pytest import param

# Standard imports
from decimal import Decimal
from pathlib import Path

# Third party imports
from assertpy import assert_that, soft_assertions
from exochoices import WebStatus as wS

# First party imports
from exoimport import Configuration
from exoimport.config.config_utils import Tables, load_config

# Local imports
from ..conftest import tables_in_config_as_funct

CWD = Path.cwd()
TEST_INPUT_PATH = "tests/input_files/config_files"

TABLES_LOAD_CONFIG = {
    "import": "app_import",
    "star": "app_star",
    "alternate_star_name": "app_alternate_star_name",
    "planet": "app_planet",
    "alternate_planet_name": "app_alternate_planet_name",
    "molecule": "app_molecule",
    "atmoshpere_molecule": "app_atmoshpere_molecule",
    "publication": "app_publication",
    "planet2publication": "app_planet2publication",
    "star2publication": "app_star2publication",
    "planetary_system": "app_planetary_system",
    "planet2star": "app_planet2star",
}


@pytest.mark.parametrize(
    "path, expected_epsilon, expected_status, expected_tables",
    [
        param(
            CWD / TEST_INPUT_PATH / "epsilon.toml",
            2,
            "active",
            TABLES_LOAD_CONFIG,
            id="toml file",
        ),
        param(
            CWD / TEST_INPUT_PATH / "epsilon.yaml",
            3,
            "hidden",
            TABLES_LOAD_CONFIG,
            id="yaml file",
        ),
        param(
            CWD / TEST_INPUT_PATH / "epsilon.yml",
            4,
            "imported",
            TABLES_LOAD_CONFIG,
            id="yml file",
        ),
    ],
)
def test_load_config(path, expected_epsilon, expected_status, expected_tables):
    assert load_config(path) == dict(
        epsilon_ra_dec=expected_epsilon,
        web_statuses=expected_status,
        tables=expected_tables,
    )


def test_load_config_wrong_ext():
    assert load_config(CWD / TEST_INPUT_PATH / "toto.txt") == None


@pytest.mark.parametrize(
    "path, expected_epsilon, expected_status",
    [
        param(
            CWD / TEST_INPUT_PATH / "epsilon.toml",
            2,
            wS.ACTIVE,
            id="toml file",
        ),
        param(
            CWD / TEST_INPUT_PATH / "epsilon.yaml",
            3,
            wS.HIDDEN,
            id="yaml file",
        ),
        param(
            CWD / TEST_INPUT_PATH / "epsilon.yml",
            4,
            wS.IMPORTED,
            id="yml file",
        ),
    ],
)
def test_configuration_w_config_file(
    path, expected_epsilon, expected_status, tables_in_config
):
    config = Configuration(file_path=path)
    with soft_assertions():
        assert_that(config.epsilon_ra_dec).is_equal_to(Decimal(expected_epsilon))
        assert_that(config.web_statuses).is_equal_to(expected_status)
        assert_that(config.tables).is_equal_to(Tables(tables_in_config))


@pytest.mark.parametrize(
    "cwd, expected_epsilon, expected_status",
    [
        param(
            CWD / TEST_INPUT_PATH / "default_yaml",
            5,
            wS.ACTIVE,
            id="default yaml file",
        ),
        param(
            CWD / TEST_INPUT_PATH / "default_yml",
            6,
            wS.HIDDEN,
            id="default yml file",
        ),
        param(
            CWD / TEST_INPUT_PATH / "default_toml",
            7,
            wS.IMPORTED,
            id="default toml file",
        ),
    ],
)
def test_configuration_default(
    cwd, expected_epsilon, expected_status, tables_in_config
):
    config = Configuration(cwd=cwd)
    with soft_assertions():
        assert_that(config.epsilon_ra_dec).is_equal_to(Decimal(expected_epsilon))
        assert_that(config.web_statuses).is_equal_to(expected_status)
        assert_that(config.tables).is_equal_to(Tables(tables_in_config))


def test_configuration_given_config(tables_in_config):
    config = Configuration(
        epsilon=1,
        web_statuses="active",
        tables=tables_in_config,
    )
    with soft_assertions():
        assert_that(config.epsilon_ra_dec).is_equal_to(Decimal("1"))
        assert_that(config.web_statuses).is_equal_to(wS.ACTIVE)
        assert_that(config.tables).is_equal_to(Tables(tables_in_config))


@pytest.mark.parametrize(
    "epsilon, expected_epsilon",
    [
        param(1, Decimal("1"), id="epsilon: given"),
        param(None, Decimal("2"), id="epsilon: default"),
    ],
)
@pytest.mark.parametrize(
    "web_statuses, expected_status",
    [
        param("hidden", wS.HIDDEN, id="web_status: given"),
        param(None, wS.ACTIVE, id="web_status: default"),
    ],
)
@pytest.mark.parametrize(
    "tables, expected_tables",
    [
        param(
            tables_in_config_as_funct(),
            Tables(tables_in_config_as_funct()),
            id="tables: given",
        ),
        param(None, Tables(tables_in_config_as_funct()), id="tables: default"),
    ],
)
def test_configuration_overload(
    epsilon,
    web_statuses,
    tables,
    expected_epsilon,
    expected_status,
    expected_tables,
):
    config = Configuration(
        file_path=CWD / TEST_INPUT_PATH / "epsilon.toml",
        epsilon=epsilon,
        web_statuses=web_statuses,
        tables=tables,
    )
    with soft_assertions():
        assert_that(config.epsilon_ra_dec).is_equal_to(expected_epsilon)
        assert_that(config.web_statuses).is_equal_to(expected_status)
        assert_that(config.tables).is_equal_to(expected_tables)


def test_configuration_cmd_epsilon_type_error():
    with pytest.raises(TypeError):
        Configuration(epsilon="a", web_statuses="active")


@pytest.mark.parametrize(
    "path, cwd",
    [
        param(CWD / TEST_INPUT_PATH / "toto.toml", None, id="given file not found"),
        param(
            None,
            CWD / TEST_INPUT_PATH / "default_empty",
            id="no default config file",
        ),
    ],
)
def test_configuration_no_config_file(path, cwd):
    with pytest.raises(FileNotFoundError):
        Configuration(file_path=path, cwd=cwd)


@pytest.mark.parametrize(
    "path",
    [
        param(CWD / TEST_INPUT_PATH / "none_epsilon.toml", id="No epsilon"),
        param(
            CWD / TEST_INPUT_PATH / "wrong_type_epsilon.yml",
            id="Wrong type for epsilon",
        ),
        param(CWD / TEST_INPUT_PATH / "wrong_ext.txt", id="Wrong extension of file"),
        param(
            CWD / TEST_INPUT_PATH / "wrong_type_tables.yml", id="Wrong type for tables"
        ),
        param(CWD / TEST_INPUT_PATH / "none_tables.yml", id="No tables"),
        param(CWD / TEST_INPUT_PATH / "a_table_is_none.yml", id="A table is None"),
    ],
)
def test_configuration_wrong_config(path):
    with pytest.raises(TypeError):
        Configuration(file_path=path)
