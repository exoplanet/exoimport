# Pytest imports
import pytest

# Standard imports
from decimal import Decimal
from pathlib import Path

# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from exoimport import (
    CsvData,
    InvalidUrlError,
    format_csv,
    read_csv,
    split_star_planet_and_other_params,
)
from exoimport.format.format import format_to_import, format_to_import_publications


def test_format_stars():

    data = [
        {
            "star_name": "1001",
            "ra": Decimal("122.5804650"),
            "dec": Decimal("-5.5138520"),
            "mag_v": None,
            "star_distance": Decimal("295.8620000"),
            "star_distance_error_min": Decimal("5.9100000"),
            "star_distance_error_max": Decimal("5.9100000"),
            "star_mass": None,
            "star_mass_error_min": None,
            "star_mass_error_max": None,
            "star_sp_type": None,
            "star_age": None,
            "star_age_error_min": None,
            "star_age_error_max": None,
            "star_detected_disc": None,
            "star_magnetic_field": None,
            "star_status": Decimal("1"),
        },
        {
            "star_name": "1007",
            "ra": Decimal("112.7523930"),
            "dec": Decimal("-4.4633590"),
            "mag_v": None,
            "star_distance": Decimal("283.2910000"),
            "star_distance_error_min": Decimal("3.0025000"),
            "star_distance_error_max": Decimal("3.0025000"),
            "star_mass": None,
            "star_mass_error_min": None,
            "star_mass_error_max": None,
            "star_sp_type": None,
            "star_age": None,
            "star_age_error_min": None,
            "star_age_error_max": None,
            "star_detected_disc": None,
            "star_magnetic_field": None,
            "star_status": Decimal("1"),
        },
    ]

    expected = (
        [
            "name",
            "ra",
            "dec",
            "magnitude_v",
            "distance",
            "mass",
            "spec_type",
            "age",
            "detected_disc",
            "magnetic_field",
            "status",
            "distance_error",
            "mass_error",
            "age_error",
        ],
        [
            (
                "1001",
                Decimal("122.5804650"),
                Decimal("-5.5138520"),
                None,
                Decimal("295.8620000"),
                None,
                None,
                None,
                None,
                None,
                Decimal("1"),
                [Decimal("5.9100000"), Decimal("5.9100000")],
                None,
                None,
            ),
            (
                "1007",
                Decimal("112.7523930"),
                Decimal("-4.4633590"),
                None,
                Decimal("283.2910000"),
                None,
                None,
                None,
                None,
                None,
                Decimal("1"),
                [Decimal("3.0025000"), Decimal("3.0025000")],
                None,
                None,
            ),
        ],
    )

    assert format_to_import(data) == expected


def test_for_star_real_condition(format_stars_expected: CsvData):
    content = read_csv(Path("tests/input_files/csv_files/csv_exoplanet_5_obj_imp.csv"))
    formated_content = format_csv(content)

    stars, planets, other = split_star_planet_and_other_params(formated_content)

    assert format_to_import(stars) == format_stars_expected


def test_for_planets():
    data = [
        {
            "name": Decimal("1001.01"),
            "planet_status": "Candidate",
            "mass": None,
            "mass_sini": None,
            "tconj": None,
            "tzero_tr": Decimal("2459250.0645490"),
            "tzero_tr_sec": None,
            "lambda_angle": None,
            "impact_parameter": None,
            "k": None,
            "k_error_min": None,
            "k_error_max": None,
            "mass_sini_error_min": None,
            "mass_sini_error_max": None,
            "mass_error_min": None,
            "mass_error_max": None,
            "tconj_error_min": None,
            "tconj_error_max": None,
            "tzero_tr_error_min": Decimal("0.001925"),
            "tzero_tr_error_max": Decimal("0.001925"),
            "tzero_tr_sec_error_min": None,
            "tzero_tr_sec_error_max": None,
            "lambda_angle_error_min": None,
            "lambda_angle_error_max": None,
            "impact_parameter_error_min": None,
            "impact_parameter_error_max": None,
        },
        {
            "name": Decimal("1007.01"),
            "planet_status": "Candidate",
            "mass": None,
            "mass_sini": None,
            "tconj": None,
            "tzero_tr": Decimal("2459247.9306950"),
            "tzero_tr_sec": None,
            "lambda_angle": None,
            "impact_parameter": None,
            "k": None,
            "k_error_min": None,
            "k_error_max": None,
            "mass_sini_error_min": None,
            "mass_sini_error_max": None,
            "mass_error_min": None,
            "mass_error_max": None,
            "tconj_error_min": None,
            "tconj_error_max": None,
            "tzero_tr_error_min": Decimal("0.001300"),
            "tzero_tr_error_max": Decimal("0.001300"),
            "tzero_tr_sec_error_min": None,
            "tzero_tr_sec_error_max": None,
            "lambda_angle_error_min": None,
            "lambda_angle_error_max": None,
            "impact_parameter_error_min": None,
            "impact_parameter_error_max": None,
        },
    ]

    expected = (
        [
            "name",
            "planet_status",
            "mass_detected_string",
            "mass_sini_string",
            "tconj",
            "tzero_tr",
            "tzero_tr_sec",
            "lambda_angle",
            "impact_parameter",
            "K",
            "K_error",
            "mass_sini_error_string",
            "mass_detected_error_string",
            "tconj_error",
            "tzero_tr_error",
            "tzero_tr_sec_error",
            "lambda_angle_error",
            "impact_parameter_error",
        ],
        [
            (
                Decimal("1001.01"),
                "Candidate",
                None,
                None,
                None,
                Decimal("2459250.0645490"),
                None,
                None,
                None,
                None,
                None,
                None,
                None,
                None,
                [Decimal("0.001925"), Decimal("0.001925")],
                None,
                None,
                None,
            ),
            (
                Decimal("1007.01"),
                "Candidate",
                None,
                None,
                None,
                Decimal("2459247.9306950"),
                None,
                None,
                None,
                None,
                None,
                None,
                None,
                None,
                [Decimal("0.001300"), Decimal("0.001300")],
                None,
                None,
                None,
            ),
        ],
    )

    assert format_to_import(data) == expected


def test_for_planet_real_condition(format_planets_expected: CsvData):
    content = read_csv(Path("tests/input_files/csv_files/csv_exoplanet_5_obj_imp.csv"))
    formated_content = format_csv(content)

    stars, planets, other = split_star_planet_and_other_params(formated_content)

    assert format_to_import(planets) == format_planets_expected


def test_format_publication():
    data = [
        dict(
            publication_title="Tess_1",
            publication_date=2022,
            publication_url="https://toto.com/",
            publication_type="book",
            status="active",
        ),
        dict(
            publication_title="Tess_2",
            publication_date=2022,
            publication_url="https://titi.com/",
            publication_type="thesis",
            status="active",
        ),
    ]

    expected = (
        ["title", "date", "url", "type", "status"],
        [
            (
                "Tess_1",
                2022,
                '<a target= "_blank"  href="https://toto.com/">website</a>',
                "book",
                "active",
            ),
            (
                "Tess_2",
                2022,
                '<a target= "_blank"  href="https://titi.com/">website</a>',
                "thesis",
                "active",
            ),
        ],
    )

    assert format_to_import_publications(data) == expected


def test_format_publication_exception_invalid_url():
    data = [
        dict(
            publication_title="Tess_1",
            publication_date=2022,
            publication_url="trucmuche",
            publication_type="book",
            status="active",
        ),
    ]

    with pytest.raises(InvalidUrlError):
        format_to_import_publications(data)


def test_format_k_error():
    content = read_csv(Path("tests/input_files/csv_files/test_k_error.csv"))
    formated_content = format_csv(content)

    stars, planets, other = split_star_planet_and_other_params(formated_content)

    formatted_planets = format_to_import(planets)

    headers = formatted_planets[0]
    with soft_assertions():
        assert_that(headers).does_not_contain("K_error_min")
        assert_that(headers).does_not_contain("K_error_max")
        assert_that(headers).does_not_contain("k_error_min")
        assert_that(headers).does_not_contain("k_error_max")
        assert_that(headers).contains("K_error")
