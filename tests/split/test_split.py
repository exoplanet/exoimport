# Pytest imports
import pytest
from pytest import param

# First party imports
from exoimport import split_star_planet_and_other_params


@pytest.mark.parametrize(
    "input_content, expected",
    [
        param(
            [
                {
                    "pl_name": "toto_a",
                    "pl_mass": 12,
                    "star_name": "toto",
                    "star_mass": 72,
                },
                {
                    "pl_name": "toto_b",
                    "pl_mass": 15,
                    "star_name": "toto",
                    "star_mass": 72,
                },
            ],
            (
                [
                    {"star_name": "toto", "star_mass": 72},
                    {"star_name": "toto", "star_mass": 72},
                ],
                [
                    {"pl_name": "toto_a", "pl_mass": 12},
                    {"pl_name": "toto_b", "pl_mass": 15},
                ],
                [{}, {}],
            ),
            id="basic",
        ),
        param(
            [
                {
                    "pl_name": "toto_a",
                    "pl_mass": 12,
                    "star_name": "toto",
                    "star_mass": 72,
                },
                {
                    "pl_name": "toto_b",
                    "pl_mass": None,
                    "star_name": "toto",
                    "star_mass": 72,
                },
            ],
            (
                [
                    {"star_name": "toto", "star_mass": 72},
                    {"star_name": "toto", "star_mass": 72},
                ],
                [
                    {"pl_name": "toto_a", "pl_mass": 12},
                    {"pl_name": "toto_b", "pl_mass": None},
                ],
                [{}, {}],
            ),
            id="with None",
        ),
    ],
)
def test_split(input_content, expected):
    assert split_star_planet_and_other_params(input_content) == expected
