# Pytest imports
import pytest
from pytest import param

# Standard imports
from pathlib import Path

# First party imports
from exoimport import EmptyFileError, InvalidYamlError, NotYamlFileError, read_yaml

TEST_PATH = "tests/input_files/mapping_files"


@pytest.mark.parametrize(
    "file",
    [
        param(
            f"{TEST_PATH}/mapping_tess_candidate_september_2022.yaml",
            id="yaml file",
        ),
        param(f"{TEST_PATH}/mapping_tess.yml", id="yml file"),
    ],
)
def test_read_yaml_success(file):
    read_yaml(Path(file))
    assert True


@pytest.mark.parametrize(
    "expected, file_name",
    [
        param(FileNotFoundError, "toto.yaml", id="file not found"),
        param(NotYamlFileError, "README.md", id="not a csv file"),
        param(EmptyFileError, f"{TEST_PATH}/empty.yml", id="empty file"),
        param(InvalidYamlError, f"{TEST_PATH}/invalid_yaml.yml", id="empty file"),
    ],
)
def test_exception(expected, file_name):
    with pytest.raises(expected):
        read_yaml(Path(file_name))
