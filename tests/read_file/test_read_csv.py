# Pytest imports
import pytest
from pytest import param

# Standard imports
from decimal import Decimal
from pathlib import Path

# Third party imports
from assertpy import assert_that

# First party imports
from exoimport import EmptyFileError, NotCsvFileError, format_csv, read_csv

TEST_PATH = "tests/input_files/csv_files"


def test_read_csv_success():
    read_csv(Path(f"{TEST_PATH}/tess.csv"))
    assert True


@pytest.mark.parametrize(
    "expected, file_name",
    [
        param(FileNotFoundError, "wrong.csv", id="file not found"),
        param(NotCsvFileError, "README.md", id="not a csv file"),
        param(EmptyFileError, f"{TEST_PATH}/empty.csv", id="empty csv file"),
    ],
)
def test_exception(expected, file_name):
    with pytest.raises(expected):
        read_csv(Path(file_name))


def test_format_csv():
    content = read_csv(Path(f"{TEST_PATH}/short_tess.csv"))
    assert_that(format_csv(content)).is_equal_to(
        [
            {
                "rowid": Decimal("1"),
                "toi": Decimal("1000.01"),
                "toipfx": Decimal("1000"),
                "tid": Decimal("50365310"),
                "ctoi_alias": Decimal("50365310.01"),
                "pl_pnum": Decimal("1"),
                "tfopwg_disp": "FP",
                "rastr": "07h29m25.85s",
                "ra": Decimal("112.3577080"),
                "raerr1": None,
                "raerr2": None,
                "decstr": "-12d41m45.46s",
                "dec": Decimal("-12.6959600"),
                "decerr1": None,
                "decerr2": None,
            },
            {
                "rowid": Decimal("2"),
                "toi": Decimal("1001.01"),
                "toipfx": Decimal("1001"),
                "tid": Decimal("88863718"),
                "ctoi_alias": Decimal("88863718.01"),
                "pl_pnum": Decimal("1"),
                "tfopwg_disp": "PC",
                "rastr": "08h10m19.31s",
                "ra": Decimal("122.5804650"),
                "raerr1": None,
                "raerr2": None,
                "decstr": "-05d30m49.87s",
                "dec": Decimal("-5.5138520"),
                "decerr1": None,
                "decerr2": None,
            },
        ]
    )
