# Standard imports
from pathlib import Path

# First party imports
from exoimport import (
    CsvData,
    format_csv,
    input_filter,
    read_csv,
    read_yaml,
    translate,
    write_csv,
)

TEST_PATH = "tests/input_files"


def test_write_translation(tmp_path):
    content = [
        ["toto", "titi", "tutu", "tata"],
        ["riri", "fifi", "loulou", 12],
        ["pull", "push", "commit", 38],
    ]

    expected_content = [
        ["toto", "titi", "tutu", "tata"],
        ["riri", "fifi", "loulou", "12"],
        ["pull", "push", "commit", "38"],
    ]

    dir_path = tmp_path / "test_translate_csv"
    test_file = "translate_csv_first_test.csv"

    write_csv(content, dir_path, test_file)

    check_content = read_csv(Path(dir_path / test_file))
    assert expected_content == check_content


def test_write_in_real_condition(tmp_path, output_csv_expected: CsvData):
    mapping = read_yaml(Path(f"{TEST_PATH}/mapping_files/mapping_tess.yml"))
    csv_content = read_csv(Path(f"{TEST_PATH}/csv_files/tess.csv"))

    formatted_csv = format_csv(csv_content)
    filter_str = mapping.get("filter")
    filtered_csv = input_filter(filter_str, formatted_csv, [], False)
    translate_csv = translate(mapping, filtered_csv)

    dir_path = tmp_path / "test_translate_csv"
    test_file = "translate_csv_real_condition_test.csv"

    write_csv(translate_csv, dir_path, test_file)

    check_csv = read_csv(Path(dir_path / test_file))

    # output_csv_expected return a list cf. conftest.py
    assert check_csv == output_csv_expected
