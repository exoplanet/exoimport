# Standard imports
from pathlib import Path

# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from exoimport import CsvData, format_csv, input_filter, read_csv, read_yaml

TEST_PATH = "tests/input_files"


def test_write_filtered_rows(tmp_path):
    content = [
        {"toto": 11, "titi": 12, "tutu": "Candidate"},
        {"toto": 7, "titi": 8, "tutu": "Candidate"},
        {"toto": 15, "titi": 16, "tutu": "Candidate"},
    ]

    filter = "toto > 10"
    warning_msg = []

    dir_path = tmp_path / "test_filtered_rows"
    dir_path.mkdir()
    test_file_path = dir_path / "filtered.csv"

    input_filter(filter, content, warning_msg, True, test_file_path)

    csv_of_filtered = read_csv(Path(test_file_path))

    with soft_assertions():
        assert_that(warning_msg).is_equal_to(
            [
                "  [bold green on black]2 rows[/bold green on black] kept out of "
                "[white on black]3[/white on black], [bold orange3 on black]1 rows"
                "[/bold orange3 on black] filtered.",
            ]
        )
        assert_that(csv_of_filtered).is_equal_to(
            [
                ["toto", "titi", "tutu"],
                ["7", "8", "Candidate"],
            ]
        )


def test_write_filtered_rows_real_conditions(
    tmp_path,
    output_filtered_expected: CsvData,
):
    mapping = read_yaml(Path(f"{TEST_PATH}/mapping_files/mapping_tess.yml"))
    csv_content = read_csv(Path(f"{TEST_PATH}/csv_files/tess.csv"))

    dir_path = tmp_path / "test_filtered_rows"
    dir_path.mkdir()
    test_file_path = f"{dir_path}/filtered.csv"

    formatted_csv = format_csv(csv_content)
    filter_str = mapping.get("filter")
    input_filter(filter_str, formatted_csv, [], True, test_file_path)

    check_csv = read_csv(Path(test_file_path))

    # output_filtered_expected return a list cf. conftest.py
    assert check_csv == output_filtered_expected
