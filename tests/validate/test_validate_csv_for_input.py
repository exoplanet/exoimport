# Standard imports
from pathlib import Path

# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from exoimport import read_csv
from exoimport.validate import get_fields, validate_headers_for_import

TEST_PATH_CSV = "tests/input_files/csv_files"


def test_get_fields(fields_expected):
    assert get_fields() == fields_expected


def test_validate_csv_header_success():
    content = read_csv(Path(f"{TEST_PATH_CSV}/csv_exoplanet.csv"))
    fields = get_fields()

    assert validate_headers_for_import(content, fields, warning_messages=[])


def test_validate_csv_header_fails():
    content = read_csv(Path(f"{TEST_PATH_CSV}/wrong_csv_exoplanet.csv"))
    fields = get_fields()

    warning_messages = []
    res = validate_headers_for_import(content, fields, warning_messages)

    with soft_assertions():
        assert_that(res).is_false()
        assert_that(warning_messages).is_equal_to(
            [
                "  [bold orange3 on black] Csv headers errors: "
                "[/bold orange3 on black]",
                "  4 header(s) are not fields of database.",
                """\t-"pl_name" is not a fields in exoplanet.eu database.""",
                """\t-"pl_mass" is not a fields in exoplanet.eu database.""",
                """\t-"pl_radius" is not a fields in exoplanet.eu database.""",
                """\t-"publication" is not a fields in exoplanet.eu database.""",
                "\n",
            ]
        )
