# Standard imports
from pathlib import Path

# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from exoimport import read_csv, validate_csv_headers

TEST_PATH = "tests/input_files/csv_files"


def test_validate_csv_header_success():
    content = read_csv(Path(f"{TEST_PATH}/tess.csv"))
    assert validate_csv_headers(content, warning_messages=[])


def test_validate_csv_header_fails():
    content = read_csv(Path(f"{TEST_PATH}/wrong_header.csv"))
    warning_messages = []
    res = validate_csv_headers(content, warning_messages)

    with soft_assertions():
        assert_that(res).is_false()
        assert_that(warning_messages).is_equal_to(
            [
                "  [bold orange3 on black] CSV errors: [/bold orange3 on black]",
                "  5 header(s) are invalid headers.",
                """\t-The header "rowid 1" is not a valid header.""",
                """\t-The header "8toipfx" is not a valid header.""",
                """\t-The header "+pl_pnum" is not a valid header.""",
                """\t-The header "/rastr" is not a valid header.""",
                """\t-The header "ra(ti)" is not a valid header.""",
                "\n",
            ]
        )
