# Pytest imports
import pytest

# Standard imports
from pathlib import Path

# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from exoimport import (
    PythonSyntaxError,
    format_csv,
    read_csv,
    read_yaml,
    validate_mapping_yaml,
)

TEST_PATH_YAML = "tests/input_files/mapping_files"
TEST_PATH_CSV = "tests/input_files/csv_files"


def test_validate_yaml_mapping_success():
    mapping = read_yaml(Path(f"{TEST_PATH_YAML}/mapping_tess.yml"))
    csv_content = read_csv(Path(f"{TEST_PATH_CSV}/tess.csv"))
    formatted_csv = format_csv(csv_content)

    assert validate_mapping_yaml(mapping, formatted_csv, warning_messages=[])


def test_validate_mapping_header_fails():
    mapping = read_yaml(Path(f"{TEST_PATH_YAML}/wrong_mapping.yml"))
    csv_content = read_csv(Path(f"{TEST_PATH_CSV}/tess.csv"))
    formatted_csv = format_csv(csv_content)
    warning_messages = []

    res = validate_mapping_yaml(mapping, formatted_csv, warning_messages)

    with soft_assertions():
        assert_that(res).is_false()
        assert_that(warning_messages).is_equal_to(
            [
                "  [bold orange3 on black] Mapping Yaml errors: "
                "[/bold orange3 on black]",
                "  4 formula(s) are invalid.",
                '\t-"filter: tfopwg_disp == PC" is not a valid formula.',
                '\t-"mass: mass_toto" is not a valid formula.',
                '\t-"orbital_period: orbper" is not a valid formula.',
                '\t-"eccentricity: pouf" is not a valid formula.',
                "\n",
            ]
        )


def test_validate_mapping_empty():
    mapping = read_yaml(Path(f"{TEST_PATH_YAML}/base_mapping.yml"))
    csv_content = read_csv(Path(f"{TEST_PATH_CSV}/tess.csv"))
    formatted_csv = format_csv(csv_content)
    warning_messages = []

    res = validate_mapping_yaml(mapping, formatted_csv, warning_messages)

    with soft_assertions():
        assert_that(res).is_false()
        assert_that(warning_messages).is_equal_to(
            [
                "  [bold orange3 on black] Mapping Yaml errors: "
                "[/bold orange3 on black]",
                "\tThis mapping file is [bold red on black]empty[/bold red on black], "
                "all field are equal to None.\n",
            ]
        )


def test_validate_mapping_python_syntax_error():
    mapping = read_yaml(Path(f"{TEST_PATH_YAML}/python_syntax_error.yml"))
    csv_content = read_csv(Path(f"{TEST_PATH_CSV}/tess.csv"))
    formatted_csv = format_csv(csv_content)
    with pytest.raises(PythonSyntaxError):
        validate_mapping_yaml(mapping, formatted_csv, warning_messages=[])
