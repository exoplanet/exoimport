"""Useful function to testing the similarity."""

# Standard imports
from decimal import Decimal
from typing import Tuple

# Third party imports
from psycopg import Connection
from py_linq_sql import SQLEnumerable

# First party imports
from exoimport.utils import DbData


def insert_star(
    conn: Connection,
    table: str,
    data: Tuple[str, Decimal, Decimal, Decimal],
) -> None:
    """
    Insert a star in table 'app_star'.

    Args:
        conn: Connection to the db.
        table: Name of the table.
        data: Data of the star.
    """
    SQLEnumerable(conn, table).insert(["name", "ra", "dec", "status"], data).execute()


def insert_planet(conn: Connection, table: str, name: str) -> None:
    """
    Insert planet in table 'app_planet'.

    Args:
        conn: Connection to the db.
        table: Name of the table.
        name: Planet's name.
    """
    SQLEnumerable(conn, table).insert(
        ["name", "detection_type", "publication_status", "status"],
        (name, [3], 1, 1),
    ).execute()
