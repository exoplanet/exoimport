# Pytest imports
import pytest
from pytest import param

# Standard imports
from decimal import Decimal

# First party imports
from exoimport import StarRaDec, is_planet_already_in_db, is_star_already_in_db
from exoimport.config import Tables
from exoimport.similarity import _is_at_the_same_position

# Local imports
from .conftest import insert_planet, insert_star

SMALL_DELTA = Decimal("0.0001")

# The real number for BIG_DELTA is 0.00028 but postgresql make its trigonometric
# functions with double precision (float) and not Numeric (Decimal), 0.000363 is the
# number that works and is closest to the true delta.
BIG_DELTA = Decimal("0.000363")

NO_DELTA = Decimal("0.0000")

# We choose the numbering of octants with the Gay code.
# For more details on the naming and numbering of octants:
# https://en.wikipedia.org/wiki/Octant_(solid_geometry)
OCTANTS = [
    StarRaDec(Decimal("40"), Decimal("40")),  # 0
    StarRaDec(Decimal("130"), Decimal("40")),  # 1
    StarRaDec(Decimal("220"), Decimal("40")),  # 2
    StarRaDec(Decimal("310"), Decimal("40")),  # 3
    StarRaDec(Decimal("310"), Decimal("-40")),  # 4
    StarRaDec(Decimal("220"), Decimal("-40")),  # 5
    StarRaDec(Decimal("130"), Decimal("-40")),  # 6
    StarRaDec(Decimal("40"), Decimal("-40")),  # 7
    StarRaDec(Decimal("50"), Decimal("90")),
    StarRaDec(Decimal("50"), Decimal("-90")),
]


@pytest.mark.xfail(
    reason="Trigonometric function postgresql take and return floating number "
    "(double precision) and not Decimal (numeric). "
    "They are converting these functions to numeric.",
)
def test_real_delta(
    db_connection_with_only_schema_for_modif,
    table_stars,
):
    conn, table = db_connection_with_only_schema_for_modif, table_stars

    ra_oct_0 = OCTANTS[0].ra
    dec_oct_0 = OCTANTS[0].dec

    insert_star(
        conn,
        table,
        ("toto", ra_oct_0, dec_oct_0, Decimal(1)),
    )

    star_ra_dec = StarRaDec(
        ra=ra_oct_0 + Decimal("0.00028"),
        dec=dec_oct_0,
    )

    assert not _is_at_the_same_position(conn, table, star_ra_dec, Decimal("1"))


@pytest.mark.parametrize(
    "star_at_octant",
    [
        param(OCTANTS[8], id="dec 90°"),
        param(OCTANTS[9], id="-90°"),
    ],
)
@pytest.mark.parametrize(
    "delta_ra",
    [
        param(Decimal(+SMALL_DELTA), id="bit more ra"),
        param(Decimal(-SMALL_DELTA), id="little less ra"),
        param(Decimal(+BIG_DELTA), id="much more ra"),
        param(Decimal(-BIG_DELTA), id="much less ra"),
        param(Decimal(NO_DELTA), id="exactly same position"),
    ],
)
def test_special_case_dec_is_90(
    db_connection_with_only_schema_for_modif,
    table_stars,
    star_at_octant,
    delta_ra,
):
    conn, table = db_connection_with_only_schema_for_modif, table_stars

    insert_star(
        conn,
        table,
        ("toto", star_at_octant.ra, star_at_octant.dec, Decimal(1)),
    )

    star_ra_dec = StarRaDec(
        ra=star_at_octant.ra + delta_ra,
        dec=star_at_octant.dec,
    )

    assert _is_at_the_same_position(conn, table, star_ra_dec, Decimal("1"))


@pytest.mark.parametrize(
    "star_at_octant",
    [
        param(OCTANTS[0], id="octan 0"),
        param(OCTANTS[1], id="octan 1"),
        param(OCTANTS[2], id="octan 2"),
        param(OCTANTS[3], id="octan 3"),
        param(OCTANTS[4], id="octan 4"),
        param(OCTANTS[5], id="octan 5"),
        param(OCTANTS[6], id="octan 6"),
        param(OCTANTS[7], id="octan 7"),
    ],
)
@pytest.mark.parametrize(
    "delta_ra, delta_dec, expected",
    [
        # RA
        param(Decimal(+SMALL_DELTA), Decimal(NO_DELTA), True, id="bit more ra"),
        param(Decimal(-SMALL_DELTA), Decimal(NO_DELTA), True, id="little less ra"),
        param(Decimal(+BIG_DELTA), Decimal(NO_DELTA), False, id="much more ra"),
        param(Decimal(-BIG_DELTA), Decimal(NO_DELTA), False, id="much less ra"),
        # DEC
        param(Decimal(NO_DELTA), Decimal(+SMALL_DELTA), True, id="bit more dec"),
        param(Decimal(NO_DELTA), Decimal(-SMALL_DELTA), True, id="little less dec"),
        param(Decimal(NO_DELTA), Decimal(+BIG_DELTA), False, id="much more dec"),
        param(Decimal(NO_DELTA), Decimal(-BIG_DELTA), False, id="much less dec"),
        # RA & DEC
        param(
            Decimal(+SMALL_DELTA),
            Decimal(+SMALL_DELTA),
            True,
            id="bit more ra & dec",
        ),
        param(
            Decimal(-SMALL_DELTA),
            Decimal(-SMALL_DELTA),
            True,
            id="litte less ra & dec",
        ),
        param(Decimal(+BIG_DELTA), Decimal(+BIG_DELTA), False, id="much more ra & dec"),
        param(Decimal(-BIG_DELTA), Decimal(-BIG_DELTA), False, id="much less ra & dec"),
        param(
            Decimal(-SMALL_DELTA),
            Decimal(+SMALL_DELTA),
            True,
            id="litlle less ra & bit more dec",
        ),
        param(
            Decimal(-BIG_DELTA),
            Decimal(+BIG_DELTA),
            False,
            id="much less ra & much more dec",
        ),
        param(
            Decimal(+SMALL_DELTA),
            Decimal(-SMALL_DELTA),
            True,
            id="bit more ra & litlle less dec",
        ),
        param(
            Decimal(+BIG_DELTA),
            Decimal(-BIG_DELTA),
            False,
            id="much more ra & much less dec",
        ),
        # Exactly same position
        param(Decimal(NO_DELTA), Decimal(NO_DELTA), True, id="exactly same position"),
    ],
)
def test_at_the_same_pos(
    db_connection_with_only_schema_for_modif,
    table_stars,
    star_at_octant,
    delta_ra,
    delta_dec,
    expected,
):
    conn, table = db_connection_with_only_schema_for_modif, table_stars

    insert_star(
        conn,
        table,
        ("toto", star_at_octant.ra, star_at_octant.dec, Decimal(1)),
    )

    star_ra_dec = StarRaDec(
        ra=star_at_octant.ra + delta_ra,
        dec=star_at_octant.dec + delta_dec,
    )

    assert _is_at_the_same_position(conn, table, star_ra_dec, Decimal("1")) == expected


@pytest.mark.parametrize(
    "star_one, star_two",
    [
        # Octant 0
        param(OCTANTS[0], OCTANTS[1], id="0 and 1"),
        param(OCTANTS[0], OCTANTS[2], id="0 and 2"),
        param(OCTANTS[0], OCTANTS[3], id="0 and 3"),
        param(OCTANTS[0], OCTANTS[4], id="0 and 4"),
        param(OCTANTS[0], OCTANTS[5], id="0 and 5"),
        param(OCTANTS[0], OCTANTS[6], id="0 and 6"),
        param(OCTANTS[0], OCTANTS[7], id="0 and 7"),
        # Octant 1
        param(OCTANTS[1], OCTANTS[2], id="1 and 2"),
        param(OCTANTS[1], OCTANTS[3], id="1 and 3"),
        param(OCTANTS[1], OCTANTS[4], id="1 and 4"),
        param(OCTANTS[1], OCTANTS[5], id="1 and 5"),
        param(OCTANTS[1], OCTANTS[6], id="1 and 6"),
        param(OCTANTS[1], OCTANTS[7], id="1 and 7"),
        # Octant 2
        param(OCTANTS[2], OCTANTS[3], id="2 and 3"),
        param(OCTANTS[2], OCTANTS[4], id="2 and 4"),
        param(OCTANTS[2], OCTANTS[5], id="2 and 5"),
        param(OCTANTS[2], OCTANTS[6], id="2 and 6"),
        param(OCTANTS[2], OCTANTS[7], id="2 and 7"),
        # Octant 3
        param(OCTANTS[3], OCTANTS[4], id="3 and 4"),
        param(OCTANTS[3], OCTANTS[5], id="3 and 5"),
        param(OCTANTS[3], OCTANTS[6], id="3 and 6"),
        param(OCTANTS[3], OCTANTS[7], id="3 and 7"),
        # Octant 4
        param(OCTANTS[4], OCTANTS[5], id="4 and 5"),
        param(OCTANTS[4], OCTANTS[6], id="4 and 6"),
        param(OCTANTS[4], OCTANTS[7], id="4 and 7"),
        # Octant 5
        param(OCTANTS[5], OCTANTS[6], id="5 and 6"),
        param(OCTANTS[5], OCTANTS[7], id="5 and 7"),
        # Octant 6
        param(OCTANTS[6], OCTANTS[7], id="6 and 7"),
    ],
)
def test_same_pos_in_different_octants(
    db_connection_with_only_schema_for_modif,
    table_stars,
    star_one,
    star_two,
):
    conn, table = db_connection_with_only_schema_for_modif, table_stars

    insert_star(conn, table, ("toto", star_one.ra, star_one.dec, Decimal("1")))

    assert not _is_at_the_same_position(conn, table, star_two, Decimal("1"))


@pytest.mark.parametrize(
    "name, position, expected",
    [
        param("titi", OCTANTS[6], False, id="different name and position"),
        param("titi", OCTANTS[0], True, id="different name"),
        param("toto", OCTANTS[6], True, id="different position"),
        param("toto", OCTANTS[0], True, id="same name and position"),
        param("TOTO", OCTANTS[6], True, id="same name insensitive case - all upper"),
        param("TotO", OCTANTS[6], True, id="same name insensitive case - mix"),
    ],
)
def test_star_similarity(
    db_connection_with_only_schema_for_modif,
    table_stars,
    name,
    position,
    expected,
    tables_in_config,
):
    conn, table = db_connection_with_only_schema_for_modif, table_stars

    insert_star(
        conn,
        table,
        ("toto", OCTANTS[0].ra, OCTANTS[0].dec, Decimal("1")),
    )

    assert (
        is_star_already_in_db(
            conn,
            (name, position),
            Decimal("1"),
            Tables(tables_in_config),
        )
        == expected
    )


@pytest.mark.parametrize(
    "name, expected",
    [
        param("titi", False, id="different name"),
        param("toto", True, id="same name"),
        param("TOTO", True, id="same name insensitive case - all upper"),
        param("ToTO", True, id="same name insensitive case - mix"),
    ],
)
def test_planet_similarity(
    db_connection_with_only_schema_for_modif,
    table_planets,
    name,
    expected,
    tables_in_config,
):
    conn, table = db_connection_with_only_schema_for_modif, table_planets

    insert_planet(conn, table, "toto")

    assert is_planet_already_in_db(conn, name, Tables(tables_in_config)) == expected
