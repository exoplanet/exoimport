# Pytest imports
import pytest
from pytest import param

# Standard imports
import datetime
import zoneinfo
from decimal import Decimal

# Third party imports
from assertpy import assert_that, soft_assertions
from py_linq_sql import SQLEnumerable

# First party imports
from exoimport.import_in_db.import_stars import import_stars

# Local imports
from ..conftest import get_last_id_for_fake_insert

COLUMN = [
    "name",
    "ra",
    "dec",
    "magnitude_v",
    "magnitude_i",
    "magnitude_j",
    "magnitude_h",
    "magnitude_k",
    "distance",
    "metallicity",
    "mass",
    "radius",
    "spec_type",
    "age",
    "teff",
    "detected_disc",
    "magnetic_field",
    "status",
    "distance_error",
    "metallicity_error",
    "mass_error",
    "radius_error",
    "age_error",
    "teff_error",
    "created",
    "modified",
]


@pytest.mark.parametrize(
    "data_to_import, expected_record, expected_data",
    [
        param(
            [
                (
                    Decimal("1001"),
                    Decimal("122.5804650"),
                    Decimal("-5.5138520"),
                    None,
                    None,
                    None,
                    None,
                    None,
                    Decimal("295.8620000"),
                    None,
                    None,
                    Decimal("2.0100000"),
                    None,
                    None,
                    Decimal("7070.0000000"),
                    None,
                    None,
                    Decimal("1"),
                    [Decimal("5.9100000"), Decimal("5.9100000")],
                    None,
                    None,
                    [Decimal("0.0900000"), Decimal("0.0900000")],
                    None,
                    [Decimal("126.4000000"), Decimal("126.4000000")],
                    "2023-03-03 10:19:43.017580",
                    "2023-03-03 10:19:43.017580",
                ),
            ],  # data_to_import
            lambda last_id: [last_id + 1],  # expected_record
            [
                (
                    "1001",
                    122.5804650,
                    datetime.datetime(
                        2023,
                        3,
                        3,
                        10,
                        19,
                        43,
                        17580,
                        tzinfo=zoneinfo.ZoneInfo(key="Europe/Paris"),
                    ),
                    datetime.datetime(
                        2023,
                        3,
                        3,
                        10,
                        19,
                        43,
                        17580,
                        tzinfo=zoneinfo.ZoneInfo(key="Europe/Paris"),
                    ),
                )
            ],  # expected_data
            id="One star",
        ),
        param(
            [
                (
                    Decimal("1001"),
                    Decimal("122.5804650"),
                    Decimal("-5.5138520"),
                    None,
                    None,
                    None,
                    None,
                    None,
                    Decimal("295.8620000"),
                    None,
                    None,
                    Decimal("2.0100000"),
                    None,
                    None,
                    Decimal("7070.0000000"),
                    None,
                    None,
                    Decimal("1"),
                    [Decimal("5.9100000"), Decimal("5.9100000")],
                    None,
                    None,
                    [Decimal("0.0900000"), Decimal("0.0900000")],
                    None,
                    [Decimal("126.4000000"), Decimal("126.4000000")],
                    "2023-03-03 10:19:43.017580",
                    "2023-03-03 10:19:43.017580",
                ),
                (
                    Decimal("1007"),
                    Decimal("112.7523930"),
                    Decimal("-4.4633590"),
                    None,
                    None,
                    None,
                    None,
                    None,
                    Decimal("283.2910000"),
                    None,
                    None,
                    Decimal("2.7000000"),
                    None,
                    None,
                    Decimal("6596.0000000"),
                    None,
                    None,
                    Decimal("1"),
                    [Decimal("3.0025000"), Decimal("3.0025000")],
                    None,
                    None,
                    [Decimal("0.1300000"), Decimal("0.1300000")],
                    None,
                    [Decimal("139.2000000"), Decimal("139.2000000")],
                    "2023-03-03 10:19:43.017580",
                    "2023-03-03 10:19:43.017580",
                ),
            ],  # data_to_import
            lambda last_id: [last_id + 1, last_id + 2],  # expected_record
            [
                (
                    "1001",
                    122.5804650,
                    datetime.datetime(
                        2023,
                        3,
                        3,
                        10,
                        19,
                        43,
                        17580,
                        tzinfo=zoneinfo.ZoneInfo(key="Europe/Paris"),
                    ),
                    datetime.datetime(
                        2023,
                        3,
                        3,
                        10,
                        19,
                        43,
                        17580,
                        tzinfo=zoneinfo.ZoneInfo(key="Europe/Paris"),
                    ),
                ),
                (
                    "1007",
                    112.7523930,
                    datetime.datetime(
                        2023,
                        3,
                        3,
                        10,
                        19,
                        43,
                        17580,
                        tzinfo=zoneinfo.ZoneInfo(key="Europe/Paris"),
                    ),
                    datetime.datetime(
                        2023,
                        3,
                        3,
                        10,
                        19,
                        43,
                        17580,
                        tzinfo=zoneinfo.ZoneInfo(key="Europe/Paris"),
                    ),
                ),
            ],  # expected_data
            id="many stars",
        ),
    ],
)
def test_import_stars_empty_table(
    db_connection_with_only_schema_for_modif,
    table_stars,
    data_to_import,
    expected_record,
    expected_data,
):
    conn, table = db_connection_with_only_schema_for_modif, table_stars

    last_import_id = get_last_id_for_fake_insert(conn, "app_import")

    last_star_id = get_last_id_for_fake_insert(conn, table_stars)
    formated_import_data = (COLUMN.copy(), data_to_import)

    record = import_stars(conn, table, formated_import_data, import_id=last_import_id)

    check = (
        SQLEnumerable(conn, table)
        .select(lambda x: (x.name, x.ra, x.created, x.modified))
        .where(lambda x: x.id > last_star_id)
        .execute()
    )
    print(check)

    with soft_assertions():
        assert_that(record).is_equal_to(expected_record(last_star_id))
        for check_elem, expect_elem in zip(check, expected_data):
            assert_that(list(check_elem)[:-2]).contains_only(*list(expect_elem)[:-2])
            assert_that(list(check_elem)[-2:]).is_equal_to(list(expect_elem)[-2:])


def test_import_star_non_empty_table(
    db_connection_with_data_for_modif,
    table_stars,
):
    conn, table = db_connection_with_data_for_modif, table_stars

    last_star_id = get_last_id_for_fake_insert(conn, table)
    last_import_id = get_last_id_for_fake_insert(conn, "app_import")

    star = (
        COLUMN.copy(),
        [
            (
                Decimal("1007"),
                Decimal("112.7523930"),
                Decimal("-4.4633590"),
                None,
                None,
                None,
                None,
                None,
                Decimal("283.2910000"),
                None,
                None,
                Decimal("2.7000000"),
                None,
                None,
                Decimal("6596.0000000"),
                None,
                None,
                Decimal("1"),
                [Decimal("3.0025000"), Decimal("3.0025000")],
                None,
                None,
                [Decimal("0.1300000"), Decimal("0.1300000")],
                None,
                [Decimal("139.2000000"), Decimal("139.2000000")],
                "2023-03-03 10:19:43.017580",
                "2023-03-03 10:19:43.017580",
            ),
        ],
    )

    record = import_stars(conn, table, star, import_id=last_import_id)

    assert record[0] == last_star_id + 1
