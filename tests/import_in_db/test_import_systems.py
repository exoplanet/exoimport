# Pytest imports
import pytest
from pytest import param

# Standard imports
from decimal import Decimal
from pathlib import Path

# Third party imports
from assertpy import assert_that, soft_assertions
from py_linq_sql import SQLEnumerable

# First party imports
from exoimport import (
    Configuration,
    ImportSystemRecord,
    PlanetAlreadyInDbError,
    StarAlreadyInDbError,
    import_systems,
)
from exoimport.import_in_db.import_system import (
    _import_one_system,
    import_rogue_planets,
)

# Local imports
from ..conftest import get_last_id_for_fake_insert

PLANETS_FOR_ONE = [
    {
        "name": '"1001.01"',
        "planet_status": Decimal("2"),
        "publication_status": Decimal("6"),
        "mass": None,
        "mass_error_min": None,
        "mass_error_max": None,
        "mass_sini": None,
        "mass_sini_error_min": None,
        "mass_sini_error_max": None,
        "radius": Decimal("10.3168000"),
        "radius_error_min": Decimal("3.2145900"),
        "radius_error_max": Decimal("3.2145900"),
        "orbital_period": Decimal("1.9316710"),
        "orbital_period_error_min": Decimal("0.0000077"),
        "orbital_period_error_max": Decimal("0.0000077"),
        "semi_major_axis": None,
        "semi_major_axis_error_min": None,
        "semi_major_axis_error_max": None,
        "eccentricity": None,
        "eccentricity_error_min": None,
        "eccentricity_error_max": None,
        "inclination": None,
        "inclination_error_min": None,
        "inclination_error_max": None,
        "angular_distance": None,
        "discovered": None,
        "omega": None,
        "omega_error_min": None,
        "omega_error_max": None,
        "tperi": None,
        "tperi_error_min": None,
        "tperi_error_max": None,
        "tconj": None,
        "tconj_error_min": None,
        "tconj_error_max": None,
        "tzero_tr": Decimal("2459250.0645490"),
        "tzero_tr_error_min": Decimal("0.001925"),
        "tzero_tr_error_max": Decimal("0.001925"),
        "tzero_tr_sec": None,
        "tzero_tr_sec_error_min": None,
        "tzero_tr_sec_error_max": None,
        "lambda_angle": None,
        "lambda_angle_error_min": None,
        "lambda_angle_error_max": None,
        "impact_parameter": None,
        "impact_parameter_error_min": None,
        "impact_parameter_error_max": None,
        "tzero_vr": None,
        "tzero_vr_error_min": None,
        "tzero_vr_error_max": None,
        "k": None,
        "k_error_min": None,
        "k_error_max": None,
        "temp_calculated": Decimal("3998.0000000"),
        "temp_calculated_error_min": None,
        "temp_calculated_error_max": None,
        "temp_measured": None,
        "hot_point_lon": None,
        "geometric_albedo": None,
        "geometric_albedo_error_min": None,
        "geometric_albedo_error_max": None,
        "log_g": None,
        "detection_type": [Decimal("3")],
        "mass_detection_type": None,
        "radius_detection_type": None,
        "status": Decimal("1"),
    },
    {
        "name": '"1009.01"',
        "planet_status": Decimal("2"),
        "publication_status": Decimal("6"),
        "mass": None,
        "mass_error_min": None,
        "mass_error_max": None,
        "mass_sini": None,
        "mass_sini_error_min": None,
        "mass_sini_error_max": None,
        "radius": None,
        "radius_error_min": None,
        "radius_error_max": None,
        "orbital_period": Decimal("1.9600296"),
        "orbital_period_error_min": Decimal("0.0000050"),
        "orbital_period_error_max": Decimal("0.0000050"),
        "semi_major_axis": None,
        "semi_major_axis_error_min": None,
        "semi_major_axis_error_max": None,
        "eccentricity": None,
        "eccentricity_error_min": None,
        "eccentricity_error_max": None,
        "inclination": None,
        "inclination_error_min": None,
        "inclination_error_max": None,
        "angular_distance": None,
        "discovered": None,
        "omega": None,
        "omega_error_min": None,
        "omega_error_max": None,
        "tperi": None,
        "tperi_error_min": None,
        "tperi_error_max": None,
        "tconj": None,
        "tconj_error_min": None,
        "tconj_error_max": None,
        "tzero_tr": Decimal("2459252.7513570"),
        "tzero_tr_error_min": Decimal("0.001632"),
        "tzero_tr_error_max": Decimal("0.001632"),
        "tzero_tr_sec": None,
        "tzero_tr_sec_error_min": None,
        "tzero_tr_sec_error_max": None,
        "lambda_angle": None,
        "lambda_angle_error_min": None,
        "lambda_angle_error_max": None,
        "impact_parameter": None,
        "impact_parameter_error_min": None,
        "impact_parameter_error_max": None,
        "tzero_vr": None,
        "tzero_vr_error_min": None,
        "tzero_vr_error_max": None,
        "k": None,
        "k_error_min": None,
        "k_error_max": None,
        "temp_calculated": Decimal("3555.0000000"),
        "temp_calculated_error_min": None,
        "temp_calculated_error_max": None,
        "temp_measured": None,
        "hot_point_lon": None,
        "geometric_albedo": None,
        "geometric_albedo_error_min": None,
        "geometric_albedo_error_max": None,
        "log_g": None,
        "detection_type": [Decimal("3")],
        "mass_detection_type": None,
        "radius_detection_type": None,
        "status": Decimal("1"),
    },
]


def test_import_one_system(
    db_connection_with_only_schema_for_modif,
    table_import,
    table_stars,
    table_planets,
    table_alternate_star_name,
    table_alternate_planet_name,
    stars4import,
    others_params4import,
    tables_in_config,
):
    config = Configuration(tables=tables_in_config)
    conn = db_connection_with_only_schema_for_modif

    last_import_id = get_last_id_for_fake_insert(conn, table_import)
    last_star_id = get_last_id_for_fake_insert(conn, table_stars)
    last_planet_id = get_last_id_for_fake_insert(conn, table_planets)
    last_alter_star_id = get_last_id_for_fake_insert(conn, table_alternate_star_name)
    get_last_id_for_fake_insert(conn, table_alternate_planet_name)

    result = _import_one_system(
        conn,
        last_import_id,
        [stars4import, PLANETS_FOR_ONE, others_params4import],
        0,
        config,
    )

    assert result == (
        last_star_id + 1,
        [last_planet_id + 1, last_planet_id + 2],
        [last_alter_star_id + 1, last_alter_star_id + 2],
        [],
    )


def test_inport_planet2star_one_system(
    db_connection_with_data_for_modif,
    table_import,
    table_planet2star,
    stars4import,
    others_params4import,
    tables_in_config,
):
    print("ICI")
    config = Configuration(tables=tables_in_config)
    conn = db_connection_with_data_for_modif

    last_import_id = get_last_id_for_fake_insert(conn, table_import)
    last_planet2star_id = get_last_id_for_fake_insert(conn, table_planet2star)

    # HACK: for modif star4import to avoid an exception
    stars4import[0]["ra"] = Decimal("125")
    _import_one_system(
        conn,
        last_import_id,
        [stars4import, PLANETS_FOR_ONE, others_params4import],
        0,
        config,
    )

    check = (
        SQLEnumerable(conn, table_planet2star)
        .select()
        .where(lambda x: x.id > last_planet2star_id)
        .execute()
    )

    with soft_assertions():
        for idx, elem in enumerate(check.to_list(), start=1):
            assert_that(elem.id).is_equal_to(last_planet2star_id + idx)


def test_import_systems(
    db_connection_with_only_schema_for_modif,
    table_import,
    stars4import,
    planets4import,
    others_params4import,
    tables_in_config,
):
    config = Configuration(tables=tables_in_config)
    conn = db_connection_with_only_schema_for_modif

    import_data = {
        "external_csv": Path().cwd()
        / Path("tests/input_files/csv_files/tess_2_obj.csv"),
        "mapping_yaml": Path().cwd()
        / Path("tests/input_files/mapping_files/mapping_tess.yml"),
        "internal_csv": Path().cwd()
        / Path("tests/input_files/expected_files/internal_from_2_obj.csv"),
    }

    result = import_systems(
        conn,
        import_data,
        [stars4import, planets4import, others_params4import],
        config,
    )

    last_import_id = (
        (
            SQLEnumerable(conn, table_import)
            .select(lambda x: x.id)
            .take_last(1)
            .execute()
        )
        .first()
        .id
    )

    assert result == (
        last_import_id,
        [
            ImportSystemRecord(
                star_id=8,
                planets_id=[4, 5],
                star_alter_id=[4, 5],
                planets_alter_id=[[2, 3]],
            ),
            ImportSystemRecord(
                star_id=9,
                planets_id=[6],
                star_alter_id=[6, 7],
                planets_alter_id=[[4]],
            ),
            ImportSystemRecord(
                star_id=10,
                planets_id=[7],
                star_alter_id=[8, 9],
                planets_alter_id=[],
            ),
            ImportSystemRecord(
                star_id=11,
                planets_id=[8],
                star_alter_id=[10, 11],
                planets_alter_id=[],
            ),
        ],
    )


@pytest.mark.parametrize(
    "name, ra, dec",
    [
        param("test-1", Decimal("1"), Decimal("2"), id="same name"),
        param(
            "Aurelia_war_leader",
            Decimal("122.5804650"),
            Decimal("-5.5138520"),
            id="same pos",
        ),
    ],
)
def test_import_systems_star_already_in_db(
    db_connection_with_data_for_modif,
    planets4import,
    others_params4import,
    name,
    ra,
    dec,
    tables_in_config,
):
    config = Configuration(tables=tables_in_config)
    conn = db_connection_with_data_for_modif

    import_data = {
        "external_csv": Path().cwd()
        / Path("tests/input_files/csv_files/tess_2_obj.csv"),
        "mapping_yaml": Path().cwd()
        / Path("tests/input_files/mapping_files/mapping_tess.yml"),
        "internal_csv": Path().cwd()
        / Path("tests/input_files/expected_files/internal_from_2_obj.csv"),
    }

    stars = [
        {
            "star_name": name,
            "ra": ra,
            "dec": dec,
            "status": Decimal("1"),
        }
    ]

    with pytest.raises(StarAlreadyInDbError):
        import_systems(
            conn,
            import_data,
            [stars, planets4import, others_params4import],
            config,
        )


def test_import_systems_planet_already_in_db(
    db_connection_with_data_for_modif,
    stars4import,
    others_params4import,
    tables_in_config,
):
    config = Configuration(tables=tables_in_config)
    conn = db_connection_with_data_for_modif

    import_data = {
        "external_csv": Path().cwd()
        / Path("tests/input_files/csv_files/tess_2_obj.csv"),
        "mapping_yaml": Path().cwd()
        / Path("tests/input_files/mapping_files/mapping_tess.yml"),
        "internal_csv": Path().cwd()
        / Path("tests/input_files/expected_files/internal_from_2_obj.csv"),
    }

    stars = [
        {
            "star_name": "toto",
            "ra": Decimal("1"),
            "dec": Decimal("2"),
            "status": Decimal("1"),
        }
    ]

    planets = [
        {
            "name": "test-1.01",
            "publication_status": Decimal("6"),
            "detection_type": [Decimal("3")],
            "status": Decimal("1"),
        }
    ]

    with pytest.raises(PlanetAlreadyInDbError):
        import_systems(
            conn,
            import_data,
            [stars, planets, others_params4import],
            config,
        )


def test_rogue_planet_exception_not_implemented():
    with pytest.raises(NotImplementedError):
        import_rogue_planets()
