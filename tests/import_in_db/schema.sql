-- Create tables

-- Create table app_import
CREATE TABLE app_import (
    id serial NOT NULL PRIMARY KEY,
    name VARCHAR(100) UNIQUE NOT NULL,
    created timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    external_csv BYTEA,
    mapping_yaml BYTEA,
    internal_csv BYTEA,
    exoimport_version varchar(80) NOT NULL
);


-- Create table app_star
CREATE TABLE app_star(
    id serial NOT NULL PRIMARY KEY,
    name VARCHAR(60) UNIQUE NOT NULL,
    distance double precision,
    distance_error double precision[],
    spec_type VARCHAR(15),
    magnitude_v double precision,
    magnitude_i double precision,
    magnitude_h double precision,
    magnitude_j double precision,
    magnitude_k double precision,
    mass double precision,
    mass_error double precision[],
    age double precision,
    age_error double precision[],
    teff double precision,
    teff_error double precision[],
    radius double precision,
    radius_error double precision[],
    metallicity double precision,
    metallicity_error double precision[],
    ra double precision NOT NULL,
    dec double precision NOT NULL,
    remarks text,
    other_web text,
    url_simbad VARCHAR(255),
    radvel_proj double precision,
    created timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    status smallint NOT NULL,
    magnetic_field boolean,
    detected_disc smallint,
    dbimport_id integer references app_import(id)
);

-- Create table app_alternate_star_name
CREATE TABLE app_alternate_star_name(
    id serial NOT NULL PRIMARY KEY,
    star_id integer references app_star(id),
    name VARCHAR(60) UNIQUE NOT NULL
);

-- Create table app_planet
CREATE TABLE app_planet (
    id serial NOT NULL PRIMARY KEY,
    name VARCHAR(100),
    detection_type integer[] NOT NULL,
    publication_status smallint NOT NULL,
    axis double precision,
    axis_error double precision[],
    period double precision,
    period_error double precision[],
    eccentricity double precision,
    eccentricity_error double precision[],
    omega double precision,
    omega_error double precision[],
    tzero_tr double precision,
    tzero_tr_error double precision[],
    tzero_vr double precision,
    tzero_vr_error double precision[],
    tperi double precision,
    tperi_error double precision[],
    tconj double precision,
    tconj_error double precision[],
    inclination double precision,
    inclination_error double precision[],
    discovered smallint,
    remarks text,
    other_web text,
    detect_mode VARCHAR(255),
    angular_distance double precision,
    temp_calculated double precision,
    temp_measured double precision,
    hot_point_lon double precision,
    log_g double precision,
    created timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    status smallint NOT NULL,
    tzero_tr_sec double precision,
    tzero_tr_sec_error double precision[],
    lambda_angle double precision,
    lambda_angle_error double precision[],
    albedo double precision,
    albedo_error double precision[],
    mass_detection_type smallint,
    radius_detection_type smallint,
    impact_parameter double precision,
    impact_parameter_error  double precision[],
    K double precision,
    K_error double precision[],
    planet_status smallint,
    temp_calculated_error double precision[],
    main_star_id integer references app_star(id),
    mass_sini_unit smallint,
    mass_sini_string VARCHAR,
    mass_detected_unit smallint,
    mass_detected_string VARCHAR,
    mass_sini_error_string VARCHAR[],
    mass_detected_error_string VARCHAR[],
    radius_unit smallint,
    radius_string VARCHAR,
    radius_error_string VARCHAR[],
    planetary_system_id integer,
    dbimport_id integer references app_import(id)
);

-- Create table app_alternate_planet_name
CREATE TABLE app_alternate_planet_name(
    id serial NOT NULL PRIMARY KEY,
    planet_id integer references app_planet(id),
    name VARCHAR(60) UNIQUE NOT NULL
);

-- Create table app_planetary_system
CREATE TABLE app_planetary_system(
    id serial NOT NULL PRIMARY KEY,
    ra double precision NOT NULL,
    dec double precision NOT NULL,
    distance double precision,
    distance_error double precision[],
    created timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    dbimport_id integer references app_import(id)
);

-- Create table app_planet2star
CREATE TABLE app_planet2star(
    id serial NOT NULL PRIMARY KEY,
    planet_id integer NOT NULL references app_planet(id),
    star_id integer NOT NULL references app_star(id),
    system_id integer references app_planetary_system(id),
    UNIQUE (planet_id, star_id)
);

-- Create table app_publication
CREATE TABLE app_publication(
    id serial NOT NULL PRIMARY KEY,
    title text NOT NULL,
    author text,
    date VARCHAR(10) NOT NULL,
    -- type of publication refereed, ...
    type smallint NOT NULL,
    journal_name VARCHAR(200),
    journal_volume VARCHAR(15),
    journal_page VARCHAR(15),
    bibcode VARCHAR(19),
    keywords VARCHAR(150),
    reference text,
    url text,
    updated date,
    created timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    -- Web status
    status smallint NOT NULL,
    doi VARCHAR[],
    dbimport_id integer references app_import(id)
);

-- Create table app_planet2publication
CREATE TABLE app_planet2publication(
    id serial NOT NULL PRIMARY KEY,
    publication_id integer NOT NULL references app_publication(id),
    planet_id integer NOT NULL references app_planet(id),
    parameter smallint NOT NULL,
    created timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    -- Web status
    status smallint NOT NULL
);

-- Create Table app_star2publication
CREATE TABLE app_star2publication(
    id serial NOT NULL PRIMARY KEY,
    publication_id integer NOT NULL references app_publication(id),
    star_id integer NOT NULL references app_star(id),
    parameter smallint NOT NULL,
    created timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    -- Web status
    status smallint NOT NULL
);

-- Create function for on update
CREATE OR REPLACE FUNCTION update_modified_column()
RETURNS TRIGGER AS $$
BEGIN
    NEW.modified := now();
    RETURN NEW;
END;
$$ language 'plpgsql';

-- Create Triggers

-- Trigger for app_star
CREATE TRIGGER update_app_star_modified_column BEFORE UPDATE ON app_star
FOR EACH ROW EXECUTE PROCEDURE update_modified_column();

-- Trigger for app_planet
CREATE TRIGGER update_app_planet_modified_column BEFORE UPDATE ON app_planet
FOR EACH ROW EXECUTE PROCEDURE update_modified_column();

-- Trigger for app_planetary_system
CREATE TRIGGER update_app_planetary_system_modified_column BEFORE UPDATE ON app_planetary_system
FOR EACH ROW EXECUTE PROCEDURE update_modified_column();

-- Trigger for app_publication
CREATE TRIGGER update_app_publication_modified_column BEFORE UPDATE ON app_publication
FOR EACH ROW EXECUTE PROCEDURE update_modified_column();

-- Trigger for app_planet2publication
CREATE TRIGGER update_app_planet2publication_modified_column BEFORE UPDATE ON app_planet2publication
FOR EACH ROW EXECUTE PROCEDURE update_modified_column();

-- Trigger for app_star2publication
CREATE TRIGGER update_app_star2publication_modified_column BEFORE UPDATE ON app_star2publication
FOR EACH ROW EXECUTE PROCEDURE update_modified_column();
