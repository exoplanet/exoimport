# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that, soft_assertions
from exochoices import Plan2PubParam, Star2PubParam

# First party imports
from exoimport.config import Configuration, Tables
from exoimport.import_in_db.import_link_publication import (
    _link_to_object,
    _link_to_planet,
    _link_to_system,
    link_to_ids,
)

# Local imports
from ..conftest import get_last_id_for_fake_insert, tables_in_config_as_funct

CONFIG = Configuration(
    epsilon=1,
    web_statuses="active",
    tables=tables_in_config_as_funct(),
)


@pytest.mark.parametrize(
    "table_name, obj2pub_param",
    [
        param("app_star2publication", Star2PubParam.GENERIC_PUB, id="for star"),
        param("app_planet2publication", Plan2PubParam.GENERIC_PUB, id="for planet"),
    ],
)
def test_import_link_to_object(
    db_connection_with_data_for_modif,
    table_name,
    obj2pub_param,
):
    conn = db_connection_with_data_for_modif
    last_pub_param_id = get_last_id_for_fake_insert(conn, table_name)

    record = _link_to_object(conn, 1, 1, table_name, obj2pub_param, CONFIG.tables)

    assert record[0] == last_pub_param_id + 1


def test_import_link_to_planet(
    db_connection_with_data_for_modif,
    table_planet2publication,
):
    conn, table_name = db_connection_with_data_for_modif, table_planet2publication
    last_pub_param_id = get_last_id_for_fake_insert(conn, table_name)

    record = _link_to_planet(conn, 1, 1, CONFIG.tables)

    with soft_assertions():
        for last_id, value in enumerate(record.values(), start=last_pub_param_id):
            assert_that(value).is_greater_than(last_id)


def test_import_link_to_star(
    db_connection_with_data_for_modif,
    table_star2publication,
):
    conn, table_name = db_connection_with_data_for_modif, table_star2publication
    last_pub_param_id = get_last_id_for_fake_insert(conn, table_name)

    record = _link_to_planet(conn, 1, 1, CONFIG.tables)

    with soft_assertions():
        for last_id, value in enumerate(record.values(), start=last_pub_param_id):
            assert_that(value).is_greater_than(last_id)


def test_import_link_system(
    db_connection_with_data_for_modif,
    table_star2publication,
    table_planet2publication,
):
    conn, table_planet2pub, table_star2pub = (
        db_connection_with_data_for_modif,
        table_planet2publication,
        table_star2publication,
    )

    last_planet_pub_param_id = get_last_id_for_fake_insert(conn, table_planet2pub)
    last_star_pub_param_id = get_last_id_for_fake_insert(conn, table_star2pub)

    system = (1, [1, 2, 3])

    record_star, record_planets = _link_to_system(conn, system, 1, CONFIG)

    with soft_assertions():
        for last_id, value in enumerate(
            record_star.values(),
            start=last_star_pub_param_id,
        ):
            assert_that(value).is_greater_than(last_id)

        for planet in record_planets:
            for last_id, value in enumerate(
                planet.values(),
                start=last_planet_pub_param_id,
            ):
                assert_that(value).is_greater_than(last_id)
            last_planet_pub_param_id = last_id + 1


def test_import_link_ids(
    db_connection_with_data_for_modif,
    table_star2publication,
    table_planet2publication,
):
    conn, table_planet2pub, table_star2pub = (
        db_connection_with_data_for_modif,
        table_planet2publication,
        table_star2publication,
    )

    last_planet_pub_param_id = get_last_id_for_fake_insert(conn, table_planet2pub)
    last_star_pub_param_id = get_last_id_for_fake_insert(conn, table_star2pub)

    systems = [(1, [1, 2]), (2, [3])]

    record = link_to_ids(conn, systems, 1, CONFIG)

    with soft_assertions():
        for rec in record:
            record_star = rec[0]
            record_planets = rec[1]

            for last_id, value in enumerate(
                record_star.values(),
                start=last_star_pub_param_id,
            ):
                assert_that(value).is_greater_than(last_id)
            last_star_pub_param_id = last_id + 1

            for planet in record_planets:
                for last_id, value in enumerate(
                    planet.values(),
                    start=last_planet_pub_param_id,
                ):
                    assert_that(value).is_greater_than(last_id)
                last_planet_pub_param_id = last_id + 1
