-- SQL Commands to fill table for all import tests.

-- Insert in import table
INSERT INTO app_import(name, external_csv, mapping_yaml, internal_csv, exoimport_version)
VALUES ('test_import_001', '\\000content_of_external', '\\000content_of_mapping', '\\000content_of_internal', '1.1.0');

-- Insert in star table
INSERT INTO
app_star(name, ra, dec, distance, radius, teff, status, distance_error, radius_error,
teff_error, dbimport_id)
VALUES
('test-1', 122.5804650, -5.5138520, 295.8620000, 2.0100000, 7070.0000000, 1,
ARRAY[5.9100000, 5.9100000], ARRAY[0.0900000, 0.0900000], ARRAY[126.4000000,
126.4000000], 1),
('test-2', 112.752393, -4.463358, 283.291, 2.7, 6596.0, 1, ARRAY[3.0025, 3.0025],
ARRAY[0.13, 0.13], ARRAY[139.2, 139.2], 1);

-- Insert in alternate star name
INSERT INTO app_alternate_star_name(star_id, name) VALUES (1, 'test_star_alt_name');

-- Insert in planet table
INSERT INTO
app_planet(name, detection_type, publication_status, period, period_error, tzero_tr,
tzero_tr_error, temp_calculated, status, planet_status, main_star_id, radius_string,
radius_error_string, dbimport_id)
VALUES
('test-1.01', ARRAY[3], 6, 1.931671, ARRAY[7.7e-06, 7.7e-06], 2459250.064549,
ARRAY[0.001925, 0.001925], 3998.0, 1, 2, 1, '10.3168000',
ARRAY['3.2145900', '3.2145900'], 1),

('test-9.01', ARRAY[3], 6, 1.9600296, ARRAY[5e-06, 5e-06], 2459252.751357,
ARRAY[0.001632, 0.001632], 3555.0, 1,  2, 1, NULL, NULL, 1),

('test-7.01', ARRAY[3], 6, 6.9989206, ARRAY[1.37e-05, 1.37e-05], 2459247.930695,
ARRAY[0.0013, 0.0013], 1282.0, 1, 2, 1, '14.7752000', ARRAY['0.7647050', '0.7647050'],
1);

-- Insert in alternate planet name
INSERT INTO app_alternate_planet_name(planet_id, name) VALUES (1, 'test_pl_alt_name');

-- Insert in publication
INSERT INTO app_publication(title, date, type, url, status, dbimport_id)
VALUES
('Test-tess_1', 2022, 1, '<a target= "_blank"  href="http://exoplanet.eu/">website</a>', 1, 1);

-- Insert in star2publicatiob
INSERT INTO app_star2publication(publication_id, star_id, parameter, status)
VALUES
(1, 1, 1, 1);

-- Insert in planet2publicatiob
INSERT INTO app_planet2publication(publication_id, planet_id, parameter, status)
VALUES
(1, 1, 1, 1);
