# Standard imports
import datetime
import zoneinfo
from decimal import Decimal
from pathlib import Path

# Third party imports
from assertpy import assert_that, soft_assertions
from py_linq_sql import SQLEnumerable

# First party imports
from exoimport import (
    Configuration,
    format_csv,
    format_planets,
    read_csv,
    split_star_planet_and_other_params,
)
from exoimport.config import Tables
from exoimport.format.format import format_to_import
from exoimport.import_in_db.import_planets import import_planets

# Local imports
from ..conftest import get_last_id_for_fake_insert


def test_import_planets(
    db_connection_with_data_for_modif,
    table_import,
    table_stars,
    table_planets,
    tables_in_config,
):

    conn = db_connection_with_data_for_modif

    last_import_id = get_last_id_for_fake_insert(conn, table_import)
    last_star_id = get_last_id_for_fake_insert(conn, table_stars)
    last_planet_id = get_last_id_for_fake_insert(conn, table_planets)
    config = Configuration()

    csv_content = read_csv(
        Path.cwd() / Path("tests/input_files/csv_files/csv_exoplanet_5_obj_imp.csv"),
    )
    formated_content = format_csv(csv_content)

    stars, planets, other = split_star_planet_and_other_params(formated_content)
    form_planets = format_planets(planets, config)

    data_to_import_planets = format_to_import(form_planets)

    id_imported_planet = import_planets(
        conn,
        Tables(tables_in_config),
        data_to_import_planets,
        import_id=last_import_id,
        main_star_id=last_star_id,
    )

    check = (
        SQLEnumerable(conn, table_planets)
        .select(lambda x: (x.name, x.period, x.detection_type, x.created, x.modified))
        .where(lambda x: x.id > last_planet_id)
        .execute()
    )

    exp_date = datetime.datetime(
        2023, 3, 3, 10, 19, 43, 17580, tzinfo=zoneinfo.ZoneInfo(key="Europe/Paris")
    )

    with soft_assertions():
        assert_that(id_imported_planet).is_equal_to(
            [
                last_planet_id + 1,
                last_planet_id + 2,
                last_planet_id + 3,
                last_planet_id + 4,
                last_planet_id + 5,
            ]
        )
        assert_that(check).contains_only(
            *[
                ("tess-1001.01", 1.931671, [Decimal("2")], exp_date, exp_date),
                ("tess-1007.01", 6.9989206, [Decimal("2")], exp_date, exp_date),
                ("tess-1009.01", 1.9600296, [Decimal("2")], exp_date, exp_date),
                ("tess-1011.01", 2.4696958, [Decimal("2")], exp_date, exp_date),
                ("tess-1012.01", 0.884182, [Decimal("2")], exp_date, exp_date),
            ]
        )
