# Pytest imports
import pytest
from pytest import param

# Standard imports
from pathlib import Path

# Third party imports
from assertpy import assert_that, soft_assertions
from py_linq_sql import SQLEnumerable

# First party imports
from exoimport.import_in_db.import_import import import_table_import

# Local imports
from ..conftest import get_last_id_for_fake_insert

TEST_PATH = Path("tests/input_files")


@pytest.mark.parametrize(
    "mapping_yaml, internal_csv, expected_record, expected_internal",
    [
        param(
            Path(TEST_PATH / "mapping_files/mapping_tess.yml"),
            Path(TEST_PATH / "expected_files/internal_from_2_obj.csv"),
            1,
            "0.001925",
            id="Not empty internal",
        ),
        param(
            Path(TEST_PATH / "mapping_files/base_mapping.yml"),
            Path(TEST_PATH / "expected_files/empty_internal.csv"),
            None,
            "",
            id="Empty internal",
        ),
    ],
)
def test_import_import(
    db_connection_with_only_schema_for_modif,
    table_import,
    mapping_yaml,
    internal_csv,
    expected_record,
    expected_internal,
):
    conn, table = db_connection_with_only_schema_for_modif, table_import

    record = import_table_import(
        conn,
        table,
        {
            "external_csv": Path().cwd() / TEST_PATH / "csv_files/tess_2_obj.csv",
            "mapping_yaml": Path().cwd() / mapping_yaml,
            "internal_csv": Path().cwd() / internal_csv,
        },
    )

    check = (
        SQLEnumerable(conn, table)
        .select(
            lambda x: (
                x.name,
                x.external_csv,
                x.mapping_yaml,
                x.internal_csv,
            )
        )
        .execute()
    )

    name = check[0][0]
    external = check[0][1]
    mapping = check[0][2]
    internal = check[0][3]

    with soft_assertions():
        assert_that(record).is_equal_to(expected_record)
        assert_that(external.decode("utf-8")).contains("3998.0000000")
        assert_that(mapping.decode("utf-8")).contains("# Mapping file for ExoImport")
        assert_that(internal.decode("utf-8")).contains(expected_internal)
        assert_that(name).is_equal_to("tess_2_obj")


def test_import_import_none_empty_table(
    db_connection_with_data_for_modif,
    table_import,
):
    conn, table = db_connection_with_data_for_modif, table_import

    last_import_id = get_last_id_for_fake_insert(conn, table)

    record = import_table_import(
        conn,
        table,
        {
            "external_csv": Path().cwd() / TEST_PATH / "csv_files/tess_2_obj.csv",
            "mapping_yaml": Path().cwd() / TEST_PATH / "mapping_files/mapping_tess.yml",
            "internal_csv": Path().cwd()
            / TEST_PATH
            / "expected_files/internal_from_2_obj.csv",
        },
    )

    assert record == last_import_id + 1


def test_import_without_input_file(
    db_connection_with_data_for_modif,
    table_import,
):
    con, table = db_connection_with_data_for_modif, table_import

    last_import_id = get_last_id_for_fake_insert(con, table)

    record = import_table_import(
        con,
        table,
        {
            "external_csv": None,
            "mapping_yaml": None,
            "internal_csv": Path.cwd()
            / "tests/input_files/expected_files/internal_from_2_obj.csv",
        },
    )

    check = (
        SQLEnumerable(con, table)
        .select(
            lambda x: (
                x.name,
                x.external_csv,
                x.mapping_yaml,
                x.internal_csv,
            )
        )
        .where(lambda x: x.id == last_import_id + 1)
        .execute()
    )
    print(check)
    name = check[0][0]
    external = check[0][1]

    with soft_assertions():
        assert_that(record).is_equal_to(last_import_id + 1)
        assert_that(external).is_equal_to(None)
        assert_that(name).contains("import_name_autogen")
        assert_that(len(name)).is_equal_to(50)
