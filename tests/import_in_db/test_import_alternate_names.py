# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that, soft_assertions
from py_linq_sql import SQLEnumerable

# First party imports
from exoimport.import_in_db.import_alternate_names import (
    _import_one_planet_alternate_names,
    import_planets_alternate_name,
    import_star_alternate_names,
)

# Local imports
from ..conftest import get_last_id_for_fake_insert


@pytest.mark.parametrize(
    "alter_names, expected_alter_name, expected_record",
    [
        param(
            "tess-1001;Tess_1001",  # alter_names
            [("tess-1001",), ("Tess_1001",)],  # expected_alter_name
            lambda last_id: [last_id + 1, last_id + 2],  # expected_record
            id="1 alternate name",
        ),
        param(
            "tess_1001",  # alter_names
            [("tess_1001",)],  # expected_alter_name
            lambda last_id: [last_id + 1],  # expected_record
            id="more then 1 alter name",
        ),
    ],
)
def test_import_star_alternate_names(
    db_connection_with_data_for_modif,
    table_alternate_star_name,
    alter_names,
    expected_alter_name,
    expected_record,
):
    conn, table_name = db_connection_with_data_for_modif, table_alternate_star_name

    last_alter_id = get_last_id_for_fake_insert(conn, table_name)

    record = import_star_alternate_names(
        conn,
        1,
        alter_names,
        table_name,
    )

    check = (
        SQLEnumerable(conn, table_name)
        .select(lambda x: x.name)
        .where(lambda x: (x.star_id == 1) & (x.id > last_alter_id))
        .execute()
    )

    with soft_assertions():
        assert_that(record).is_equal_to(expected_record(last_alter_id))
        assert_that(check).contains_only(*expected_alter_name)


@pytest.mark.parametrize(
    "alter_names, expected_alter_name, expected_record",
    [
        param(
            "tess-1001.01;Tess_1001.01",  # alter_names
            [("tess-1001.01",), ("Tess_1001.01",)],  # expected_alter_name
            lambda last_id: [last_id + 1, last_id + 2],  # expected_record
            id="1 alternate name",
        ),
        param(
            "tess_1001.01",  # alter_names
            [("tess_1001.01",)],  # expected_alter_name
            lambda last_id: [last_id + 1],  # expected_record
            id="more then 1 alter name",
        ),
    ],
)
def test_import_one_planet_alternate_names(
    db_connection_with_data_for_modif,
    table_alternate_planet_name,
    alter_names,
    expected_alter_name,
    expected_record,
):
    conn, table_name = db_connection_with_data_for_modif, table_alternate_planet_name

    last_alter_id = get_last_id_for_fake_insert(conn, table_name)

    record = _import_one_planet_alternate_names(conn, 1, alter_names, table_name)

    check = (
        SQLEnumerable(conn, table_name)
        .select(lambda x: x.name)
        .where(lambda x: (x.planet_id == 1) & (x.id > last_alter_id))
        .execute()
    )

    with soft_assertions():
        assert_that(record).is_equal_to(expected_record(last_alter_id))
        assert_that(check).contains_only(*expected_alter_name)


def test_import_planets_alternate_names(
    db_connection_with_data_for_modif,
    table_planets,
    table_alternate_planet_name,
    others_params4import,
):
    conn = db_connection_with_data_for_modif

    last_alter_id = get_last_id_for_fake_insert(conn, table_alternate_planet_name)

    record = import_planets_alternate_name(
        conn,
        [1, 2, 3],
        others_params4import,
        table_planets,
        table_alternate_planet_name,
    )

    check = (
        SQLEnumerable(conn, table_alternate_planet_name)
        .select(lambda x: x.name)
        .where(lambda x: x.id > last_alter_id)
        .execute()
    )

    with soft_assertions():
        assert_that(record).is_equal_to(
            [
                [last_alter_id + 1, last_alter_id + 2],
                [last_alter_id + 3],
            ],
        )
        assert_that(check).contains_only(
            *[
                ("tess-1001.01",),
                ("Tess_1001.01",),
                ("Tess_1007.01",),
            ],
        )
