# Pytest imports
import pytest
from pytest import param

# Standard imports
import datetime
import zoneinfo

# Third party imports
from assertpy import assert_that, soft_assertions
from py_linq_sql import SQLEnumerable

# First party imports
from exoimport import Configuration, import_and_link_publication
from exoimport.import_in_db.import_publication import import_publication
from exoimport.import_in_db.import_system import _import_publication_in_db

# Local imports
from ..conftest import get_last_id_for_fake_insert


@pytest.mark.parametrize(
    "data, expected_record, expected_data",
    [
        param(
            [
                (
                    "Tess_1",
                    2022,
                    1,
                    '<a target= "_blank"  href="http://toto.com/">website</a>',
                    1,
                    "2023-03-03 10:19:43.017580",
                    "2023-03-03 10:19:43.017580",
                )
            ],  # data
            lambda last_id: [last_id + 1],  # expected_record
            [
                (
                    "Tess_1",
                    '<a target= "_blank"  href="http://toto.com/">website</a>',
                    datetime.datetime(
                        2023,
                        3,
                        3,
                        10,
                        19,
                        43,
                        17580,
                        tzinfo=zoneinfo.ZoneInfo(key="Europe/Paris"),
                    ),
                    datetime.datetime(
                        2023,
                        3,
                        3,
                        10,
                        19,
                        43,
                        17580,
                        tzinfo=zoneinfo.ZoneInfo(key="Europe/Paris"),
                    ),
                ),
            ],  # expected_data
            id="one pub",
        ),
        param(
            [
                (
                    "Tess_2",
                    2022,
                    1,
                    '<a target= "_blank"  href="http://titi.com/">website</a>',
                    1,
                    "2023-03-03 10:19:43.017580",
                    "2023-03-03 10:19:43.017580",
                ),
                (
                    "Tess_3",
                    2022,
                    1,
                    '<a target= "_blank"  href="http://tata.com/">website</a>',
                    1,
                    "2023-03-03 10:19:43.017580",
                    "2023-03-03 10:19:43.017580",
                ),
            ],  # data
            lambda last_id: [last_id + 1, last_id + 2],  # expected_record
            [
                (
                    "Tess_2",
                    '<a target= "_blank"  href="http://titi.com/">website</a>',
                    datetime.datetime(
                        2023,
                        3,
                        3,
                        10,
                        19,
                        43,
                        17580,
                        tzinfo=zoneinfo.ZoneInfo(key="Europe/Paris"),
                    ),
                    datetime.datetime(
                        2023,
                        3,
                        3,
                        10,
                        19,
                        43,
                        17580,
                        tzinfo=zoneinfo.ZoneInfo(key="Europe/Paris"),
                    ),
                ),
                (
                    "Tess_3",
                    '<a target= "_blank"  href="http://tata.com/">website</a>',
                    datetime.datetime(
                        2023,
                        3,
                        3,
                        10,
                        19,
                        43,
                        17580,
                        tzinfo=zoneinfo.ZoneInfo(key="Europe/Paris"),
                    ),
                    datetime.datetime(
                        2023,
                        3,
                        3,
                        10,
                        19,
                        43,
                        17580,
                        tzinfo=zoneinfo.ZoneInfo(key="Europe/Paris"),
                    ),
                ),
            ],  # expected_data
            id="many pub",
        ),
    ],
)
def test_import_publication_empty_table(
    db_connection_with_only_schema_for_modif,
    table_publication,
    data,
    expected_record,
    expected_data,
):
    conn, table = db_connection_with_only_schema_for_modif, table_publication

    import_id = get_last_id_for_fake_insert(conn, "app_import")

    last_pub_id = get_last_id_for_fake_insert(conn, table, import_id)

    publication = (
        ["title", "date", "type", "url", "status", "created", "modified"],
        data,
    )

    record = import_publication(conn, table, publication, import_id=import_id)

    check = (
        SQLEnumerable(conn, table)
        .select(lambda x: (x.title, x.url, x.created, x.modified))
        .where(lambda x: x.id > last_pub_id)
        .execute()
    )

    with soft_assertions():
        assert_that(record).is_equal_to(expected_record(last_pub_id))
        for check_elem, expect_elem in zip(check, expected_data):
            assert_that(list(check_elem)[:-2]).contains_only(*list(expect_elem)[:-2])
            assert_that(list(check_elem)[-2:]).is_equal_to(list(expect_elem)[-2:])


def test_import_publication_non_empty_table(
    db_connection_with_data_for_modif,
    table_publication,
):
    conn, table = db_connection_with_data_for_modif, table_publication

    last_pub_id = get_last_id_for_fake_insert(conn, table)

    publication = (
        ["title", "date", "type", "url", "status"],
        [
            (
                "Tess",
                2022,
                1,
                '<a target= "_blank"  href="http://toto.com/">website</a>',
                1,
            ),
        ],
    )

    record = import_publication(conn, table, publication, import_id=1)

    assert record[0] == last_pub_id + 1


def test_import_publication_in_db(
    db_connection_with_data_for_modif,
    table_publication,
    others_params4import,
    tables_in_config,
):
    conn, table = db_connection_with_data_for_modif, table_publication

    last_pub_id = get_last_id_for_fake_insert(conn, table)

    record = _import_publication_in_db(
        conn,
        1,
        others_params4import,
        Configuration(tables=tables_in_config),
    )

    assert record == last_pub_id + 1


def test_import_and_link_publication(
    db_connection_with_data_for_modif,
    table_publication,
    table_planet2publication,
    table_star2publication,
    others_params4import,
    tables_in_config,
):
    conn, table_pub, table_star2pub, table_planet2pub = (
        db_connection_with_data_for_modif,
        table_publication,
        table_star2publication,
        table_planet2publication,
    )

    last_pub_id = get_last_id_for_fake_insert(conn, table_pub)
    last_planet_pub_param_id = get_last_id_for_fake_insert(conn, table_planet2pub)
    last_star_pub_param_id = get_last_id_for_fake_insert(conn, table_star2pub)
    imported_systems = (1, [(1, [1, 2]), (2, [3])])

    record = import_and_link_publication(
        conn,
        others_params4import,
        imported_systems,
        Configuration(tables=tables_in_config),
    )

    with soft_assertions():
        record_pub = record[0]
        record_links = record[1]
        assert_that(record_pub).is_equal_to(last_pub_id + 1)

        for rec in record_links:
            record_star = rec[0]
            record_planets = rec[1]

            for last_id, value in enumerate(
                record_star.values(),
                start=last_star_pub_param_id,
            ):
                assert_that(value).is_greater_than(last_id)
            last_star_pub_param_id = last_id + 1

            for planet in record_planets:
                for last_id, value in enumerate(
                    planet.values(),
                    start=last_planet_pub_param_id,
                ):
                    assert_that(value).is_greater_than(last_id)
                last_planet_pub_param_id = last_id + 1
