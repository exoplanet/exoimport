"""All functions used in tests with db."""

# Pytest imports
import pytest
import pytest_postgresql  # noqa: F401
from pytest_postgresql.executor import PostgreSQLExecutor
from pytest_postgresql.janitor import DatabaseJanitor

# Standard imports
import logging
import os.path
import platform
import subprocess  # noqa: S404
from decimal import Decimal
from os import chdir, environ, getenv
from pathlib import Path
from tempfile import gettempdir, mkdtemp

# Third party imports
import psycopg
from dotenv import load_dotenv
from psycopg import Connection
from psycopg.conninfo import make_conninfo
from psycopg_pool import ConnectionPool
from py_linq_sql import SQLEnumerable

# First party imports
from exoimport import CsvDictData

# Local imports
from .utils.insert_and_get_id import (
    insert_in_alter_planet_and_get_id,
    insert_in_alter_star_and_get_id,
    insert_in_import_and_get_id,
    insert_in_planet2publication_and_get_id,
    insert_in_planet2star_and_get_id,
    insert_in_planet_and_get_id,
    insert_in_publication_and_get_id,
    insert_in_star2publication_and_get_id,
    insert_in_star_and_get_id,
)
from .utils.planets_star_for_systems import (
    get_planets_for_systems,
    get_stars_for_systems,
)

# Don't change the order of table, it's important for foreign key
TABLE_NAMES = [
    "core_planet2star",
    "core_star2publication",
    "core_planet2publication",
    "core_publication",
    "core_atmospheremolecule",
    "core_molecule",
    "core_alternateplanetname",
    "core_alternatestarname",
    "core_planet",
    "core_planetarysystem",
    "core_star",
    "core_import",
]


@pytest.fixture(scope="session")
def db_password() -> str:
    """Return a stupid password for the temporary database created during the tests."""
    return "dummypassword"


@pytest.fixture(scope="session")
def table_import() -> str:
    """Name of the table to store imports."""
    return "app_import"


@pytest.fixture(scope="session")
def table_stars() -> str:
    """Name of the table to store stars."""
    return "app_star"


@pytest.fixture(scope="session")
def table_planets() -> str:
    """Name of the table to store planets."""
    return "app_planet"


@pytest.fixture(scope="session")
def table_alternate_star_name() -> str:
    """Name of the table to store star's alternate names."""
    return "app_alternate_star_name"


@pytest.fixture(scope="session")
def table_alternate_planet_name() -> str:
    """Name of the table to store planet's alternate names."""
    return "app_alternate_planet_name"


@pytest.fixture(scope="session")
def table_publication() -> str:
    """Name of the table to store publications."""
    return "app_publication"


@pytest.fixture(scope="session")
def table_star2publication() -> str:
    """Name of the table to store link between star and publication."""
    return "app_star2publication"


@pytest.fixture(scope="session")
def table_planet2publication() -> str:
    """Name of the table to store link between planet and publication."""
    return "app_planet2publication"


@pytest.fixture(scope="session")
def table_planet2star() -> str:
    """Name of the table to store link between planet and star."""
    return "app_planet2star"


@pytest.fixture(scope="session")
def db_empty(postgresql_proc: PostgreSQLExecutor, db_password: str) -> ConnectionPool:
    """
    Return a temporary empty database name shared during the whole test session.

    Args:
        - postgresql_proc: a postgreSQL process to create a database
        - db_password: dummy password to create a database

    Returns:
        A connection pool to connect to the database
    """
    TEST_DB_NAME = "exoimport_for_sql_test_db_empty_rel"

    conninfo = make_conninfo(
        dbname=TEST_DB_NAME,
        user=postgresql_proc.user,
        password=db_password,
        host=postgresql_proc.host,
        port=postgresql_proc.port,
    )

    with DatabaseJanitor(
        postgresql_proc.user,
        postgresql_proc.host,
        postgresql_proc.port,
        TEST_DB_NAME,
        postgresql_proc.version,
        password=db_password,
    ):
        with ConnectionPool(conninfo) as pool:
            # Uncomment if something when wrong because it will fail now and not later
            # when the pool is used
            # pool.wait()
            yield pool


@pytest.fixture
def db_connection_empty(db_empty: ConnectionPool) -> psycopg.Connection:
    """
    Return a function scoped connection to the temporary empty database.

    Args:
        - db_empty: connection pool to get a connection to an empty db

    Returns:
        A connection to the database
    """
    with db_empty.connection() as conn:
        yield conn


def _load_schema(db: ConnectionPool) -> None:
    """
    Fill a database with a basic db schema.

    Args:
        - db: connection pool to get a connection to the desired db
    """
    # Read the SQL file contains the schema of the database.
    with open(Path.cwd() / Path("tests/import_in_db/schema.sql"), "rb") as sql_file:
        schema = sql_file.read()
        with db.connection() as conn:
            with conn.cursor() as cursor:
                cursor.execute(schema)
                conn.commit()


@pytest.fixture(scope="session")
def db_with_only_schema(
    postgresql_proc: PostgreSQLExecutor, db_password: str
) -> ConnectionPool:
    """
    Return a temporary empty db with a db schema shared during the whole test session.

    Args:
        - postgresql_proc: a postgreSQL process to create a database
        - db_password: dummy password to create a database

    Returns:
        A connection pool to connect to the database
    """
    TEST_DB_NAME = "exoimport_for_sql_test_db_with_schema"

    conninfo = make_conninfo(
        dbname=TEST_DB_NAME,
        user=postgresql_proc.user,
        password=db_password,
        host=postgresql_proc.host,
        port=postgresql_proc.port,
    )

    with DatabaseJanitor(
        postgresql_proc.user,
        postgresql_proc.host,
        postgresql_proc.port,
        TEST_DB_NAME,
        postgresql_proc.version,
        password=db_password,
    ):
        with ConnectionPool(conninfo) as pool:
            # Uncomment if something when wrong because it will fail now and not later
            # when the pool is used
            # pool.wait()
            _load_schema(pool)
            yield pool


def _load_data(db: ConnectionPool) -> None:
    """
    Fill a database with some data for tests.

    Args:
        - db: connection pool to get a connection to the desired db
    """
    # Read the SQL file contains the schema of the database.
    with open(Path.cwd() / Path("tests/import_in_db/load_data.sql"), "rb") as sql_file:
        data = sql_file.read()
        with db.connection() as conn:
            with conn.cursor() as cursor:
                cursor.execute(data)
                conn.commit


@pytest.fixture(scope="session")
def db_with_data(
    postgresql_proc: PostgreSQLExecutor, db_password: str
) -> ConnectionPool:
    """
    Return a temporary db with schema and data shared during the whole test session.

    Args:
        - postgresql_proc: a postgreSQL process to create a database
        - db_password: dummy password to create a database

    Returns:
        A connection pool to connect to the database
    """
    TEST_DB_NAME = "exoimport_for_sql_test_db_with_data"

    conninfo = make_conninfo(
        dbname=TEST_DB_NAME,
        user=postgresql_proc.user,
        password=db_password,
        host=postgresql_proc.host,
        port=postgresql_proc.port,
    )

    with DatabaseJanitor(
        postgresql_proc.user,
        postgresql_proc.host,
        postgresql_proc.port,
        TEST_DB_NAME,
        postgresql_proc.version,
        password=db_password,
    ):
        with ConnectionPool(conninfo) as pool:
            # Uncomment if something when wrong because it will fail now and not later
            # when the pool is used
            # pool.wait()
            _load_schema(pool)
            _load_data(pool)
            yield pool


@pytest.fixture(scope="function")
def db_connection_with_only_schema_for_modif(
    db_with_only_schema: ConnectionPool,
) -> psycopg.Connection:
    """
    Return a function scoped connection to the temporary empty db with a db schema.

    Suitable for modification tests since all modification will be rolled back at the
    end of the test function.

    Args:
        - db_with_only_schema: a connection pool to the database

    Returns:
        A connection to the database
    """
    with db_with_only_schema.connection() as conn:
        yield conn
        conn.rollback()


@pytest.fixture(scope="function")
def db_connection_with_data_for_modif(
    db_with_data: ConnectionPool,
) -> psycopg.Connection:
    """
    Return a function scoped connection to the temporary db with schema and data.

    Suitable for modification tests since All modification will be rolled back at the
    end of the test function.

    Args:
        - db_with_only_schema: a connection pool to the database

    Returns:
        A connection to the database
    """
    with db_with_data.connection() as conn:
        yield conn
        conn.rollback()


@pytest.fixture
def others_params4import() -> CsvDictData:
    """Get others parameters of an import for import a system(s)."""
    return [
        {
            "star_alternate_names": "tess-1001;Tess_1001",
            "alternate_names": "tess-1001.01;Tess_1001.01",
            "planet_name": "test-1.01",
            "publication_title": "tess",
            "publication_date": Decimal("2022"),
            "publication_url": "https://exoplanetarchive.ipac.caltech.edu/cgi-bin/TblView/nph-tblView?app=ExoTbls&config=TOI",
            "publication_type": "refereed article",
        },
        {
            "star_alternate_names": "tess-1007;Tess_1007",
            "alternate_names": "Tess_1007.01",
            "planet_name": "test-7.01",
            "publication_title": "tess",
            "publication_date": Decimal("2022"),
            "publication_url": "https://exoplanetarchive.ipac.caltech.edu/cgi-bin/TblView/nph-tblView?app=ExoTbls&config=TOI",
            "publication_type": "refereed article",
        },
        {
            "star_alternate_names": "tess-1001;Tess_1001",
            "alternate_names": None,
            "planet_name": "test-9.01",
            "publication_title": "tess",
            "publication_date": Decimal("2022"),
            "publication_url": "https://exoplanetarchive.ipac.caltech.edu/cgi-bin/TblView/nph-tblView?app=ExoTbls&config=TOI",
            "publication_type": "refereed article",
        },
        {
            "star_alternate_names": "tess-1011;Tess_1011",
            "alternate_names": None,
            "planet_name": "test-11.01",
            "publication_title": "tess",
            "publication_date": Decimal("2022"),
            "publication_url": "https://exoplanetarchive.ipac.caltech.edu/cgi-bin/TblView/nph-tblView?app=ExoTbls&config=TOI",
            "publication_type": "refereed article",
        },
        {
            "star_alternate_names": "tess-1012;Tess_1012",
            "alternate_names": None,
            "planet_name": "test-12.01",
            "publication_title": "tess",
            "publication_date": Decimal("2022"),
            "publication_url": "https://exoplanetarchive.ipac.caltech.edu/cgi-bin/TblView/nph-tblView?app=ExoTbls&config=TOI",
            "publication_type": "refereed article",
        },
    ]


@pytest.fixture
def planets4import() -> CsvDictData:
    """Get planets for import system(s)."""
    return get_planets_for_systems()


@pytest.fixture
def stars4import() -> CsvDictData:
    """Get stars for import system(s)."""
    return get_stars_for_systems()


def get_last_id_for_fake_insert(
    conn: Connection,
    table: str,
    import_id: int = 1,
) -> int | None:
    """
    Get the last id of an objects in a table of the database.

    We insert an object and get his id.

    Args:
        conn: Connection to the db.
        table: Name of the table.
        import_id: Id of the import. 1 by default.

    Returns:
        The last id of an objects in a table.
    """
    match table:
        case "app_import":
            return insert_in_import_and_get_id(conn, table).first().id
        case "app_star":
            return insert_in_star_and_get_id(conn, table).first().id
        case "app_planet":
            return insert_in_planet_and_get_id(conn, table).first().id
        case "app_alternate_star_name":
            return insert_in_alter_star_and_get_id(conn, table).first().id
        case "app_alternate_planet_name":
            return insert_in_alter_planet_and_get_id(conn, table).first().id
        case "app_publication":
            return insert_in_publication_and_get_id(conn, table, import_id).first().id
        case "app_star2publication":
            return insert_in_star2publication_and_get_id(conn, table).first().id
        case "app_planet2publication":
            return insert_in_planet2publication_and_get_id(conn, table).first().id
        case "app_planet2star":
            return insert_in_planet2star_and_get_id(conn, table).first().id
        case _:
            None


@pytest.fixture(autouse=True)
def disable_log(request):
    if "disable_autouse" in request.keywords:
        yield
    else:
        logging.disable(logging.CRITICAL)
        yield
        logging.disable(logging.NOTSET)


@pytest.fixture(scope="session")
def create_db_for_typer():
    """
    Get a temporary postgresql database.
    """
    load_dotenv()
    # If we are on the CI we keep the port give in the environment variable because if
    # we launch tests when the pipeline is running,
    # 2 process try to access the same service on the same port.
    # Otherwise, we set the port to 9000 to not access the service through the same
    # port of the program (5432 by default).
    if getenv("EXOIMPORT_CI") is None or getenv("EXOIMPORT_CI").lower() != "true":
        environ["EXOIMPORT_DBPORT"] = "9000"

    pg_load = ["./sql_empty_db.sql"]
    postgresql_ctl = "/usr/lib/postgresql/13/bin/pg_ctl"

    if not os.path.exists(postgresql_ctl):
        pg_bindir = subprocess.check_output(  # nosec B607 B603 # noqa: S603 S607
            ["pg_config", "--bindir"],
            universal_newlines=True,
        ).strip()
        postgresql_ctl = os.path.join(pg_bindir, "pg_ctl")

    tmpdir = mkdtemp()

    pg_port = int(getenv("EXOIMPORT_DBPORT"))
    datadir = f"{tmpdir}data-{pg_port}"
    logfile_path = f"{tmpdir}postgresql.{pg_port}.log"

    if platform.system() == "FreeBSD":
        with (datadir / "pg_hba.conf").open(mode="a") as conf_file:  # type: ignore
            conf_file.write("host all all 0.0.0.0/0 trust\n")

    psql_executor = PostgreSQLExecutor(
        executable=postgresql_ctl,
        host="localhost",
        port=pg_port,
        password=getenv("EXOIMPORT_DBPASSWORD"),
        dbname=getenv("EXOIMPORT_DBNAME"),
        datadir=str(datadir),
        unixsocketdir=gettempdir(),
        logfile=str(logfile_path),
        startparams="-w",
        user=getenv("EXOIMPORT_DBUSER"),
    )

    with psql_executor:
        psql_executor.wait_for_postgres()
        with DatabaseJanitor(
            user=psql_executor.user,
            host=psql_executor.host,
            port=psql_executor.port,
            dbname=psql_executor.dbname,
            version=psql_executor.version,
            password=psql_executor.password,
        ) as janitor:
            if pg_load:
                for load_element in pg_load:
                    janitor.load(load_element)  # type: ignore
            yield


@pytest.fixture(scope="function")
def db_for_typer(create_db_for_typer):
    pg_port = int(getenv("EXOIMPORT_DBPORT"))
    conn = psycopg.connect(
        user=getenv("EXOIMPORT_DBUSER"),
        password=getenv("EXOIMPORT_DBPASSWORD"),
        host="localhost",
        port=pg_port,
        dbname=getenv("EXOIMPORT_DBNAME"),
        autocommit=True,
    )

    # Kill all connection on the database exoimport to avoid that the tests
    # hang the base.
    conn.execute(
        "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE "
        f"""pg_stat_activity.datname = '{getenv("EXOIMPORT_DBNAME")}' AND pid <> """
        "pg_backend_pid()",
    )

    with conn.pipeline():
        for name in TABLE_NAMES:
            conn.execute(f"DELETE FROM {name}", prepare=True)
    conn.close()


@pytest.fixture(scope="function")
def db_for_typer_with_data(create_db_for_typer):
    """blabla"""
    pg_port = int(getenv("EXOIMPORT_DBPORT"))
    conn = psycopg.connect(
        user=getenv("EXOIMPORT_DBUSER"),
        password=getenv("EXOIMPORT_DBPASSWORD"),
        host="localhost",
        port=pg_port,
        dbname=getenv("EXOIMPORT_DBNAME"),
        autocommit=True,
    )
    SQLEnumerable(conn, "core_star").insert(
        ["name, ra, dec, status"],
        (
            "tess-1001",
            Decimal("122.5804650"),
            Decimal("-5.5138520"),
            Decimal("1"),
        ),
    ).execute()
    yield

    conn.execute(
        "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE "
        f"""pg_stat_activity.datname = '{getenv("EXOIMPORT_DBNAME")}' AND pid <> """
        "pg_backend_pid()",
    )

    with conn.pipeline():
        for name in TABLE_NAMES:
            conn.execute(f"DELETE FROM {name}", prepare=True)
    conn.close()


@pytest.fixture(scope="function")
def define_new_cwd(tmp_path):

    old_cwd = Path.cwd()
    new_cwd = tmp_path / "typer_genrate_config"
    new_cwd.mkdir()
    chdir(new_cwd)

    yield

    chdir(old_cwd)


def tables_in_config_as_funct():
    return {
        "import_table": "app_import",
        "star_table": "app_star",
        "alternate_star_name_table": "app_alternate_star_name",
        "planet_table": "app_planet",
        "alternate_planet_name_table": "app_alternate_planet_name",
        "molecule_table": "app_molecule",
        "atmoshpere_molecule_table": "app_atmoshpere_molecule",
        "publication_table": "app_publication",
        "planet2publication_table": "app_planet2publication",
        "star2publication_table": "app_star2publication",
        "planetary_system_table": "app_planetary_system",
        "planet2star_table": "app_planet2star",
    }


@pytest.fixture(scope="session")
def tables_in_config():
    return tables_in_config_as_funct()
