"""Functions to insert somethings and get his id to testing import system(s)."""

# Standard imports
from pathlib import Path
from typing import Dict

# Third party imports
import psycopg
from psycopg import Connection
from py_linq_sql import SQLEnumerable


def _insert_value_in_import(
    db_connection_with_only_schema_for_modif: Connection,
    table_import: str,
    data: Dict,
) -> None:
    """
    Insert a value in a empty database.

    Args:
        db_connection_with_only_schema_for_modif: Name of the database to connect.
        table_import: Table to insert.
        data : Value to insert in the db.
    """
    external_csv_content = open(data["external_csv"], "rb").read()
    mapping_yaml_content = open(data["mapping_yaml"], "rb").read()
    internal_csv_content = open(data["internal_csv"], "rb").read()

    cursor = db_connection_with_only_schema_for_modif.cursor()
    cursor.execute(
        f"INSERT INTO {table_import}(name, external_csv, mapping_yaml, internal_csv, exoimport_version) "
        "VALUES (%s, %s, %s, %s, %s)",
        (
            "test_import_002",
            psycopg.Binary(external_csv_content),
            psycopg.Binary(mapping_yaml_content),
            psycopg.Binary(internal_csv_content),
            "1.1.0",
        ),
    )


def insert_in_import_and_get_id(conn: Connection, table_import: str):
    """
    Insert an import in table 'app_import' and get his id.

    Args:
        conn: Connection to the db.
        table_import: Name of the table.

    Returns:
        Id of the imported import.
    """
    cwd = Path().cwd()
    import_data = dict(
        external_csv=cwd / "tests/input_files/csv_files/tess_2_obj.csv",
        mapping_yaml=cwd / "tests/input_files/mapping_files/mapping_tess.yml",
        internal_csv=cwd / "tests/input_files/expected_files/internal_from_2_obj.csv",
    )

    _insert_value_in_import(conn, table_import, import_data)

    return (
        SQLEnumerable(conn, table_import).select(lambda x: x.id).take_last(1).execute()
    )


def insert_in_star_and_get_id(conn: Connection, table_stars: str):
    """
    Insert a star in table 'app_star' and get his id.

    Args:
        conn: Connection to the db.
        table_stars: Name of the table.

    Returns:
        Id of the imported star.
    """
    SQLEnumerable(conn, table_stars).insert(
        ["name", "ra", "dec", "status"], ("fake_star", 0.1, 0.2, 1)
    ).execute()

    return (
        SQLEnumerable(conn, table_stars).select(lambda x: x.id).take_last(1).execute()
    )


def insert_in_planet_and_get_id(conn: Connection, table_planets: str):
    """
    Insert a planet in table 'app_planet' and get his id.

    Args:
        conn: Connection to the db.
        table_planets: Name of the table.

    Returns:
        Id of the imported planet.
    """
    SQLEnumerable(conn, table_planets).insert(
        ["detection_type", "publication_status", "status"], ([3], 1, 1)
    ).execute()

    return (
        SQLEnumerable(conn, table_planets).select(lambda x: x.id).take_last(1).execute()
    )


def insert_in_alter_star_and_get_id(conn: Connection, table_alternate_star_name: str):
    """
    Insert an alternate star name in table 'app_alternate_star_name' and get his id.

    Args:
        conn: Connection to the db.
        table_alternate_star_name: Name of the table.

    Returns:
        Id of the imported alternate name.
    """
    SQLEnumerable(conn, table_alternate_star_name).insert(
        ["name"], ("fake_star_alter_name")
    ).execute()

    return (
        SQLEnumerable(conn, table_alternate_star_name)
        .select(lambda x: x.id)
        .take_last(1)
        .execute()
    )


def insert_in_alter_planet_and_get_id(
    conn: Connection,
    table_alternate_planet_name: str,
):
    """
    Insert an alternate planet name in table 'app_alternate_planet_name' and get his id.

    Args:
        conn: Connection to the db.
        table_alternate_planet_name: Name of the table.

    Returns:
        Id of the imported alternate name.
    """
    SQLEnumerable(conn, table_alternate_planet_name).insert(
        ["name"], ("fake_planet_alter_name")
    ).execute()

    return (
        SQLEnumerable(conn, table_alternate_planet_name)
        .select(lambda x: x.id)
        .take_last(1)
        .execute()
    )


def insert_in_publication_and_get_id(
    conn: Connection,
    table_publication: str,
    import_id: int,
):
    """
    Insert a publication in table `app_publication` and get his id.

    Args:
        conn: Connection to the db.
        table_publication: Name of the table.
        import_id: Id of the import.

    Returns:
        Id of the imported alternate name.
    """
    SQLEnumerable(conn, table_publication).insert(
        ["title", "date", "type", "url", "status", "dbimport_id"],
        (
            "fake_publication",
            2022,
            1,
            '<a target= "_blank"  href="http://toto.com/">website</a>',
            1,
            import_id,
        ),
    ).execute()

    return (
        SQLEnumerable(conn, table_publication)
        .select(lambda x: x.id)
        .take_last(1)
        .execute()
    )


def insert_in_star2publication_and_get_id(
    conn: Connection,
    table_star2publication: str,
):
    """
    Insert a link between star and a pub in table `app_star2publication` and get his id.

    Args:
        conn: Connection to the db.
        table_star2publication: Name of the table.

    Returns:
        Id of the imported alternate name.
    """
    SQLEnumerable(conn, table_star2publication).insert(
        ["publication_id", "star_id", "parameter", "status"],
        (1, 1, 1, 1),
    ).execute()

    return (
        SQLEnumerable(conn, table_star2publication)
        .select(lambda x: x.id)
        .take_last(1)
        .execute()
    )


def insert_in_planet2publication_and_get_id(
    conn: Connection,
    table_planet2publication: str,
):
    """
    Insert a link between planet and a pub in table `app_planet2publication`.

    And get his id.

    Args:
        conn: Connection to the db.
        table_planet2publication: Name of the table.

    Returns:
        Id of the imported alternate name.
    """
    SQLEnumerable(conn, table_planet2publication).insert(
        ["publication_id", "planet_id", "parameter", "status"],
        (1, 1, 1, 1),
    ).execute()

    return (
        SQLEnumerable(conn, table_planet2publication)
        .select(lambda x: x.id)
        .take_last(1)
        .execute()
    )


def insert_in_planet2star_and_get_id(
    conn: Connection,
    table_planet2star: str,
):
    """
    Insert a link between planet and a star in table `app_planet2star`.

    And get his id.

    Args:
        conn: Connection to the db.
        table_planet2star: Name of the table.

    Returns:
        Id of the imported alternate name.
    """
    SQLEnumerable(conn, table_planet2star).insert(
        ["star_id", "planet_id"],
        (1, 1),
    ).execute()

    return (
        SQLEnumerable(conn, table_planet2star)
        .select(lambda x: x.id)
        .take_last(1)
        .execute()
    )
