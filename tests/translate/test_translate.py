# Pytest imports
import pytest
from pytest import param

# Standard imports
from decimal import Decimal
from pathlib import Path

# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from exoimport import (
    CsvData,
    PythonSyntaxError,
    format_csv,
    read_csv,
    read_yaml,
    translate,
)
from exoimport.translate import _dict_csv_to_exoplanet_dict_csv

TEST_PATH = "tests/input_files"


@pytest.mark.parametrize(
    "mapping, csv_rows, expected",
    [
        param(
            {"a": "toto", "b": "titi", "c": "tutu"},
            [
                {"toto": Decimal("10"), "titi": Decimal("20"), "tutu": Decimal("40")},
                {"toto": Decimal("11"), "titi": Decimal("22"), "tutu": Decimal("44")},
                {"toto": Decimal("12"), "titi": Decimal("24"), "tutu": Decimal("48")},
            ],
            [
                {"a": Decimal("10"), "b": Decimal("20"), "c": Decimal("40")},
                {"a": Decimal("11"), "b": Decimal("22"), "c": Decimal("44")},
                {"a": Decimal("12"), "b": Decimal("24"), "c": Decimal("48")},
            ],
            id="basic",
        ),
        param(
            {"a": "toto", "b": "titi", "c": "-toto+2", "d": 'val("Candidate")'},
            [
                {"toto": Decimal("10"), "titi": Decimal("20")},
                {"toto": Decimal("11"), "titi": Decimal("22")},
                {"toto": Decimal("12"), "titi": Decimal("24")},
            ],
            [
                {
                    "a": Decimal("10"),
                    "b": Decimal("20"),
                    "c": Decimal("-8"),
                    "d": "Candidate",
                },
                {
                    "a": Decimal("11"),
                    "b": Decimal("22"),
                    "c": Decimal("-9"),
                    "d": "Candidate",
                },
                {
                    "a": Decimal("12"),
                    "b": Decimal("24"),
                    "c": Decimal("-10"),
                    "d": "Candidate",
                },
            ],
            id="with formula",
        ),
        param(
            {"a": "toto", "b": "titi"},
            [
                {"toto": Decimal("10"), "titi": None},
                {"toto": Decimal("11"), "titi": None},
                {"toto": Decimal("12"), "titi": None},
            ],
            [
                {"a": Decimal("10"), "b": None},
                {"a": Decimal("11"), "b": None},
                {"a": Decimal("12"), "b": None},
            ],
            id="with empty str",
        ),
        param(
            {"a": "toto", "b": "titi", "c": "-toto+2", "d": 'val("Candidate")'},
            [
                {"toto": Decimal("10"), "titi": None},
                {"toto": Decimal("11"), "titi": Decimal("22")},
                {"toto": None, "titi": Decimal("24")},
            ],
            [
                {
                    "a": Decimal("10"),
                    "b": None,
                    "c": Decimal("-8"),
                    "d": "Candidate",
                },
                {
                    "a": Decimal("11"),
                    "b": Decimal("22"),
                    "c": Decimal("-9"),
                    "d": "Candidate",
                },
                {
                    "a": None,
                    "b": Decimal("24"),
                    "c": None,
                    "d": "Candidate",
                },
            ],
            id="mixed",
        ),
    ],
)
def test_dict_csv_to_dict_csv_exoplanet(mapping, csv_rows, expected):
    assert _dict_csv_to_exoplanet_dict_csv(mapping, csv_rows) == expected


@pytest.mark.parametrize(
    "mapping, csv_rows, expected",
    [
        param(
            {"a": "toto", "b": "titi", "c": "tutu"},
            [
                {"toto": Decimal("10"), "titi": Decimal("20"), "tutu": Decimal("40")},
                {"toto": Decimal("11"), "titi": Decimal("22"), "tutu": Decimal("44")},
                {"toto": Decimal("12"), "titi": Decimal("24"), "tutu": Decimal("48")},
            ],
            [
                ["a", "b", "c"],
                [Decimal("10"), Decimal("20"), Decimal("40")],
                [Decimal("11"), Decimal("22"), Decimal("44")],
                [Decimal("12"), Decimal("24"), Decimal("48")],
            ],
            id="basic",
        ),
        param(
            {"a": "toto", "b": "titi", "c": "-toto+2", "d": 'val("Candidate")'},
            [
                {"toto": Decimal("10"), "titi": Decimal("20")},
                {"toto": Decimal("11"), "titi": Decimal("22")},
                {"toto": Decimal("12"), "titi": Decimal("24")},
            ],
            [
                ["a", "b", "c", "d"],
                [Decimal("10"), Decimal("20"), Decimal("-8"), "Candidate"],
                [Decimal("11"), Decimal("22"), Decimal("-9"), "Candidate"],
                [Decimal("12"), Decimal("24"), Decimal("-10"), "Candidate"],
            ],
            id="with formula",
        ),
        param(
            {"a": "toto", "b": "titi"},
            [
                {"toto": Decimal("10"), "titi": None},
                {"toto": Decimal("11"), "titi": None},
                {"toto": Decimal("12"), "titi": None},
            ],
            [
                ["a", "b"],
                [Decimal("10"), None],
                [Decimal("11"), None],
                [Decimal("12"), None],
            ],
            id="with empty str",
        ),
        param(
            {"a": "toto", "b": "titi", "c": "-toto+2", "d": 'val("Candidate")'},
            [
                {"toto": Decimal("10"), "titi": None},
                {"toto": Decimal("11"), "titi": Decimal("22")},
                {"toto": None, "titi": Decimal("24")},
            ],
            [
                ["a", "b", "c", "d"],
                [Decimal("10"), None, Decimal("-8"), "Candidate"],
                [Decimal("11"), Decimal("22"), Decimal("-9"), "Candidate"],
                [None, Decimal("24"), None, "Candidate"],
            ],
            id="mixed",
        ),
    ],
)
def test_translate(mapping, csv_rows, expected):
    assert translate(mapping, csv_rows) == expected


def test_translate_real_condition(translate_expected: CsvData):
    mapping = read_yaml(Path(f"{TEST_PATH}/mapping_files/mapping_tess.yml"))
    csv_content = read_csv(Path(f"{TEST_PATH}/csv_files/tess_2_obj.csv"))
    formatted_csv = format_csv(csv_content)

    # translate_expected return a list cf. conftest.py
    assert translate(mapping, formatted_csv) == translate_expected


def test_translate_with_created_modified():
    mapping = read_yaml(Path(f"{TEST_PATH}/mapping_files/mapping_w_crea_modif.yml"))
    csv_content = read_csv(Path(f"{TEST_PATH}/csv_files/tess_2_obj_w_crea_modif.csv"))
    formatted_csv = format_csv(csv_content)

    translated_csv = translate(mapping, formatted_csv)
    formatted_translated_csv = format_csv(translated_csv)

    exp_date = "2023-03-03 10:19:43.017580"

    with soft_assertions():
        for elem in formatted_translated_csv:
            assert_that(elem["planet_created"]).is_equal_to(exp_date)
            assert_that(elem["planet_modified"]).is_equal_to(exp_date)
            assert_that(elem["star_created"]).is_equal_to(exp_date)
            assert_that(elem["star_modified"]).is_equal_to(exp_date)
            assert_that(elem["publication_created"]).is_equal_to(exp_date)
            assert_that(elem["publication_modified"]).is_equal_to(exp_date)


def test_translate_python_syntax_error_in_mapping():
    mapping = read_yaml(Path(f"{TEST_PATH}/mapping_files/python_syntax_error.yml"))
    csv_content = read_csv(Path(f"{TEST_PATH}/csv_files/tess_2_obj.csv"))
    formatted_csv = format_csv(csv_content)

    with pytest.raises(PythonSyntaxError):
        translate(mapping, formatted_csv)
