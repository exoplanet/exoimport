# Standard imports
# Standard imports
from os import getenv
from pathlib import Path

# Third party imports
from pytest_bdd import given, parsers, then, when
from typer.testing import CliRunner

# First party imports
from cli_app import APP
from exoimport import get_logger, reset_config

TEST_CWD = Path.cwd()
TEST_PATH = TEST_CWD / "tests/input_files"
RUNNER = CliRunner()


@given(parsers.parse("I want to make {arg}"), target_fixture="import")
def imp(arg, cmd_args: list):
    match arg:
        case "an import":
            # Uncomment following line for debugging
            # cmd_args.append("--debug")

            cmd_args.append("import")
        case "a translate":
            # Uncomment following line for debugging
            # cmd_args.append("--debug")

            cmd_args.append("translate")


@given(parsers.parse("I have {arg}"), target_fixture="files")
def files(arg, cmd_args: list):
    file = {
        "an empty csv file": "csv_files/empty.csv",
        "an invalid external csv file": "csv_files/wrong_header.csv",
        "an invalid exoplanet csv file": "csv_files/wrong_csv_exoplanet.csv",
        "a valid exoplanet csv file": "csv_files/csv_exoplanet_5_obj_imp_2.csv",
        "a valid external csv file": "csv_files/tess_2_obj.csv",
        "a csv file with an invalid object name": "csv_files/wrong_name.csv",
        "a csv file with 2 stars at the same position": "csv_files/double_same_star.csv",
        "an empty mapping file": "mapping_files/empty.yml",
        "an invalid mapping file": "mapping_files/invalid_yaml.yml",
        "a valid mapping file": "mapping_files/mapping_tess.yml",
    }

    cmd_args.append(f"{TEST_PATH}/{file[arg]}")


@given("I ask no validation", target_fixture="no_validate")
def no_validate(cmd_args: list):
    cmd_args.append("--no-validate")


@given("I ask no filtration", target_fixture="no_filter")
def no_filter(cmd_args: list):
    cmd_args.append("--no-filter")


@given("I ask no translation", target_fixture="no_translate")
def no_validate(cmd_args: list):
    cmd_args.append("--no-translate")


@when(parsers.parse("I launch the program"), target_fixture="launch")
def launch(cmd_args, db_for_typer, define_new_cwd):
    if "import" in cmd_args:
        cmd_args.append(f"--config-file={TEST_CWD}/pyproject.toml")
        return cmd_args

    if "translate" in cmd_args:
        reset_config()
        get_logger.cache_clear()
        return RUNNER.invoke(
            APP,
            cmd_args,
            input=None,
            env=dict(EXOIMPORT_DBPORT=getenv("EXOIMPORT_DBPORT")),
        )


@when("confirm by pressing y and enter", target_fixture="enter")
def enter(launch, db_for_typer, define_new_cwd):
    reset_config()
    get_logger.cache_clear()
    return RUNNER.invoke(
        APP,
        args=launch,
        input="y",
        env=dict(EXOIMPORT_DBPORT=getenv("EXOIMPORT_DBPORT")),
    )


@then("a message confirm that the csv was imported")
def import_msg(enter):
    # Uncomment following line for debugging
    print(enter.stdout)

    assert "The import has completed without error" in enter.stdout


@then("a message confirm that the translate was finish")
def translate_msg(launch):
    # Uncomment following line for debugging
    # print(launch.stdout)

    assert "The translate end without error" in launch.stdout


@then("an error message was display")
def err_msg(enter, launch):
    match enter:
        case None:
            result = launch
        case _:
            result = enter
    assert "Error" in result.stdout


@then(parsers.parse("the program end {arg} error"))
def finish(arg, enter, launch):
    match enter:
        case None:
            result = launch
        case _:
            result = enter
    match arg:
        case "without":
            assert result.exit_code == 0
        case "with an":
            assert result.exit_code > 0
