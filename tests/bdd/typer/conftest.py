# Pytest imports
import pytest


@pytest.fixture(scope="function")
def cmd_args():
    return []


@pytest.fixture(scope="function")
def enter():
    return None
