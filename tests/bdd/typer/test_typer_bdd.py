# Third party imports
from pytest_bdd import scenario
from utils_bdd import *


@scenario(
    "../scenarios/translate.feature",
    "Translate a csv file and we make only validation",
)
def test_translate_only_validate():
    pass


@scenario(
    "../scenarios/translate.feature",
    "Translate a csv file and we make only filtration",
)
def test_translate_only_filter():
    pass


@scenario(
    "../scenarios/translate.feature",
    "Translate a csv file and we make only translation",
)
def test_translate_only_translate():
    pass


@scenario(
    "../scenarios/translate.feature",
    "Translate a csv file and we make validation and filtration",
)
def test_translate_validate_and_filter():
    pass


@scenario(
    "../scenarios/translate.feature",
    "Translate a csv file and we make validation and translation",
)
def test_translate_validate_and_translate():
    pass


@scenario(
    "../scenarios/translate.feature",
    "Translate a csv file and we make filtration and translation",
)
def test_translate_filter_and_Translate():
    pass


@scenario(
    "../scenarios/translate.feature",
    "Translate a csv file and we make all steps",
)
def test_translate_all_steps():
    pass


@scenario(
    "../scenarios/translate.feature",
    "Translate a csv file but the csv file is empty",
)
def test_translate_csv_empty():
    pass


@scenario(
    "../scenarios/translate.feature",
    "Translate a csv file but the mapping file is empty",
)
def test_translate_mapping_empty():
    pass


@scenario(
    "../scenarios/translate.feature",
    "Translate a csv file but the csv file is invalid",
)
def test_translate_invalid_csv():
    pass


@scenario(
    "../scenarios/translate.feature",
    "Translate a csv file but the mapping file is invalid",
)
def test_translate_invalid_mapping():
    pass


@scenario(
    "../scenarios/translate.feature",
    "Translate a csv file but the csv file and the mapping file are invalid",
)
def test_translate_invalid_csv_and_mapping():
    pass


@scenario("../scenarios/import.feature", "Import a csv file")
def test_import_valid():
    pass


@scenario("../scenarios/import.feature", "Import a csv file but the file is invalid")
def test_import_invalid():
    pass


@scenario(
    "../scenarios/import.feature",
    "Import a csv file but the objects name are invalid",
)
def test_import_invalid_object_name():
    pass


@scenario(
    "../scenarios/import.feature", "Import a csv file but a star is already in the db"
)
def test_import_invalid_same_star():
    pass
