Feature: Import

    Scenario: Import a csv file
        Given I want to make an import
        And I have a valid external csv file
        And I have a valid mapping file
        And I have a valid exoplanet csv file
        When I launch the program
        And confirm by pressing y and enter
        Then a message confirm that the csv was imported
        And the program end without error

    Scenario: Import a csv file but the file is invalid
        Given I want to make an import
        And I have a valid external csv file
        And I have a valid mapping file
        And I have an invalid exoplanet csv file
        When I launch the program
        And confirm by pressing y and enter
        Then an error message was display
        Then the program end with an error

    Scenario: Import a csv file but the objects name are invalid
        Given I want to make an import
        And I have a valid external csv file
        And I have a valid mapping file
        And I have a csv file with an invalid object name
        When I launch the program
        And confirm by pressing y and enter
        Then an error message was display
        Then the program end with an error


    Scenario: Import a csv file but a star is already in the db
        Given I want to make an import
        And I have a valid external csv file
        And I have a valid mapping file
        And I have a csv file with 2 stars at the same position
        When I launch the program
        And confirm by pressing y and enter
        Then an error message was display
        Then the program end with an error
