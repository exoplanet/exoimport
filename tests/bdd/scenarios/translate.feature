Feature: Translate

    Scenario: Translate a csv file and we make only validation
        Given I want to make a translate
        And I have a valid external csv file
        And I have a valid mapping file
        And I ask no filtration
        And I ask no translation
        When I launch the program
        Then a message confirm that the translate was finish
        And the program end without error

    Scenario: Translate a csv file and we make only filtration
        Given I want to make a translate
        And I have a valid external csv file
        And I have a valid mapping file
        And I ask no validation
        And I ask no translation
        When I launch the program
        Then a message confirm that the translate was finish
        And the program end without error

    Scenario: Translate a csv file and we make only translation
        Given I want to make a translate
        And I have a valid external csv file
        And I have a valid mapping file
        And I ask no validation
        And I ask no translation
        When I launch the program
        Then a message confirm that the translate was finish
        And the program end without error

    Scenario: Translate a csv file and we make validation and filtration
        Given I want to make a translate
        And I have a valid external csv file
        And I have a valid mapping file
        And I ask no translation
        When I launch the program
        Then a message confirm that the translate was finish
        And the program end without error

    Scenario: Translate a csv file and we make validation and translation
        Given I want to make a translate
        And I have a valid external csv file
        And I have a valid mapping file
        And I ask no filtration
        When I launch the program
        Then a message confirm that the translate was finish
        And the program end without error

    Scenario: Translate a csv file and we make filtration and translation
        Given I want to make a translate
        And I have a valid external csv file
        And I have a valid mapping file
        And I ask no validation
        When I launch the program
        Then a message confirm that the translate was finish
        And the program end without error

    Scenario: Translate a csv file and we make all steps
        Given I want to make a translate
        And I have a valid external csv file
        And I have a valid mapping file
        When I launch the program
        Then a message confirm that the translate was finish
        And the program end without error

    Scenario: Translate a csv file but the csv file is empty
        Given I want to make a translate
        And I have an empty csv file
        And I have a valid mapping file
        When I launch the program
        Then an error message was display
        And the program end with an error

    Scenario: Translate a csv file but the mapping file is empty
        Given I want to make a translate
        And I have a valid external csv file
        And I have an empty mapping file
        When I launch the program
        Then an error message was display
        And the program end with an error

    Scenario: Translate a csv file but the csv file is invalid
        Given I want to make a translate
        And I have an invalid external csv file
        And I have a valid mapping file
        When I launch the program
        Then an error message was display
        And the program end with an error

    Scenario: Translate a csv file but the mapping file is invalid
        Given I want to make a translate
        And I have a valid external csv file
        And I have an invalid mapping file
        When I launch the program
        Then an error message was display
        And the program end with an error

    Scenario: Translate a csv file but the csv file and the mapping file are invalid
        Given I want to make a translate
        And I have an invalid external csv file
        And I have an invalid mapping file
        When I launch the program
        Then an error message was display
        And the program end with an error
